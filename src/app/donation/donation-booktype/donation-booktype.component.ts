import {Component, OnInit} from "@angular/core";
import {TitleConstants, HttpStatusConstants, APPConstants} from "../../utility/constants";
import {Title} from "@angular/platform-browser";
import {Router, ActivatedRoute} from "@angular/router";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {DonationBookTypeManagementService} from "./donation-booktype.service";
import {DonationBookType} from "./donation-booktype.model";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {Pager} from "../../templemanagement/pager.model";
import {Md2Module} from "md2";
import any = jasmine.any;


@Component({
  selector: 'app-donation-booktype',
  templateUrl: './donation-booktype.component.html',
  styleUrls: ['./donation-booktype.component.css'],
  providers:[DonationBookTypeManagementService]
})
export class DonationBooktypeComponent implements OnInit {
  titleConstants = new TitleConstants();
  appConstant = new APPConstants();
  position: string = 'before';
  bookTypeList: DonationBookType[];
  pager: Pager;
  totalCount: number;
  isFormInvalid: boolean;
  isFormSubmitted: boolean;
  bookTypeForm: FormGroup;
  selectedBookType: DonationBookType;
  toggle: boolean;
  checkedBookTypeId: number[] = [];

  constructor(private donationBookTypeService:DonationBookTypeManagementService, private title: Title,
              private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router, private builder: FormBuilder) {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
      this.toggle = false;
    })
  }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.DONATION_BOOK_TYPE_LIST);
    this.getBookType(1);
    this.initialize();
  }

  initialize() {
    this.isFormSubmitted = false
    this.createBookTypeForm();
  }

  createBookTypeForm() {
    this.isFormInvalid = true
    this.bookTypeForm = this.builder.group({
      name: new FormControl('', <any>Validators.required),
      denomination: new FormControl('', <any>Validators.required),
      numberOfReceipt: new FormControl('', <any>Validators.required),
    })
  }

  getBookType(pageIndex: number) {
    this.donationBookTypeService.getDonationBookTypeList(pageIndex).subscribe(response => {
      if (response.status == HttpStatusConstants.SUCCESS) {
        // this.eventCount = response['count'];
        this.pager = response.pager;
        this.bookTypeList = response.payload.data;
        this.totalCount = this.pager.totalRecords;
        this.appConstant.PAGINATION_SIZE = this.pager.recordsPerPage;
        this.toggle = false;
        this.checkedBookTypeId = [];
      }
    })
  }

  saveDonationBookType(value: any, isFormValid: boolean, dialog: any) {
    this.isFormSubmitted = true;
    this.isFormInvalid = isFormValid;
    if (isFormValid) {
      if (this.selectedBookType == null) {
        this.donationBookTypeService.addDonationBookType(value).subscribe(result => {
          this.initialize()
        })
      } else {
        let body = {
          "name": value.name,
          "denomination":value.denomination,
          "numberOfReceipt":value.numberOfReceipt,
        }
        this.donationBookTypeService.editDonationBookType(this.selectedBookType.id, body).subscribe(result => {
          this.initialize()
          this.selectedBookType = null;
          dialog.close();
        })
      }
    }
  }

  show(dialog: any, bookTypeData) {
    this.initialize();
    dialog.open();
    if (bookTypeData) {
      this.selectedBookType = bookTypeData;
      this.isFormInvalid = true;
      this.bookTypeForm = this.builder.group({
        name: new FormControl(bookTypeData.name, <any>Validators.required),
        denomination: new FormControl(bookTypeData.denomination, <any>Validators.required),
        numberOfReceipt: new FormControl(bookTypeData.numberOfReceipt, <any>Validators.required)
      })
    }
  }

  toggleBookType(bookType) {
    bookType.checked = !bookType.checked
    if(!bookType.checked){
      let idValue = bookType.id;
      let ind = this.checkedBookTypeId.indexOf(idValue);
      this.checkedBookTypeId.splice(ind,1);
    }else{
      let idVal = bookType.id;
      this.checkedBookTypeId.push(idVal);
    }
    this.toggle = this.bookTypeList.every(bookType => bookType.checked);
    console.log(this.checkedBookTypeId);
  }

  close(dialog: any) {
    dialog.close();
  }

  toggleAll() {
    this.toggle = !this.toggle
    this.bookTypeList.forEach(bookType => bookType.checked = this.toggle)
    if(this.toggle){
      for (let i = 0; i < this.bookTypeList.length; ++i) {
        let idVal = this.bookTypeList[i]["id"];
        if(this.checkedBookTypeId.length == 0){
          this.checkedBookTypeId.push(idVal);
        }else{
          let ind = this.checkedBookTypeId.indexOf(idVal);
          if(ind == -1){
            this.checkedBookTypeId.push(idVal);
          }
        }
      }
    }else{
      this.checkedBookTypeId = [];
    }
    console.log(this.checkedBookTypeId);
  }
}
