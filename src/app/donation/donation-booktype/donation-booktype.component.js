"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../../utility/constants");
var donation_booktype_service_1 = require("./donation-booktype.service");
var forms_1 = require("@angular/forms");
var DonationBooktypeComponent = (function () {
    function DonationBooktypeComponent(donationBookTypeService, title, breadcrumbService, activatedRoute, router, builder) {
        var _this = this;
        this.donationBookTypeService = donationBookTypeService;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.builder = builder;
        this.titleConstants = new constants_1.TitleConstants();
        this.appConstant = new constants_1.APPConstants();
        this.position = 'before';
        this.checkedBookTypeId = [];
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
            _this.toggle = false;
        });
    }
    DonationBooktypeComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.DONATION_BOOK_TYPE_LIST);
        this.getBookType(1);
        this.initialize();
    };
    DonationBooktypeComponent.prototype.initialize = function () {
        this.isFormSubmitted = false;
        this.createBookTypeForm();
    };
    DonationBooktypeComponent.prototype.createBookTypeForm = function () {
        this.isFormInvalid = true;
        this.bookTypeForm = this.builder.group({
            name: new forms_1.FormControl('', forms_1.Validators.required),
            denomination: new forms_1.FormControl('', forms_1.Validators.required),
            numberOfReceipt: new forms_1.FormControl('', forms_1.Validators.required)
        });
    };
    DonationBooktypeComponent.prototype.getBookType = function (pageIndex) {
        var _this = this;
        this.donationBookTypeService.getDonationBookTypeList(pageIndex).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                // this.eventCount = response['count'];
                _this.pager = response.pager;
                _this.bookTypeList = response.payload.data;
                _this.totalCount = _this.pager.totalRecords;
                _this.appConstant.PAGINATION_SIZE = _this.pager.recordsPerPage;
                _this.toggle = false;
                _this.checkedBookTypeId = [];
            }
        });
    };
    DonationBooktypeComponent.prototype.saveDonationBookType = function (value, isFormValid, dialog) {
        var _this = this;
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid) {
            if (this.selectedBookType == null) {
                this.donationBookTypeService.addDonationBookType(value).subscribe(function (result) {
                    _this.initialize();
                });
            }
            else {
                var body = {
                    "name": value.name,
                    "denomination": value.denomination,
                    "numberOfReceipt": value.numberOfReceipt
                };
                this.donationBookTypeService.editDonationBookType(this.selectedBookType.id, body).subscribe(function (result) {
                    _this.initialize();
                    _this.selectedBookType = null;
                    dialog.close();
                });
            }
        }
    };
    DonationBooktypeComponent.prototype.show = function (dialog, bookTypeData) {
        this.initialize();
        dialog.open();
        if (bookTypeData) {
            this.selectedBookType = bookTypeData;
            this.isFormInvalid = true;
            this.bookTypeForm = this.builder.group({
                name: new forms_1.FormControl(bookTypeData.name, forms_1.Validators.required),
                denomination: new forms_1.FormControl(bookTypeData.denomination, forms_1.Validators.required),
                numberOfReceipt: new forms_1.FormControl(bookTypeData.numberOfReceipt, forms_1.Validators.required)
            });
        }
    };
    DonationBooktypeComponent.prototype.toggleBookType = function (bookType) {
        bookType.checked = !bookType.checked;
        if (!bookType.checked) {
            var idValue = bookType.id;
            var ind = this.checkedBookTypeId.indexOf(idValue);
            this.checkedBookTypeId.splice(ind, 1);
        }
        else {
            var idVal = bookType.id;
            this.checkedBookTypeId.push(idVal);
        }
        this.toggle = this.bookTypeList.every(function (bookType) { return bookType.checked; });
        console.log(this.checkedBookTypeId);
    };
    DonationBooktypeComponent.prototype.close = function (dialog) {
        dialog.close();
    };
    DonationBooktypeComponent.prototype.toggleAll = function () {
        var _this = this;
        this.toggle = !this.toggle;
        this.bookTypeList.forEach(function (bookType) { return bookType.checked = _this.toggle; });
        if (this.toggle) {
            for (var i = 0; i < this.bookTypeList.length; ++i) {
                var idVal = this.bookTypeList[i]["id"];
                if (this.checkedBookTypeId.length == 0) {
                    this.checkedBookTypeId.push(idVal);
                }
                else {
                    var ind = this.checkedBookTypeId.indexOf(idVal);
                    if (ind == -1) {
                        this.checkedBookTypeId.push(idVal);
                    }
                }
            }
        }
        else {
            this.checkedBookTypeId = [];
        }
        console.log(this.checkedBookTypeId);
    };
    DonationBooktypeComponent = __decorate([
        core_1.Component({
            selector: 'app-donation-booktype',
            templateUrl: './donation-booktype.component.html',
            styleUrls: ['./donation-booktype.component.css'],
            providers: [donation_booktype_service_1.DonationBookTypeManagementService]
        })
    ], DonationBooktypeComponent);
    return DonationBooktypeComponent;
}());
exports.DonationBooktypeComponent = DonationBooktypeComponent;
//# sourceMappingURL=donation-booktype.component.js.map