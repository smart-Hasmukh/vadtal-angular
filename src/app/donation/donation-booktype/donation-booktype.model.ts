export class DonationBookType{
    get checked(): boolean {
        return this._checked;
    }

    set checked(value: boolean) {
        this._checked = value;
    }

    private name : string;
    private isEnable :number;
    private numberOfReceipt:number;
    private denomination:number;
    private _id:number;
    private _checked:boolean;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
}