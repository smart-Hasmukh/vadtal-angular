"use strict";
var DonationBookType = (function () {
    function DonationBookType() {
    }
    Object.defineProperty(DonationBookType.prototype, "checked", {
        get: function () {
            return this._checked;
        },
        set: function (value) {
            this._checked = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DonationBookType.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    return DonationBookType;
}());
exports.DonationBookType = DonationBookType;
//# sourceMappingURL=donation-booktype.model.js.map