import { Injectable } from '@angular/core';
import {HttpService} from "../../utility/http-service";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../../utility/constants";
import {ToastsManager} from "ng2-toastr";
import {Response} from "@angular/http";

@Injectable()
export class DonationBookTypeManagementService {

    constructor(private  httpService: HttpService,private toastManager : ToastsManager) {}

    getDonationBookTypeList(pageIndex : number): Observable<any> {
        return this.httpService.get(ApiEventConstants.DONATIONBOOKTYPE +"?pageNumber="+pageIndex).map(res => this.extractData(res, false));
    }

    private extractData(res: Response, showToast: boolean) {
        if (showToast) {
            this.toastManager.success(res.json().message)
        }
        let data = res.json();
        return data || {};
    }

    addDonationBookType(values): Observable<any> {
        return this.httpService
            .post(ApiEventConstants.DONATIONBOOKTYPE, values)
            .map(res => this.extractData(res, true));
    }

    editDonationBookType(bookTypeId, values): Observable<any> {
        return this.httpService
            .put(ApiEventConstants.DONATIONBOOKTYPE +"/"+ bookTypeId+"/", values)
            .map(res => this.extractData(res, true));
    }
}
