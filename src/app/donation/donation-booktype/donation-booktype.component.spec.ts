/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DonationBooktypeComponent } from './donation-booktype.component';

describe('DonationBooktypeComponent', () => {
  let component: DonationBooktypeComponent;
  let fixture: ComponentFixture<DonationBooktypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonationBooktypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonationBooktypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
