"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var donation_component_1 = require("./donation.component");
var donation_dashboard_component_1 = require("./donation-dashboard/donation-dashboard.component");
var donation_area_management_component_1 = require("./donation-area-management/donation-area-management.component");
var donation_year_management_component_1 = require("./donation-year-management/donation-year-management.component");
var donation_booktype_component_1 = require("./donation-booktype/donation-booktype.component");
var donation_donor_profile_update_component_1 = require("./donation-donor-profile-update/donation-donor-profile-update.component");
var donation_delete_receipt_component_1 = require("./donation-delete-receipt/donation-delete-receipt.component");
var constants_1 = require("../utility/constants");
exports.routes = [
    {
        path: '',
        component: donation_component_1.DonationComponent,
        children: [
            {
                path: '',
                component: donation_dashboard_component_1.DonationDashboardComponent,
                data: { title: "Donation Dashboard" }
            },
            {
                path: constants_1.RouteConstants.APP_DONATION_AREA_MANAGEMENT,
                component: donation_area_management_component_1.DonationAreaManagementComponent,
                data: { title: "Donation Area Management" }
            },
            {
                path: constants_1.RouteConstants.APP_DONATION_YEAR_MANAGEMENT,
                component: donation_year_management_component_1.DonationYearManagementComponent,
                data: { title: "Donation Year Management" }
            },
            {
                path: constants_1.RouteConstants.APP_DONATION_BOOK_TYPE,
                component: donation_booktype_component_1.DonationBooktypeComponent,
                data: { title: "Donation Book Type" }
            },
            {
                path: constants_1.RouteConstants.APP_DONATION_DONOR_PROFILE_UPDATE,
                component: donation_donor_profile_update_component_1.DonationDonorProfileUpdateComponent,
                data: { title: "Donor Profile Update" }
            },
            {
                path: constants_1.RouteConstants.APP_DONATION_DELETE_RECEIPT,
                component: donation_delete_receipt_component_1.DonationDeleteReceiptComponent,
                data: { title: "Delete Receipt" }
            },
        ]
    },
];
var DonationRoutingModule = (function () {
    function DonationRoutingModule() {
    }
    DonationRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(exports.routes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], DonationRoutingModule);
    return DonationRoutingModule;
}());
exports.DonationRoutingModule = DonationRoutingModule;
//# sourceMappingURL=donation-routing.module.js.map