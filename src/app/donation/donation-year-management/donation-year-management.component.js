"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../../utility/constants");
var donation_year_management_service_1 = require("./donation-year-management.service");
var forms_1 = require("@angular/forms");
var DonationYearManagementComponent = (function () {
    function DonationYearManagementComponent(donationYearManagementService, title, breadcrumbService, activatedRoute, router, builder) {
        var _this = this;
        this.donationYearManagementService = donationYearManagementService;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.builder = builder;
        this.titleConstants = new constants_1.TitleConstants();
        this.appConstant = new constants_1.APPConstants();
        this.position = 'before';
        this.checkedYearId = [];
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
            _this.toggle = false;
        });
    }
    // disabled: boolean = true;
    // date: Date = new Date(2016, 9, 15);
    // time: Date = new Date(1, 1, 1, 12, 10);
    // datetime: Date = new Date(2016, 9, 15, 12, 10);
    // minDate: Date = new Date(2016, 7, 15);
    // maxDate: Date = new Date(2016, 12, 15);
    DonationYearManagementComponent.prototype.handleChange = function (value) {
        console.log('Changed data: ', value);
    };
    DonationYearManagementComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.YEAR_LIST);
        this.getYear(1);
        this.initialize();
    };
    DonationYearManagementComponent.prototype.initialize = function () {
        this.isFormSubmitted = false;
        this.createYearForm();
    };
    DonationYearManagementComponent.prototype.createYearForm = function () {
        this.isFormInvalid = true;
        this.yearForm = this.builder.group({
            gujaratiYear: new forms_1.FormControl('', forms_1.Validators.required),
            startDate: new forms_1.FormControl('', forms_1.Validators.required),
            endDate: new forms_1.FormControl('', forms_1.Validators.required)
        });
    };
    DonationYearManagementComponent.prototype.getYear = function (pageIndex) {
        var _this = this;
        this.donationYearManagementService.getYearList(pageIndex).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                // this.eventCount = response['count'];
                _this.pager = response.pager;
                _this.yearList = response.payload.data;
                _this.totalCount = _this.pager.totalRecords;
                _this.appConstant.PAGINATION_SIZE = _this.pager.recordsPerPage;
                _this.toggle = false;
                _this.checkedYearId = [];
            }
        });
    };
    DonationYearManagementComponent.prototype.saveYear = function (value, isFormValid, dialog) {
        var _this = this;
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid) {
            if (this.selectedYear == null) {
                console.log(value);
                this.donationYearManagementService.addYear(value).subscribe(function (result) {
                    _this.initialize();
                });
            }
            else {
                var body = {
                    "gujaratiYear": value.gujaratiYear,
                    "startDate": value.startDate,
                    "endDate": value.endDate
                };
                this.donationYearManagementService.editYear(this.selectedYear.id, body).subscribe(function (result) {
                    _this.initialize();
                    _this.selectedYear = null;
                    dialog.close();
                });
            }
        }
    };
    DonationYearManagementComponent.prototype.show = function (dialog, yearData) {
        this.initialize();
        dialog.open();
        if (yearData) {
            this.selectedYear = yearData;
            this.isFormInvalid = true;
            this.yearForm = this.builder.group({
                gujaratiYear: new forms_1.FormControl(yearData.gujaratiYear, forms_1.Validators.required),
                startDate: new forms_1.FormControl(yearData.startDate, forms_1.Validators.required),
                endDate: new forms_1.FormControl(yearData.endDate, forms_1.Validators.required)
            });
        }
    };
    DonationYearManagementComponent.prototype.close = function (dialog) {
        dialog.close();
    };
    DonationYearManagementComponent.prototype.toggleYear = function (year) {
        year.checked = !year.checked;
        if (!year.checked) {
            var idValue = year.id;
            var ind = this.checkedYearId.indexOf(idValue);
            this.checkedYearId.splice(ind, 1);
        }
        else {
            var idVal = year.id;
            this.checkedYearId.push(idVal);
        }
        this.toggle = this.yearList.every(function (year) { return year.checked; });
        console.log(this.checkedYearId);
    };
    DonationYearManagementComponent.prototype.toggleAll = function () {
        var _this = this;
        this.toggle = !this.toggle;
        this.yearList.forEach(function (year) { return year.checked = _this.toggle; });
        if (this.toggle) {
            for (var i = 0; i < this.yearList.length; ++i) {
                var idVal = this.yearList[i]["id"];
                if (this.checkedYearId.length == 0) {
                    this.checkedYearId.push(idVal);
                }
                else {
                    var ind = this.checkedYearId.indexOf(idVal);
                    if (ind == -1) {
                        this.checkedYearId.push(idVal);
                    }
                }
            }
        }
        else {
            this.checkedYearId = [];
        }
        console.log(this.checkedYearId);
    };
    DonationYearManagementComponent = __decorate([
        core_1.Component({
            selector: 'app-donation-year-management',
            templateUrl: './donation-year-management.component.html',
            styleUrls: ['./donation-year-management.component.css'],
            providers: [donation_year_management_service_1.DonationYearManagementService]
        })
    ], DonationYearManagementComponent);
    return DonationYearManagementComponent;
}());
exports.DonationYearManagementComponent = DonationYearManagementComponent;
//# sourceMappingURL=donation-year-management.component.js.map