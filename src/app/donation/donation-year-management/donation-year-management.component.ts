import {Component, OnInit } from "@angular/core";
import {TitleConstants, HttpStatusConstants, APPConstants} from "../../utility/constants";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {DonationYearManagementService} from "./donation-year-management.service";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {Pager} from "../../templemanagement/pager.model";
import {Md2Module} from "md2";
import {Year} from "./donation-year.model";
import any = jasmine.any;

@Component({
  selector: 'app-donation-year-management',
  templateUrl: './donation-year-management.component.html',
  styleUrls: ['./donation-year-management.component.css'],
  providers:[DonationYearManagementService]
})
export class DonationYearManagementComponent implements OnInit {
  titleConstants = new TitleConstants();
  appConstant = new APPConstants()
  position: string = 'before';
  yearList: Year[];
  pager: Pager;
  totalCount: number;
  isFormInvalid: boolean;
  isFormSubmitted: boolean;
  yearForm: FormGroup;
  selectedYear: Year;
  toggle: boolean;
  checkedYearId: number[] = [];

  // disabled: boolean = true;
  // date: Date = new Date(2016, 9, 15);
  // time: Date = new Date(1, 1, 1, 12, 10);
  // datetime: Date = new Date(2016, 9, 15, 12, 10);
  // minDate: Date = new Date(2016, 7, 15);
  // maxDate: Date = new Date(2016, 12, 15);
  handleChange(value: any) {
    console.log('Changed data: ',value);
  }

  constructor(private donationYearManagementService: DonationYearManagementService, private title: Title,
              private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router, private builder: FormBuilder) {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
      this.toggle = false;
    })
  }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.YEAR_LIST);
    this.getYear(1);
    this.initialize();
  }

  initialize() {
    this.isFormSubmitted = false
    this.createYearForm();
  }

  createYearForm() {
    this.isFormInvalid = true
    this.yearForm = this.builder.group({
      gujaratiYear: new FormControl('', <any>Validators.required),
      startDate: new FormControl('', <any>Validators.required),
      endDate: new FormControl('', <any>Validators.required)
    })
  }

  getYear(pageIndex: number) {
    this.donationYearManagementService.getYearList(pageIndex).subscribe(response => {
      if (response.status == HttpStatusConstants.SUCCESS) {
        // this.eventCount = response['count'];
        this.pager = response.pager;
        this.yearList = response.payload.data;
        this.totalCount = this.pager.totalRecords;
        this.appConstant.PAGINATION_SIZE = this.pager.recordsPerPage;
        this.toggle = false;
        this.checkedYearId = [];
      }
    })
  }

  saveYear(value: any, isFormValid: boolean, dialog: any) {
    this.isFormSubmitted = true;
    this.isFormInvalid = isFormValid;

    if (isFormValid) {
      if (this.selectedYear == null) {
        console.log(value);
        this.donationYearManagementService.addYear(value).subscribe(result => {
          this.initialize();
        })
      } else {
        let body = {
          "gujaratiYear": value.gujaratiYear,
          "startDate":value.startDate,
          "endDate":value.endDate,
        }
        this.donationYearManagementService.editYear(this.selectedYear.id, body).subscribe(result => {
          this.initialize()
          this.selectedYear = null;
          dialog.close();
        })
      }
    }
  }

  show(dialog: any, yearData) {
    this.initialize();
    dialog.open();
    if (yearData) {
        this.selectedYear = yearData;
        this.isFormInvalid = true
        this.yearForm = this.builder.group({
            gujaratiYear: new FormControl(yearData.gujaratiYear, <any>Validators.required),
            startDate: new FormControl(yearData.startDate, <any>Validators.required),
            endDate: new FormControl(yearData.endDate, <any>Validators.required)
        })
    }
  }

  close(dialog: any) {
    dialog.close();
  }

  toggleYear(year) {
    year.checked = !year.checked
    if(!year.checked){
      let idValue = year.id;
      let ind = this.checkedYearId.indexOf(idValue);
      this.checkedYearId.splice(ind,1);
    }else{
      let idVal = year.id;
      this.checkedYearId.push(idVal);
    }
    this.toggle = this.yearList.every(year => year.checked);
    console.log(this.checkedYearId);
  }

  toggleAll() {
    this.toggle = !this.toggle;
    this.yearList.forEach(year => year.checked = this.toggle);
    if(this.toggle){
      for (let i = 0; i < this.yearList.length; ++i) {
        let idVal = this.yearList[i]["id"];
        if(this.checkedYearId.length == 0){
          this.checkedYearId.push(idVal);
        }else{
          let ind = this.checkedYearId.indexOf(idVal);
          if(ind == -1){
            this.checkedYearId.push(idVal);
          }
        }
      }
    }else{
      this.checkedYearId = [];
    }
    console.log(this.checkedYearId);
  }
}
