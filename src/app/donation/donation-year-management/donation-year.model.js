"use strict";
var Year = (function () {
    function Year() {
    }
    Object.defineProperty(Year.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Year.prototype, "checked", {
        get: function () {
            return this._checked;
        },
        set: function (value) {
            this._checked = value;
        },
        enumerable: true,
        configurable: true
    });
    return Year;
}());
exports.Year = Year;
//# sourceMappingURL=donation-year.model.js.map