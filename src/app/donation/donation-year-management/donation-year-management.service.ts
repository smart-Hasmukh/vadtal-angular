import { Injectable } from '@angular/core';
import {HttpService} from "../../utility/http-service";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../../utility/constants";
import {ToastsManager} from "ng2-toastr";
import {Response} from "@angular/http";

@Injectable()
export class DonationYearManagementService {

    constructor(private  httpService: HttpService,private toastManager : ToastsManager) {}

    getYearList(pageIndex : number): Observable<any> {
        return this.httpService.get(ApiEventConstants.YEAR+"?pageNumber="+pageIndex).map(res => this.extractData(res, false));
    }

    private extractData(res: Response, showToast: boolean) {
        if (showToast) {
            this.toastManager.success(res.json().message)
        }
        let data = res.json();
        return data || {};
    }

    addYear(values): Observable<any> {
        return this.httpService
            .post(ApiEventConstants.YEAR, values)
            .map(res => this.extractData(res, true));
    }

    editYear(yearId, values): Observable<any> {
        return this.httpService
            .put(ApiEventConstants.YEAR +"/"+ yearId+"/", values)
            .map(res => this.extractData(res, true));
    }
}
