"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var DonationYearManagementService = (function () {
    function DonationYearManagementService(httpService, toastManager) {
        this.httpService = httpService;
        this.toastManager = toastManager;
    }
    DonationYearManagementService.prototype.getYearList = function (pageIndex) {
        var _this = this;
        return this.httpService.get(constants_1.ApiEventConstants.YEAR + "?pageNumber=" + pageIndex).map(function (res) { return _this.extractData(res, false); });
    };
    DonationYearManagementService.prototype.extractData = function (res, showToast) {
        if (showToast) {
            this.toastManager.success(res.json().message);
        }
        var data = res.json();
        return data || {};
    };
    DonationYearManagementService.prototype.addYear = function (values) {
        var _this = this;
        return this.httpService
            .post(constants_1.ApiEventConstants.YEAR, values)
            .map(function (res) { return _this.extractData(res, true); });
    };
    DonationYearManagementService.prototype.editYear = function (yearId, values) {
        var _this = this;
        return this.httpService
            .put(constants_1.ApiEventConstants.YEAR + "/" + yearId + "/", values)
            .map(function (res) { return _this.extractData(res, true); });
    };
    DonationYearManagementService = __decorate([
        core_1.Injectable()
    ], DonationYearManagementService);
    return DonationYearManagementService;
}());
exports.DonationYearManagementService = DonationYearManagementService;
//# sourceMappingURL=donation-year-management.service.js.map