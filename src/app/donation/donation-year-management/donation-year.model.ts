export class Year{
    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
    get checked(): boolean {
        return this._checked;
    }

    set checked(value: boolean) {
        this._checked = value;
    }

    private startDate : string;
    private endDate : string;
    private isEnable :number;
    private _id:number;
    private _checked:boolean;

}