import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {DonationComponent} from "./donation.component";
import {DonationDashboardComponent} from "./donation-dashboard/donation-dashboard.component";
import {DonationAreaManagementComponent} from "./donation-area-management/donation-area-management.component";
import {DonationYearManagementComponent} from "./donation-year-management/donation-year-management.component";
import {DonationBooktypeComponent} from "./donation-booktype/donation-booktype.component";
import {DonationDonorProfileUpdateComponent} from "./donation-donor-profile-update/donation-donor-profile-update.component";
import {DonationDeleteReceiptComponent} from "./donation-delete-receipt/donation-delete-receipt.component";
import {RouteConstants} from "../utility/constants";

export const routes: Routes = [
    {
        path: '',
        component: DonationComponent,
        children: [
            {
                path: '',
                component: DonationDashboardComponent,
                data: {title: "Donation Dashboard"}
            },
            {
                path: RouteConstants.APP_DONATION_AREA_MANAGEMENT,
                component: DonationAreaManagementComponent,
                data: {title: "Donation Area Management"}
            },
            {
                path: RouteConstants.APP_DONATION_YEAR_MANAGEMENT,
                component: DonationYearManagementComponent,
                data: {title: "Donation Year Management"}
            },
            {
                path: RouteConstants.APP_DONATION_BOOK_TYPE,
                component: DonationBooktypeComponent,
                data: {title: "Donation Book Type"}
            },
            {
                path: RouteConstants.APP_DONATION_DONOR_PROFILE_UPDATE,
                component: DonationDonorProfileUpdateComponent,
                data: {title: "Donor Profile Update"}
            },
            {
                path: RouteConstants.APP_DONATION_DELETE_RECEIPT,
                component: DonationDeleteReceiptComponent,
                data: {title: "Delete Receipt"}
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class DonationRoutingModule {
}


