import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DonationComponent} from "./donation.component";
import {DonationRoutingModule} from "./donation-routing.module";
import {MaterialModule} from "@angular/material";
import {DonationDashboardComponent} from "./donation-dashboard/donation-dashboard.component";
import {DonationAreaManagementComponent} from "./donation-area-management/donation-area-management.component";
import {DonationYearManagementComponent} from "./donation-year-management/donation-year-management.component";
import {DonationBooktypeComponent} from "./donation-booktype/donation-booktype.component";
import {DonationDonorProfileUpdateComponent} from "./donation-donor-profile-update/donation-donor-profile-update.component";
import {DonationDeleteReceiptComponent} from "./donation-delete-receipt/donation-delete-receipt.component";
import {DonationAreaManagementService} from "./donation-area-management/donation-area-management.service";
import {Md2Module} from "md2";
import {Ng2PaginationModule} from "ng2-pagination";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    Md2Module,
      FormsModule,
      ReactiveFormsModule,
    DonationRoutingModule,
      Ng2PaginationModule
  ],
  declarations: [
    DonationComponent,
    DonationDashboardComponent,
    DonationAreaManagementComponent,
    DonationYearManagementComponent,
    DonationBooktypeComponent,
    DonationDonorProfileUpdateComponent,
    DonationDeleteReceiptComponent
  ],
  providers:[
      DonationAreaManagementService
  ]
})

export class DonationModule {

}
