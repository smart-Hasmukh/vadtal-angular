import {Component, OnInit} from "@angular/core";
import {TitleConstants} from "../utility/constants";
import {Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {HeaderBreadcrumbService} from "../header/header-breadcrumb.service";
import {UserService} from "../sharedService/user.service";

@Component({
    selector: 'app-donation-home',
    templateUrl: './donation.component.html',
    styleUrls: ['./donation.component.css']
})
export class DonationComponent implements OnInit {
    titleConstants = new TitleConstants();

    constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute : ActivatedRoute, private router : Router, private userService : UserService) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.DONATION);
    }

}
