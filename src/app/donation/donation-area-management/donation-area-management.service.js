"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var DonationAreaManagementService = (function () {
    function DonationAreaManagementService(httpService, toastManager) {
        this.httpService = httpService;
        this.toastManager = toastManager;
    }
    DonationAreaManagementService.prototype.getAreaList = function (pageIndex) {
        var _this = this;
        return this.httpService.get(constants_1.ApiEventConstants.AREA + "?pageNumber=" + pageIndex).map(function (res) { return _this.extractData(res, false); });
    };
    DonationAreaManagementService.prototype.extractData = function (res, showToast) {
        if (showToast) {
            this.toastManager.success(res.json().message);
        }
        var data = res.json();
        return data || {};
    };
    DonationAreaManagementService.prototype.addArea = function (values) {
        var _this = this;
        return this.httpService
            .post(constants_1.ApiEventConstants.AREA, values)
            .map(function (res) { return _this.extractData(res, true); });
    };
    DonationAreaManagementService.prototype.editArea = function (areaId, values) {
        var _this = this;
        return this.httpService
            .put(constants_1.ApiEventConstants.AREA + "/" + areaId + "/", values)
            .map(function (res) { return _this.extractData(res, true); });
    };
    DonationAreaManagementService = __decorate([
        core_1.Injectable()
    ], DonationAreaManagementService);
    return DonationAreaManagementService;
}());
exports.DonationAreaManagementService = DonationAreaManagementService;
//# sourceMappingURL=donation-area-management.service.js.map