import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {TitleConstants, HttpStatusConstants, APPConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Area} from "./donation-area.model";
import {DonationAreaManagementService} from "./donation-area-management.service";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {Pager} from "../../templemanagement/pager.model";
import {Md2Module} from "md2";
import any = jasmine.any;

@Component({
  selector: 'app-donation-area-management',
  templateUrl: './donation-area-management.component.html',
  styleUrls: ['./donation-area-management.component.css'],
  providers:[DonationAreaManagementService]
})
export class DonationAreaManagementComponent implements OnInit {
  titleConstants = new TitleConstants();
  appConstant = new APPConstants();
  position: string = 'before';
  areaList: Area[];
  pager: Pager;
  totalCount: number;
  isFormInvalid: boolean;
  isFormSubmitted: boolean;
  areaForm: FormGroup;
  selectedArea: Area;
  toggle: boolean;
  checkedAreaId: number[] = [];

  constructor(private donationAreaManagementService: DonationAreaManagementService, private title: Title,
              private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router, private builder: FormBuilder) {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
      this.toggle = false;
    })
  }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.AREA_LIST);
    this.getArea(1);
    this.initialize();
  }

  initialize() {
    this.isFormSubmitted = false
    this.createAreaForm();
  }

  createAreaForm() {
    this.isFormInvalid = true
    this.areaForm = this.builder.group({
      name: new FormControl('', <any>Validators.required)
    })
  }

  getArea(pageIndex: number) {
    this.donationAreaManagementService.getAreaList(pageIndex).subscribe(response => {
      if (response.status == HttpStatusConstants.SUCCESS) {
        // this.eventCount = response['count'];
        this.pager = response.pager;
        this.areaList = response.payload.data;
        this.totalCount = this.pager.totalRecords;
        this.appConstant.PAGINATION_SIZE = this.pager.recordsPerPage;
        this.toggle = false;
        this.checkedAreaId = [];
      }
    })
  }

  saveArea(value: any, isFormValid: boolean, dialog: any) {
    this.isFormSubmitted = true;
    this.isFormInvalid = isFormValid;
    if (isFormValid) {
      if (this.selectedArea == null) {
        this.donationAreaManagementService.addArea(value).subscribe(result => {
          this.initialize()
        })
      } else {
        let body = {
          "name": value.name,
        }
        this.donationAreaManagementService.editArea(this.selectedArea.id, body).subscribe(result => {
          this.initialize()
          this.selectedArea = null;
          dialog.close();
        })
      }
    }
  }

  show(dialog: any, areaData) {
    this.initialize();
    dialog.open();
    if (areaData) {
      this.selectedArea = areaData;
      this.isFormInvalid = true
      this.areaForm = this.builder.group({
        name: new FormControl(areaData.name, <any>Validators.required)
      })
    }
  }

  toggleArea(area) {
    area.checked = !area.checked
    if(!area.checked){
      let idValue = area.id;
      let ind = this.checkedAreaId.indexOf(idValue);
      this.checkedAreaId.splice(ind,1);
    }else{
      let idVal = area.id;
      this.checkedAreaId.push(idVal);
    }
    this.toggle = this.areaList.every(area => area.checked)
  }

  close(dialog: any) {
    dialog.close();
  }

  toggleAll() {
    this.toggle = !this.toggle
    this.areaList.forEach(area => area.checked = this.toggle)
    if(this.toggle){
      for (let i = 0; i < this.areaList.length; ++i) {
        let idVal = this.areaList[i]["id"];
        if(this.checkedAreaId.length == 0){
          this.checkedAreaId.push(idVal);
        }else{
          let ind = this.checkedAreaId.indexOf(idVal);
          if(ind == -1){
            this.checkedAreaId.push(idVal);
          }
        }
      }
    }else{
      this.checkedAreaId = [];
    }
  }
}
