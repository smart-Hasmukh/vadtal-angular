export class Area{
    get checked(): boolean {
        return this._checked;
    }

    set checked(value: boolean) {
        this._checked = value;
    }

    private  _areaId : number;
    private name : string;
    private isEnable :number;
    private _id:number;
    private _checked:boolean;

    get areaId(): number {
        return this._areaId;
    }

    set areaId(value: number) {
        this._areaId = value;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
}