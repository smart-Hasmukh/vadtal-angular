import { Injectable } from '@angular/core';
import {HttpService} from "../../utility/http-service";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../../utility/constants";
import {ToastsManager} from "ng2-toastr";
import {Response} from "@angular/http";

@Injectable()
export class DonationAreaManagementService {

    constructor(private  httpService: HttpService,private toastManager : ToastsManager) {}

    getAreaList(pageIndex : number): Observable<any> {
        return this.httpService.get(ApiEventConstants.AREA+"?pageNumber="+pageIndex).map(res => this.extractData(res, false));
    }

    private extractData(res: Response, showToast: boolean) {
        if (showToast) {
            this.toastManager.success(res.json().message)
        }
        let data = res.json();
        return data || {};
    }

    addArea(values): Observable<any> {
        return this.httpService
            .post(ApiEventConstants.AREA, values)
            .map(res => this.extractData(res, true));
    }

    editArea(areaId, values): Observable<any> {
        return this.httpService
            .put(ApiEventConstants.AREA +"/"+ areaId+"/", values)
            .map(res => this.extractData(res, true));
    }
}
