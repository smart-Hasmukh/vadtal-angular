"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../../utility/constants");
var donation_area_management_service_1 = require("./donation-area-management.service");
var forms_1 = require("@angular/forms");
var DonationAreaManagementComponent = (function () {
    function DonationAreaManagementComponent(donationAreaManagementService, title, breadcrumbService, activatedRoute, router, builder) {
        var _this = this;
        this.donationAreaManagementService = donationAreaManagementService;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.builder = builder;
        this.titleConstants = new constants_1.TitleConstants();
        this.appConstant = new constants_1.APPConstants();
        this.position = 'before';
        this.checkedAreaId = [];
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
            _this.toggle = false;
        });
    }
    DonationAreaManagementComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.AREA_LIST);
        this.getArea(1);
        this.initialize();
    };
    DonationAreaManagementComponent.prototype.initialize = function () {
        this.isFormSubmitted = false;
        this.createAreaForm();
    };
    DonationAreaManagementComponent.prototype.createAreaForm = function () {
        this.isFormInvalid = true;
        this.areaForm = this.builder.group({
            name: new forms_1.FormControl('', forms_1.Validators.required)
        });
    };
    DonationAreaManagementComponent.prototype.getArea = function (pageIndex) {
        var _this = this;
        this.donationAreaManagementService.getAreaList(pageIndex).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                // this.eventCount = response['count'];
                _this.pager = response.pager;
                _this.areaList = response.payload.data;
                _this.totalCount = _this.pager.totalRecords;
                _this.appConstant.PAGINATION_SIZE = _this.pager.recordsPerPage;
                _this.toggle = false;
                _this.checkedAreaId = [];
            }
        });
    };
    DonationAreaManagementComponent.prototype.saveArea = function (value, isFormValid, dialog) {
        var _this = this;
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid) {
            if (this.selectedArea == null) {
                this.donationAreaManagementService.addArea(value).subscribe(function (result) {
                    _this.initialize();
                });
            }
            else {
                var body = {
                    "name": value.name
                };
                this.donationAreaManagementService.editArea(this.selectedArea.id, body).subscribe(function (result) {
                    _this.initialize();
                    _this.selectedArea = null;
                    dialog.close();
                });
            }
        }
    };
    DonationAreaManagementComponent.prototype.show = function (dialog, areaData) {
        this.initialize();
        dialog.open();
        if (areaData) {
            this.selectedArea = areaData;
            this.isFormInvalid = true;
            this.areaForm = this.builder.group({
                name: new forms_1.FormControl(areaData.name, forms_1.Validators.required)
            });
        }
    };
    DonationAreaManagementComponent.prototype.toggleArea = function (area) {
        area.checked = !area.checked;
        if (!area.checked) {
            var idValue = area.id;
            var ind = this.checkedAreaId.indexOf(idValue);
            this.checkedAreaId.splice(ind, 1);
        }
        else {
            var idVal = area.id;
            this.checkedAreaId.push(idVal);
        }
        this.toggle = this.areaList.every(function (area) { return area.checked; });
    };
    DonationAreaManagementComponent.prototype.close = function (dialog) {
        dialog.close();
    };
    DonationAreaManagementComponent.prototype.toggleAll = function () {
        var _this = this;
        this.toggle = !this.toggle;
        this.areaList.forEach(function (area) { return area.checked = _this.toggle; });
        if (this.toggle) {
            for (var i = 0; i < this.areaList.length; ++i) {
                var idVal = this.areaList[i]["id"];
                if (this.checkedAreaId.length == 0) {
                    this.checkedAreaId.push(idVal);
                }
                else {
                    var ind = this.checkedAreaId.indexOf(idVal);
                    if (ind == -1) {
                        this.checkedAreaId.push(idVal);
                    }
                }
            }
        }
        else {
            this.checkedAreaId = [];
        }
    };
    DonationAreaManagementComponent = __decorate([
        core_1.Component({
            selector: 'app-donation-area-management',
            templateUrl: './donation-area-management.component.html',
            styleUrls: ['./donation-area-management.component.css'],
            providers: [donation_area_management_service_1.DonationAreaManagementService]
        })
    ], DonationAreaManagementComponent);
    return DonationAreaManagementComponent;
}());
exports.DonationAreaManagementComponent = DonationAreaManagementComponent;
//# sourceMappingURL=donation-area-management.component.js.map