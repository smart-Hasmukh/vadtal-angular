"use strict";
var Area = (function () {
    function Area() {
    }
    Object.defineProperty(Area.prototype, "checked", {
        get: function () {
            return this._checked;
        },
        set: function (value) {
            this._checked = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Area.prototype, "areaId", {
        get: function () {
            return this._areaId;
        },
        set: function (value) {
            this._areaId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Area.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    return Area;
}());
exports.Area = Area;
//# sourceMappingURL=donation-area.model.js.map