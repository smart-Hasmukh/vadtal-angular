"use strict";
/* tslint:disable:no-unused-variable */
var testing_1 = require('@angular/core/testing');
var donation_donor_profile_update_component_1 = require('./donation-donor-profile-update.component');
describe('DonationDonorProfileUpdateComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [donation_donor_profile_update_component_1.DonationDonorProfileUpdateComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(donation_donor_profile_update_component_1.DonationDonorProfileUpdateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=donation-donor-profile-update.component.spec.js.map