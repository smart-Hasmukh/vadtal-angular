import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {TitleConstants} from "../../utility/constants";
import {ActivatedRoute, Router} from "@angular/router";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";

@Component({
  selector: 'app-donation-donor-profile-update',
  templateUrl: './donation-donor-profile-update.component.html',
  styleUrls: ['./donation-donor-profile-update.component.css']
})
export class DonationDonorProfileUpdateComponent implements OnInit {
  titleConstants = new TitleConstants();

  constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
    })
  }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.DONATION_DONOR_PROFILE_UPDATE);
  }

}
