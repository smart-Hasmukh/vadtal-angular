"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var donation_component_1 = require("./donation.component");
var donation_routing_module_1 = require("./donation-routing.module");
var material_1 = require("@angular/material");
var donation_dashboard_component_1 = require("./donation-dashboard/donation-dashboard.component");
var donation_area_management_component_1 = require("./donation-area-management/donation-area-management.component");
var donation_year_management_component_1 = require("./donation-year-management/donation-year-management.component");
var donation_booktype_component_1 = require("./donation-booktype/donation-booktype.component");
var donation_donor_profile_update_component_1 = require("./donation-donor-profile-update/donation-donor-profile-update.component");
var donation_delete_receipt_component_1 = require("./donation-delete-receipt/donation-delete-receipt.component");
var donation_area_management_service_1 = require("./donation-area-management/donation-area-management.service");
var md2_1 = require("md2");
var ng2_pagination_1 = require("ng2-pagination");
var forms_1 = require("@angular/forms");
var DonationModule = (function () {
    function DonationModule() {
    }
    DonationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                md2_1.Md2Module,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                donation_routing_module_1.DonationRoutingModule,
                ng2_pagination_1.Ng2PaginationModule
            ],
            declarations: [
                donation_component_1.DonationComponent,
                donation_dashboard_component_1.DonationDashboardComponent,
                donation_area_management_component_1.DonationAreaManagementComponent,
                donation_year_management_component_1.DonationYearManagementComponent,
                donation_booktype_component_1.DonationBooktypeComponent,
                donation_donor_profile_update_component_1.DonationDonorProfileUpdateComponent,
                donation_delete_receipt_component_1.DonationDeleteReceiptComponent
            ],
            providers: [
                donation_area_management_service_1.DonationAreaManagementService
            ]
        })
    ], DonationModule);
    return DonationModule;
}());
exports.DonationModule = DonationModule;
//# sourceMappingURL=donation.module.js.map