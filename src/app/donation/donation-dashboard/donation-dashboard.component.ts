import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {TitleConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-donation-dashboard',
    templateUrl: './donation-dashboard.component.html',
    styleUrls: ['./donation-dashboard.component.css']
})
export class DonationDashboardComponent implements OnInit {
    titleConstants = new TitleConstants();

    constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.DONATION);
    }


}
