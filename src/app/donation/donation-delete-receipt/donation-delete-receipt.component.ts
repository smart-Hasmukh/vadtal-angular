import {Component, OnInit} from '@angular/core';
import {TitleConstants} from "../../utility/constants";
import {Title} from "@angular/platform-browser";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-donation-delete-receipt',
    templateUrl: './donation-delete-receipt.component.html',
    styleUrls: ['./donation-delete-receipt.component.css']
})
export class DonationDeleteReceiptComponent implements OnInit {
    titleConstants = new TitleConstants();

    constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.DONATION_DELETE_RECEIPT);
    }

}
