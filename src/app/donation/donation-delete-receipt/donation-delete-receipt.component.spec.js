"use strict";
/* tslint:disable:no-unused-variable */
var testing_1 = require('@angular/core/testing');
var donation_delete_receipt_component_1 = require('./donation-delete-receipt.component');
describe('DonationDeleteReceiptComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [donation_delete_receipt_component_1.DonationDeleteReceiptComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(donation_delete_receipt_component_1.DonationDeleteReceiptComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=donation-delete-receipt.component.spec.js.map