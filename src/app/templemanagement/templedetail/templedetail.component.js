"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
require('rxjs/add/operator/switchMap');
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var TempledetailComponent = (function () {
    function TempledetailComponent(title, templeService, breadcrumbService, activatedRoute, router, userService, builder, countryService, stateService, cityService) {
        var _this = this;
        this.title = title;
        this.templeService = templeService;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.userService = userService;
        this.builder = builder;
        this.countryService = countryService;
        this.stateService = stateService;
        this.cityService = cityService;
        this.titleConstants = new constants_1.TitleConstants();
        this.appConstants = new constants_1.APPConstants();
        this.countryList = [];
        this.stateList = [];
        this.cityList = [];
        this.activeBtnValue = "Activate";
        this.suspendBtnValue = "Suspend";
        this.position = 'below';
        this.delay = 0;
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
        });
    }
    TempledetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.title.setTitle(this.titleConstants.TEMPLE_DETAIL);
        this.activatedRoute.queryParams.subscribe(function (data) { return _this.getTemple(data['id']); });
        this.initialize();
    };
    TempledetailComponent.prototype.initialize = function () {
        this.isFormSubmitted = false;
        this.createTempleForm();
        this.getCountryList();
    };
    //create module driven validation
    TempledetailComponent.prototype.createTempleForm = function () {
        this.isFormInvalid = true;
        this.templeForm = this.builder.group({
            templeName: new forms_1.FormControl('', forms_1.Validators.required),
            templeShortName: new forms_1.FormControl('', forms_1.Validators.required),
            templeAddress: new forms_1.FormControl('', forms_1.Validators.required),
            templeCountry: new forms_1.FormControl('', forms_1.Validators.required),
            templeState: new forms_1.FormControl('', forms_1.Validators.required),
            templePincode: new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, TempledetailComponent.validatePincode])),
            templeEmailAddress: new forms_1.FormControl('', []),
            templeLandline: new forms_1.FormControl('', []),
            templePhone: new forms_1.FormControl('', [])
        });
    };
    TempledetailComponent.prototype.setTempleForm = function () {
        debugger;
        this.selectedCity = this.temple.city;
        this.templeForm.setValue({
            templeName: this.temple.name,
            templeShortName: this.temple.aliasName,
            templeAddress: this.temple.address,
            templeCountry: this.temple.country.id,
            templeState: this.temple.state.id,
            templePincode: this.temple.pincode,
            templeEmailAddress: this.temple.email,
            templeLandline: this.temple.landlineNo,
            templePhone: this.temple.mobileNo
        });
    };
    //get Location for Event
    TempledetailComponent.prototype.getCountryList = function () {
        var _this = this;
        var searchParams = '?records=all';
        this.countryService.getCountryList(searchParams).subscribe(function (response) {
            _this.countryList = response.payload.data;
        });
    };
    TempledetailComponent.prototype.getStateList = function (countryId) {
        var _this = this;
        var searchParams = '?country=' + (+countryId) + '&records=all';
        this.stateService.getStateList(searchParams).subscribe(function (response) {
            _this.stateList = response.payload.data;
        });
    };
    TempledetailComponent.prototype.getCityList = function (stateId) {
        var _this = this;
        var searchParams = '?state=' + (+stateId) + '&records=all';
        this.cityService.getCityList(searchParams).subscribe(function (response) {
            _this.cityList = response.payload.data;
        });
    };
    TempledetailComponent.prototype.onCityChange = function (city) {
        this.selectedCity = city;
    };
    TempledetailComponent.prototype.getTemple = function (templeId) {
        var _this = this;
        this.templeService.getTemple(templeId).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                _this.temple = response.payload.data[0];
                if (_this.temple.isEnable == 1) {
                    _this.activeBtnValue = "Active";
                    _this.suspendBtnValue = "Suspend";
                }
                else {
                    _this.activeBtnValue = "Activate";
                    _this.suspendBtnValue = "Suspended";
                }
                _this.getStateList(_this.temple.country.id);
                _this.getCityList(_this.temple.state.id);
            }
        });
    };
    TempledetailComponent.prototype.saveTemple = function (value, isFormValid) {
        var _this = this;
        console.log(value);
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid && this.selectedCity != null) {
            var body = {
                "templeName": value.templeName,
                "templeShortName": value.templeShortName,
                "templeAddress": value.templeAddress,
                "templeCountry": value.templeCountry,
                "templeState": value.templeState,
                "templeCity": this.selectedCity.id,
                "templePincode": value.templePincode,
                "templeEmailAddress": value.templeEmailAddress,
                "templeLandline": value.templeLandline,
                "templePhone": value.templePhone
            };
            this.templeService.editTemple(this.temple.id, body).subscribe(function (response) {
                if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                    _this.temple = response.payload.data[0];
                    if (_this.temple.isEnable == 1) {
                        _this.activeBtnValue = "Active";
                        _this.suspendBtnValue = "Suspend";
                    }
                    else {
                        _this.activeBtnValue = "Activate";
                        _this.suspendBtnValue = "Suspended";
                    }
                }
            });
        }
    };
    TempledetailComponent.prototype.activateTemple = function () {
        var _this = this;
        if (this.temple.isEnable == 0) {
            var options = new http_1.RequestOptions({
                search: new http_1.URLSearchParams('status=1')
            });
            this.templeService.activateOrSupendTemple(this.temple.id, options).subscribe(function (response) {
                console.log(response);
                if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                    _this.temple.isEnable = 1;
                    _this.activeBtnValue = "Active";
                    _this.suspendBtnValue = "Suspend";
                }
            });
        }
    };
    TempledetailComponent.prototype.suspendTemple = function () {
        var _this = this;
        if (this.temple.isEnable == 1) {
            var options = new http_1.RequestOptions({
                search: new http_1.URLSearchParams('status=0')
            });
            this.templeService.activateOrSupendTemple(this.temple.id, options).subscribe(function (response) {
                if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                    _this.temple.isEnable = 0;
                    _this.activeBtnValue = "Activate";
                    _this.suspendBtnValue = "Suspended";
                }
            });
        }
    };
    TempledetailComponent.validatePincode = function (control) {
        var pincodeLength = String(control.value).length;
        if (control.value == null || pincodeLength != 6) {
            return { templePincode: false };
        }
        else {
            return null;
        }
    };
    TempledetailComponent.prototype.hoverEffectClass = function () {
        if (!this.isCollapsed) {
            return "contactHover";
        }
        else {
            return "";
        }
    };
    // temple details model methods
    TempledetailComponent.prototype.showEditTempleDetails = function (dialog) {
        dialog.show();
    };
    TempledetailComponent.prototype.closeEditTempleDetails = function (dialog) {
        dialog.close();
    };
    TempledetailComponent = __decorate([
        core_1.Component({
            selector: 'app-templedetail',
            templateUrl: './templedetail.component.html',
            styleUrls: ['./templedetail.component.css']
        })
    ], TempledetailComponent);
    return TempledetailComponent;
}());
exports.TempledetailComponent = TempledetailComponent;
//# sourceMappingURL=templedetail.component.js.map