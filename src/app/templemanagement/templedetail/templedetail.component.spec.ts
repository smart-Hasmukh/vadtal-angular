/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TempledetailComponent } from './templedetail.component';

describe('TempledetailComponent', () => {
  let component: TempledetailComponent;
  let fixture: ComponentFixture<TempledetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempledetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempledetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
