import {Component, OnInit, Input} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {TitleConstants, HttpStatusConstants, APPConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router, RouterStateSnapshot, Params} from "@angular/router";
import {Temple} from "../temple.model";
import {TempleService} from "../temples/temple.service";
import 'rxjs/add/operator/switchMap';
import {Observable} from "rxjs";
import {RequestOptions, Headers, URLSearchParams} from "@angular/http";
import {UserService} from "../../sharedService/user.service";
import {StateService} from "../../state/state.service";
import {CityService} from "../../city/city.service";
import {CountryService} from "../../country/country.service";
import {FormGroup, Validators, FormControl, FormBuilder} from "@angular/forms";
import {TemplesComponent} from "../temples/temples.component";
import {Country} from "../../country/country.model";
import {State} from "../../state/state.model";
import {City} from "../../city/city.model";

@Component({
    selector: 'app-templedetail',
    templateUrl: './templedetail.component.html',
    styleUrls: ['./templedetail.component.css']
})
export class TempledetailComponent implements OnInit {
    temple: Temple;
    titleConstants = new TitleConstants();
    appConstants = new APPConstants()
    isFormInvalid: boolean;
    isFormSubmitted: boolean;
    templeForm: FormGroup;
    countryList: Country[] = [];
    stateList: State[] = [];
    cityList: City[] = [];
    selectedCity: City;
    activeBtnValue = "Activate";
    suspendBtnValue = "Suspend";
    position: string = 'below';
    delay: number = 0;


    constructor(private title: Title, private templeService: TempleService, private breadcrumbService: HeaderBreadcrumbService,
                private activatedRoute: ActivatedRoute, private router: Router,private userService : UserService,
                private builder: FormBuilder, private countryService: CountryService, private stateService: StateService,
                private cityService: CityService) {

        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.TEMPLE_DETAIL);
        this.activatedRoute.queryParams.subscribe(data =>  this.getTemple(data['id']));
        this.initialize()
    }

    initialize() {
        this.isFormSubmitted = false
        this.createTempleForm();
        this.getCountryList();
    }

    //create module driven validation
    createTempleForm() {
        this.isFormInvalid = true
        this.templeForm = this.builder.group({
            templeName: new FormControl('', <any>Validators.required),
            templeShortName: new FormControl('', <any>Validators.required),
            templeAddress: new FormControl('', <any>Validators.required),
            templeCountry: new FormControl('', <any>Validators.required),
            templeState: new FormControl('', <any>Validators.required),
            templePincode: new FormControl('', Validators.compose([<any>Validators.required, TempledetailComponent.validatePincode])),
            templeEmailAddress: new FormControl('', []),
            templeLandline: new FormControl('', []),
            templePhone: new FormControl('', []),
        })
    }

    setTempleForm(){
        debugger
        this.selectedCity = this.temple.city
        this.templeForm.setValue({
            templeName  : this.temple.name,
            templeShortName: this.temple.aliasName,
            templeAddress: this.temple.address,
            templeCountry: this.temple.country.id,
            templeState: this.temple.state.id,
            templePincode: this.temple.pincode,
            templeEmailAddress: this.temple.email,
            templeLandline: this.temple.landlineNo,
            templePhone: this.temple.mobileNo,
        })
    }

    //get Location for Event
    getCountryList() {
        let searchParams = '?records=all'
        this.countryService.getCountryList(searchParams).subscribe(response => {
            this.countryList = response.payload.data
        })
    }

    getStateList(countryId : number) {
        let searchParams = '?country=' + (+countryId)+'&records=all'
        this.stateService.getStateList(searchParams).subscribe(response => {
            this.stateList = response.payload.data
        })
    }

    getCityList(stateId :number) {
        let searchParams = '?state=' + (+stateId)+'&records=all'
        this.cityService.getCityList(searchParams).subscribe(response => {
            this.cityList = response.payload.data

        })

    }

    onCityChange(city: City) {
        this.selectedCity = city
    }


    getTemple(templeId: number) {
        this.templeService.getTemple(templeId).subscribe(response => {
            if (response.status == HttpStatusConstants.SUCCESS) {
                this.temple = response.payload.data[0];
                if (this.temple.isEnable == 1) {
                    this.activeBtnValue = "Active";
                    this.suspendBtnValue = "Suspend";
                } else {
                    this.activeBtnValue = "Activate";
                    this.suspendBtnValue = "Suspended";
                }
                this.getStateList(this.temple.country.id)
                this.getCityList(this.temple.state.id)

            }
        })
    }

    saveTemple(value: any, isFormValid: boolean) {
        console.log(value);
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid && this.selectedCity != null) {
            let body = {
                "templeName": value.templeName,
                "templeShortName": value.templeShortName,
                "templeAddress": value.templeAddress,
                "templeCountry": value.templeCountry,
                "templeState": value.templeState,
                "templeCity": this.selectedCity.id,
                "templePincode": value.templePincode,
                "templeEmailAddress": value.templeEmailAddress,
                "templeLandline": value.templeLandline,
                "templePhone": value.templePhone
            }
            this.templeService.editTemple(this.temple.id, body).subscribe(response => {
                if (response.status == HttpStatusConstants.SUCCESS) {
                    this.temple = response.payload.data[0];
                    if (this.temple.isEnable == 1) {
                        this.activeBtnValue = "Active";
                        this.suspendBtnValue = "Suspend";
                    } else {
                        this.activeBtnValue = "Activate";
                        this.suspendBtnValue = "Suspended";
                    }
                }
            })
        }
    }

    activateTemple(){
        if (this.temple.isEnable == 0) {
            let options = new RequestOptions({
                search: new URLSearchParams('status=1')
            });
            this.templeService.activateOrSupendTemple(this.temple.id,options).subscribe(response => {
                console.log(response)
                if (response.status == HttpStatusConstants.SUCCESS) {
                    this.temple.isEnable = 1
                    this.activeBtnValue = "Active";
                    this.suspendBtnValue = "Suspend";
                }
            })
        }
    }

    suspendTemple(){
        if (this.temple.isEnable == 1) {
            let options = new RequestOptions({
                search: new URLSearchParams('status=0')
            });
            this.templeService.activateOrSupendTemple(this.temple.id,options).subscribe(response => {
                if (response.status == HttpStatusConstants.SUCCESS) {
                    this.temple.isEnable = 0
                    this.activeBtnValue = "Activate";
                    this.suspendBtnValue = "Suspended";

                }
            })
        }
    }

    static validatePincode(control: FormControl): {[key: string]: boolean} {
        let pincodeLength = String(control.value).length
        if (control.value == null || pincodeLength != 6) {
            return {templePincode: false};
        } else {
            return null;
        }
    }

    // Add class hoverEffect on contact details
    isCollapsed;
    hoverEffectClass() {
        if(!this.isCollapsed){
            return "contactHover";
        }else{
            return "";
        }
    }

    // temple details model methods
    showEditTempleDetails(dialog: any){
        dialog.show();
    }

    closeEditTempleDetails(dialog: any){
        dialog.close();
    }
}
