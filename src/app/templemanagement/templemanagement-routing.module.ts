import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {TemplemanagementComponent} from "./templemanagement.component";
import {TemplesComponent} from "./temples/temples.component";
import {TempledetailComponent} from "./templedetail/templedetail.component";
import {RouteConstants} from "../utility/constants";
import {TempleuserComponent} from "./templeuser/templeuser.component";

const routes: Routes = [
    {
        path: '',
        component: TemplemanagementComponent,
        children: [
            {
                path: '',
                component: TemplesComponent,
                data: {title: "Temples"}
            },
            {
                path: RouteConstants.APP_TEMPLE_DETAIL,
                component: TempledetailComponent,
                data: {title: "Temple Detail"}
            },
            {
                path: RouteConstants.APP_TEMPLE_USER,
                component: TempleuserComponent,
                data: {title: "Users"}
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TempleManagementRoutingModule {

}