import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "@angular/material";
import {TemplemanagementComponent} from "./templemanagement.component";
import {TempleManagementRoutingModule} from "./templemanagement-routing.module";
import {TemplesComponent} from "./temples/temples.component";
import {Md2Module} from "md2";
import {TempledetailComponent} from "./templedetail/templedetail.component";
import {TempleuserComponent} from "./templeuser/templeuser.component";
import {Ng2PaginationModule} from "ng2-pagination";
import {Names} from "./names.pipe";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TempleService} from "./temples/temple.service";
import {UserService} from "../user/user.service";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    Md2Module,
    Ng2PaginationModule,
    TempleManagementRoutingModule
  ],
  declarations: [TemplemanagementComponent, TemplesComponent, TempledetailComponent, TempleuserComponent, Names],
  providers : [TempleService,UserService]
})
export class TemplemanagementModule {
}
