import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {TitleConstants} from "../utility/constants";
import {Router, ActivatedRoute} from "@angular/router";
import {HeaderBreadcrumbService} from "../header/header-breadcrumb.service";

@Component({
  selector: 'app-templemanagement',
  templateUrl: './templemanagement.component.html',
  styleUrls: ['./templemanagement.component.css']
})
export class TemplemanagementComponent implements OnInit {
  titleConstants = new TitleConstants();
  constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
      this.breadcrumbService.hideRoute("/home");
    })
  }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.TEMPLE_MANAGEMENT);
  }

}
