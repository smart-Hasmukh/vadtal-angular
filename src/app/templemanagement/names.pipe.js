"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var directives_1 = require("@angular/core/src/metadata/directives");
var constants_1 = require("../utility/constants");
var Names = (function () {
    function Names() {
        this.appConstants = new constants_1.APPConstants();
    }
    Names.prototype.transform = function (users) {
        var admins = users.map(function (user) { return user.firstName + " " + user.lastName; }).join(', ');
        if (admins == "") {
            return this.appConstants.NOT_AVAILABLE_MESSAGE;
        }
        else {
            return admins;
        }
    };
    Names = __decorate([
        directives_1.Pipe({ name: 'Names' })
    ], Names);
    return Names;
}());
exports.Names = Names;
//# sourceMappingURL=names.pipe.js.map