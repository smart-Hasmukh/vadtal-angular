"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../utility/constants");
var TemplemanagementComponent = (function () {
    function TemplemanagementComponent(title, breadcrumbService, activatedRoute, router) {
        var _this = this;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.titleConstants = new constants_1.TitleConstants();
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
            _this.breadcrumbService.hideRoute("/home");
        });
    }
    TemplemanagementComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.TEMPLE_MANAGEMENT);
    };
    TemplemanagementComponent = __decorate([
        core_1.Component({
            selector: 'app-templemanagement',
            templateUrl: './templemanagement.component.html',
            styleUrls: ['./templemanagement.component.css']
        })
    ], TemplemanagementComponent);
    return TemplemanagementComponent;
}());
exports.TemplemanagementComponent = TemplemanagementComponent;
//# sourceMappingURL=templemanagement.component.js.map