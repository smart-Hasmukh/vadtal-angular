"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var material_1 = require("@angular/material");
var templemanagement_component_1 = require("./templemanagement.component");
var templemanagement_routing_module_1 = require("./templemanagement-routing.module");
var temples_component_1 = require("./temples/temples.component");
var md2_1 = require("md2");
var templedetail_component_1 = require("./templedetail/templedetail.component");
var templeuser_component_1 = require("./templeuser/templeuser.component");
var ng2_pagination_1 = require("ng2-pagination");
var names_pipe_1 = require("./names.pipe");
var forms_1 = require("@angular/forms");
var temple_service_1 = require("./temples/temple.service");
var user_service_1 = require("../user/user.service");
var TemplemanagementModule = (function () {
    function TemplemanagementModule() {
    }
    TemplemanagementModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                md2_1.Md2Module,
                ng2_pagination_1.Ng2PaginationModule,
                templemanagement_routing_module_1.TempleManagementRoutingModule
            ],
            declarations: [templemanagement_component_1.TemplemanagementComponent, temples_component_1.TemplesComponent, templedetail_component_1.TempledetailComponent, templeuser_component_1.TempleuserComponent, names_pipe_1.Names],
            providers: [temple_service_1.TempleService, user_service_1.UserService]
        })
    ], TemplemanagementModule);
    return TemplemanagementModule;
}());
exports.TemplemanagementModule = TemplemanagementModule;
//# sourceMappingURL=templemanagement.module.js.map