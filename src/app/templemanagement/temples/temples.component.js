"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
///<reference path="../../../../node_modules/@angular/core/src/change_detection/pipe_transform.d.ts"/>
var core_1 = require("@angular/core");
var constants_1 = require("../../utility/constants");
var temple_service_1 = require("./temple.service");
var forms_1 = require("@angular/forms");
var TemplesComponent = (function () {
    function TemplesComponent(templeService, title, breadcrumbService, activatedRoute, router, builder, countryService, stateService, cityService) {
        var _this = this;
        this.templeService = templeService;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.builder = builder;
        this.countryService = countryService;
        this.stateService = stateService;
        this.cityService = cityService;
        this.titleConstants = new constants_1.TitleConstants();
        this.appConstant = new constants_1.APPConstants();
        this.countryList = [];
        this.stateList = [];
        this.cityList = [];
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
        });
    }
    TemplesComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.TEMPLE_LIST);
        this.getTemples(1);
        this.initialize();
    };
    TemplesComponent.prototype.initialize = function () {
        this.isFormSubmitted = false;
        this.createTempleForm();
        this.getCountryList();
    };
    //create module driven validation
    TemplesComponent.prototype.createTempleForm = function () {
        this.isFormInvalid = true;
        this.templeForm = this.builder.group({
            templeName: new forms_1.FormControl('', forms_1.Validators.required),
            templeShortName: new forms_1.FormControl('', forms_1.Validators.required),
            templeAddress: new forms_1.FormControl('', forms_1.Validators.required),
            templeCountry: new forms_1.FormControl('', forms_1.Validators.required),
            templeState: new forms_1.FormControl('', forms_1.Validators.required),
            templeCity: new forms_1.FormControl('', forms_1.Validators.required),
            templePincode: new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, TemplesComponent.validatePincode])),
            templeEmailAddress: new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, TemplesComponent.validateEmail])),
            templeLandline: new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, TemplesComponent.validateLandline])),
            templePhone: new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, TemplesComponent.validatePhone]))
        });
    };
    TemplesComponent.prototype.resetTempleForm = function () {
        this.createTempleForm();
    };
    //get Location for Event
    TemplesComponent.prototype.getCountryList = function () {
        var _this = this;
        var searchParams = '?records=all';
        this.countryService.getCountryList(searchParams).subscribe(function (response) {
            _this.countryList = response.payload.data;
        });
    };
    TemplesComponent.prototype.getStateList = function (countryId) {
        var _this = this;
        var searchParams = '?country=' + (+countryId) + '&records=all';
        this.stateService.getStateList(searchParams).subscribe(function (response) {
            _this.stateList = response.payload.data;
        });
    };
    TemplesComponent.prototype.getCityList = function (stateId) {
        var _this = this;
        var searchParams = '?state=' + (+stateId) + '&records=all';
        this.cityService.getCityList(searchParams).subscribe(function (response) {
            _this.cityList = response.payload.data;
        });
    };
    TemplesComponent.prototype.onCityChange = function (city) {
        this.selectedCity = city;
    };
    TemplesComponent.prototype.getTemples = function (pageIndex) {
        var _this = this;
        this.templeService.getTempleList(pageIndex).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                _this.pager = response.pager;
                _this.templeList = response.payload.data;
                _this.totalCount = _this.pager.totalRecords;
                _this.appConstant.PAGINATION_SIZE = _this.pager.recordsPerPage;
            }
        });
    };
    TemplesComponent.prototype.addProject = function () {
        this.createTempleForm();
    };
    TemplesComponent.prototype.saveTemple = function (value, isFormValid) {
        var _this = this;
        console.log(value);
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid && this.selectedCity != null) {
            if (this.selectedTemple == null) {
                value["templeCity"] = this.selectedCity.id;
                this.templeService.addTemple(value).subscribe(function (result) {
                    _this.initialize();
                });
            }
            else {
                var body = {
                    "templeName": value.templeName,
                    "templeShortName": value.templeShortName,
                    "templeAddress": value.templeAddress,
                    "templeCountry": value.templeCountry,
                    "templeState": value.templeState,
                    "templeCity": this.selectedCity.id,
                    "templePincode": value.templePincode,
                    "templeEmailAddress": value.templeEmailAddress,
                    "templeLandline": value.templeLandline,
                    "templePhone": value.templePhone
                };
                this.templeService.editTemple(this.selectedTemple.id, body).subscribe(function (result) {
                    _this.initialize();
                });
            }
        }
    };
    TemplesComponent.validatePincode = function (control) {
        var pincodeLength = String(control.value).length;
        if (control.value == null || pincodeLength != 6) {
            return { templePincode: false };
        }
        else {
            return null;
        }
    };
    TemplesComponent.validatePhone = function (control) {
        var phoneLength = String(control.value).length;
        if (control.value == null || phoneLength < 7 || phoneLength > 14) {
            return { templePhone: false };
        }
        else {
            return null;
        }
    };
    TemplesComponent.validateLandline = function (control) {
        var landlinelength = String(control.value).length;
        if (control.value == null || landlinelength < 7 || landlinelength > 14) {
            return { templeLandline: false };
        }
        else {
            return null;
        }
    };
    TemplesComponent.validateEmail = function (control) {
        var EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { templeEmailAddress: false };
        }
        else {
            return null;
        }
    };
    TemplesComponent.prototype.show = function (dialog) {
        dialog.show();
    };
    TemplesComponent.prototype.close = function (dialog) {
        dialog.close();
    };
    TemplesComponent = __decorate([
        core_1.Component({
            selector: 'app-temples',
            templateUrl: './temples.component.html',
            styleUrls: ['./temples.component.css'],
            providers: [temple_service_1.TempleService]
        })
    ], TemplesComponent);
    return TemplesComponent;
}());
exports.TemplesComponent = TemplesComponent;
//# sourceMappingURL=temples.component.js.map