import { Injectable } from '@angular/core';
import {HttpService} from "../../utility/http-service";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../../utility/constants";
import {ToastsManager} from "ng2-toastr";
import {Response} from "@angular/http";

@Injectable()
export class TempleService {

  constructor(private  httpService: HttpService,private toastManager : ToastsManager) { }

  getTempleList(pageIndex : number): Observable<any> {
    return this.httpService.get(ApiEventConstants.TEMPLE).map(res => this.extractData(res, false));
  }

  getTemple(templeId : number) :Observable<any> {
    return this.httpService.get(ApiEventConstants.TEMPLE +"/"+ templeId+"/").map(res => this.extractData(res, false));
  }

  addTemple(values): Observable<any> {

    return this.httpService
        .post(ApiEventConstants.TEMPLE, values)
        .map(res => this.extractData(res, true));
  }

  editTemple(templeId, values): Observable<any> {
    return this.httpService
        .put(ApiEventConstants.TEMPLE +"/"+ templeId+"/", values)
        .map(res => this.extractData(res, true));
  }


  activateOrSupendTemple(id,options): Observable<any> {
    return this.httpService
        .delete(ApiEventConstants.TEMPLE+"/"+id+"/",options)
        .map(res => this.extractData(res, true));
  }

  private extractData(res: Response, showToast: boolean) {
    if (showToast) {
      this.toastManager.success(res.json().message)
    }
    let data = res.json();
    return data || {};
  }
}
