/* tslint:disable:no-unused-variable */
"use strict";
var testing_1 = require('@angular/core/testing');
var temple_service_1 = require('./temple.service');
describe('TempleService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [temple_service_1.TempleService]
        });
    });
    it('should ...', testing_1.inject([temple_service_1.TempleService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=temple.service.spec.js.map