"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var TempleService = (function () {
    function TempleService(httpService, toastManager) {
        this.httpService = httpService;
        this.toastManager = toastManager;
    }
    TempleService.prototype.getTempleList = function (pageIndex) {
        var _this = this;
        return this.httpService.get(constants_1.ApiEventConstants.TEMPLE).map(function (res) { return _this.extractData(res, false); });
    };
    TempleService.prototype.getTemple = function (templeId) {
        var _this = this;
        return this.httpService.get(constants_1.ApiEventConstants.TEMPLE + "/" + templeId + "/").map(function (res) { return _this.extractData(res, false); });
    };
    TempleService.prototype.addTemple = function (values) {
        var _this = this;
        return this.httpService
            .post(constants_1.ApiEventConstants.TEMPLE, values)
            .map(function (res) { return _this.extractData(res, true); });
    };
    TempleService.prototype.editTemple = function (templeId, values) {
        var _this = this;
        return this.httpService
            .put(constants_1.ApiEventConstants.TEMPLE + "/" + templeId + "/", values)
            .map(function (res) { return _this.extractData(res, true); });
    };
    TempleService.prototype.activateOrSupendTemple = function (id, options) {
        var _this = this;
        return this.httpService
            .delete(constants_1.ApiEventConstants.TEMPLE + "/" + id + "/", options)
            .map(function (res) { return _this.extractData(res, true); });
    };
    TempleService.prototype.extractData = function (res, showToast) {
        if (showToast) {
            this.toastManager.success(res.json().message);
        }
        var data = res.json();
        return data || {};
    };
    TempleService = __decorate([
        core_1.Injectable()
    ], TempleService);
    return TempleService;
}());
exports.TempleService = TempleService;
//# sourceMappingURL=temple.service.js.map