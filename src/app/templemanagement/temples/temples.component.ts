///<reference path="../../../../node_modules/@angular/core/src/change_detection/pipe_transform.d.ts"/>
import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {TitleConstants, HttpStatusConstants, APPConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TempleService} from "./temple.service";
import {Temple} from "../temple.model";
import {Pager} from "../pager.model";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {City} from "../../city/city.model";
import {State} from "../../state/state.model";
import {Country} from "../../country/country.model";
import {CountryService} from "../../country/country.service";
import {CityService} from "../../city/city.service";
import {StateService} from "../../state/state.service";
import {URLSearchParams, RequestOptions} from "@angular/http";


@Component({
    selector: 'app-temples',
    templateUrl: './temples.component.html',
    styleUrls: ['./temples.component.css'],
    providers: [TempleService]
})
export class TemplesComponent implements OnInit {
    titleConstants = new TitleConstants();
    appConstant = new APPConstants();

    templeList: Temple[];
    pager: Pager;
    totalCount: number;
    isFormInvalid: boolean;
    isFormSubmitted: boolean;
    templeForm: FormGroup;
    selectedTemple: Temple;
    countryList: Country[] = [];
    stateList: State[] = [];
    cityList: City[] = [];
    selectedCity: City;

    constructor(private templeService: TempleService, private title: Title, private breadcrumbService: HeaderBreadcrumbService,
                private activatedRoute: ActivatedRoute, private router: Router, private builder: FormBuilder,
                private countryService: CountryService, private stateService: StateService,
                private cityService: CityService) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.TEMPLE_LIST);
        this.getTemples(1);
        this.initialize()
    }

    initialize() {
        this.isFormSubmitted = false
        this.createTempleForm();
        this.getCountryList();
    }

    //create module driven validation
    createTempleForm() {
        this.isFormInvalid = true
        this.templeForm = this.builder.group({
            templeName: new FormControl('', <any>Validators.required),
            templeShortName: new FormControl('', <any>Validators.required),
            templeAddress: new FormControl('', <any>Validators.required),
            templeCountry: new FormControl('', <any>Validators.required),
            templeState: new FormControl('', <any>Validators.required),
            templeCity: new FormControl('', <any>Validators.required),
            templePincode: new FormControl('', Validators.compose([<any>Validators.required, TemplesComponent.validatePincode])),
            templeEmailAddress: new FormControl('', Validators.compose([<any>Validators.required, TemplesComponent.validateEmail])),
            templeLandline: new FormControl('', Validators.compose([<any>Validators.required, TemplesComponent.validateLandline])),
            templePhone: new FormControl('', Validators.compose([<any>Validators.required, TemplesComponent.validatePhone])),
        })

    }

    resetTempleForm(){
        this.createTempleForm();
    }

    //get Location for Event
    getCountryList() {
        let searchParams = '?records=all'
        this.countryService.getCountryList(searchParams).subscribe(response => {
            this.countryList = response.payload.data
        })
    }

    getStateList(countryId : number) {
        let searchParams = '?country=' + (+countryId)+'&records=all'
        this.stateService.getStateList(searchParams).subscribe(response => {
            this.stateList = response.payload.data
        })
    }

    getCityList(stateId :number) {
        let searchParams = '?state=' + (+stateId)+'&records=all'
        this.cityService.getCityList(searchParams).subscribe(response => {
            this.cityList = response.payload.data

        })

    }

    onCityChange(city: City) {
        this.selectedCity = city
    }


    getTemples(pageIndex: number) {
        this.templeService.getTempleList(pageIndex).subscribe(response => {
            if (response.status == HttpStatusConstants.SUCCESS) {
                this.pager = response.pager;
                this.templeList = response.payload.data;
                this.totalCount = this.pager.totalRecords;
                this.appConstant.PAGINATION_SIZE = this.pager.recordsPerPage;
            }
        })
    }

    addProject() {
        this.createTempleForm()
    }

    saveTemple(value: any, isFormValid: boolean) {
        console.log(value);
        this.isFormSubmitted = true;
        this.isFormInvalid = isFormValid;
        if (isFormValid && this.selectedCity != null) {
            if (this.selectedTemple == null) {
                value["templeCity"] = this.selectedCity.id
                this.templeService.addTemple(value).subscribe(result => {
                    this.initialize()
                })
            } else {
                let body = {
                    "templeName": value.templeName,
                    "templeShortName": value.templeShortName,
                    "templeAddress": value.templeAddress,
                    "templeCountry": value.templeCountry,
                    "templeState": value.templeState,
                    "templeCity": this.selectedCity.id,
                    "templePincode": value.templePincode,
                    "templeEmailAddress": value.templeEmailAddress,
                    "templeLandline": value.templeLandline,
                    "templePhone": value.templePhone
                }
                this.templeService.editTemple(this.selectedTemple.id, body).subscribe(result => {
                    this.initialize()
                })
            }
        }
    }

    static validatePincode(control: FormControl): {[key: string]: boolean} {
        let pincodeLength = String(control.value).length
        if (control.value == null || pincodeLength != 6) {
            return {templePincode: false};
        } else {
            return null;
        }
    }

    static validatePhone(control: FormControl): {[key: string]: boolean} {
        let phoneLength = String(control.value).length
        if (control.value == null || phoneLength < 7 || phoneLength > 14) {
            return {templePhone: false};
        } else {
            return null;
        }
    }

    static validateLandline(control: FormControl): {[key: string]: boolean} {
        let landlinelength = String(control.value).length
        if (control.value == null || landlinelength < 7 || landlinelength > 14) {
            return {templeLandline: false};
        } else {
            return null;
        }
    }

    static validateEmail(control: FormControl): {[key: string]: boolean} {
        var EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return {templeEmailAddress: false};
        } else {
            return null;
        }
    }

    show(dialog: any) {
        dialog.show();
    }
    close(dialog: any) {
        dialog.close();
    }
}
