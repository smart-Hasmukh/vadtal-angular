// import {Pager} from "../pager.model";
// export class Temples {
//     private status: number;
//     private message: string;
//     private payload: Privileges;
//     private pager: Pager;
// }
//
// export class Privileges{
//     data : Data[];
// }
//
// export class Data{
//     private name: string;
//     private aliasName: string;
//     private address: string;
//     private countryId: number;
//     private stateId: number;
//     private pincode: number;
//     private email: string;
//     private landlineNo: string;
//     private mobileNo: string;
//     private updatedBy: number;
//     private isEnable: number;
//     private city: City;
//     private state: State;
//     private country: Country;
// }
//
// export class City {
//     private _name: string;
//     private _stateId: number;
//     private _isEnable: number;
//
//     get name(): string {
//         return this._name;
//     }
//
//     set name(value: string) {
//         this._name = value;
//     }
//
//     get stateId(): number {
//         return this._stateId;
//     }
//
//     set stateId(value: number) {
//         this._stateId = value;
//     }
//
//     get isEnable(): number {
//         return this._isEnable;
//     }
//
//     set isEnable(value: number) {
//         this._isEnable = value;
//     }
// }
//
// export class State {
//     private _name: string;
//     private _countryId: number;
//     private _isEnable: number;
//
//     get name(): string {
//         return this._name;
//     }
//
//     set name(value: string) {
//         this._name = value;
//     }
//
//     get countryId(): number {
//         return this._countryId;
//     }
//
//     set countryId(value: number) {
//         this._countryId = value;
//     }
//
//     get isEnable(): number {
//         return this._isEnable;
//     }
//
//     set isEnable(value: number) {
//         this._isEnable = value;
//     }
// }
//
// export class Country {
//     private _name: string;
//     private _phoneCode: string;
//     private _createdBy: number;
//     private _updatedBy: number;
//     private _isEnable: number;
//
//     get name(): string {
//         return this._name;
//     }
//
//     set name(value: string) {
//         this._name = value;
//     }
//
//     get phoneCode(): string {
//         return this._phoneCode;
//     }
//
//     set phoneCode(value: string) {
//         this._phoneCode = value;
//     }
//
//     get createdBy(): number {
//         return this._createdBy;
//     }
//
//     set createdBy(value: number) {
//         this._createdBy = value;
//     }
//
//     get updatedBy(): number {
//         return this._updatedBy;
//     }
//
//     set updatedBy(value: number) {
//         this._updatedBy = value;
//     }
//
//     get isEnable(): number {
//         return this._isEnable;
//     }
//
//     set isEnable(value: number) {
//         this._isEnable = value;
//     }
// }
//
//
//# sourceMappingURL=temples.model.js.map