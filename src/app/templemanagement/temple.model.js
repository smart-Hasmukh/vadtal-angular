"use strict";
var Temple = (function () {
    function Temple() {
    }
    Object.defineProperty(Temple.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "city", {
        get: function () {
            return this._city;
        },
        set: function (value) {
            this._city = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "state", {
        get: function () {
            return this._state;
        },
        set: function (value) {
            this._state = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "country", {
        get: function () {
            return this._country;
        },
        set: function (value) {
            this._country = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "user", {
        get: function () {
            return this._user;
        },
        set: function (value) {
            this._user = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "aliasName", {
        get: function () {
            return this._aliasName;
        },
        set: function (value) {
            this._aliasName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "address", {
        get: function () {
            return this._address;
        },
        set: function (value) {
            this._address = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "countryId", {
        get: function () {
            return this._countryId;
        },
        set: function (value) {
            this._countryId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "stateId", {
        get: function () {
            return this._stateId;
        },
        set: function (value) {
            this._stateId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "cityId", {
        get: function () {
            return this._cityId;
        },
        set: function (value) {
            this._cityId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "pincode", {
        get: function () {
            return this._pincode;
        },
        set: function (value) {
            this._pincode = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            this._email = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "landlineNo", {
        get: function () {
            return this._landlineNo;
        },
        set: function (value) {
            this._landlineNo = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "mobileNo", {
        get: function () {
            return this._mobileNo;
        },
        set: function (value) {
            this._mobileNo = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "updatedBy", {
        get: function () {
            return this._updatedBy;
        },
        set: function (value) {
            this._updatedBy = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "isEnable", {
        get: function () {
            return this._isEnable;
        },
        set: function (value) {
            this._isEnable = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Temple.prototype, "image", {
        get: function () {
            return this._image;
        },
        set: function (value) {
            this._image = value;
        },
        enumerable: true,
        configurable: true
    });
    return Temple;
}());
exports.Temple = Temple;
//# sourceMappingURL=temple.model.js.map