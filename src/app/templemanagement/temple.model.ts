import {City} from "../city/city.model";
import {State} from "../state/state.model";
import {Country} from "../country/country.model";
import {User} from "../user/user.model";

export class Temple {


    private _id:number;
    private _name: string;
    private _aliasName: string;
    private _image: string;
    private _address: string;
    private _countryId: number;
    private _stateId: number;
    private _cityId: number;
    private _pincode: string;
    private _email: string;
    private _landlineNo: string;
    private _mobileNo: string;
    private _updatedBy: string;
    private _isEnable: number;
    private _city: City;
    private _state: State;
    private _country: Country;
    private _user: User[];


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get city(): City {
        return this._city;
    }

    set city(value: City) {
        this._city = value;
    }

    get state(): State {
        return this._state;
    }

    set state(value: State) {
        this._state = value;
    }

    get country(): Country {
        return this._country;
    }

    set country(value: Country) {
        this._country = value;
    }

    get user(): User[] {
        return this._user;
    }

    set user(value: User[]) {
        this._user = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get aliasName(): string {
        return this._aliasName;
    }

    set aliasName(value: string) {
        this._aliasName = value;
    }

    get address(): string {
        return this._address;
    }

    set address(value: string) {
        this._address = value;
    }

    get countryId(): number {
        return this._countryId;
    }

    set countryId(value: number) {
        this._countryId = value;
    }

    get stateId(): number {
        return this._stateId;
    }

    set stateId(value: number) {
        this._stateId = value;
    }

    get cityId(): number {
        return this._cityId;
    }

    set cityId(value: number) {
        this._cityId = value;
    }

    get pincode(): string {
        return this._pincode;
    }

    set pincode(value: string) {
        this._pincode = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get landlineNo(): string {
        return this._landlineNo;
    }

    set landlineNo(value: string) {
        this._landlineNo = value;
    }

    get mobileNo(): string {
        return this._mobileNo;
    }

    set mobileNo(value: string) {
        this._mobileNo = value;
    }

    get updatedBy(): string {
        return this._updatedBy;
    }

    set updatedBy(value: string) {
        this._updatedBy = value;
    }

    get isEnable(): number {
        return this._isEnable;
    }

    set isEnable(value: number) {
        this._isEnable = value;
    }

    get image(): string {
        return this._image;
    }

    set image(value: string) {
        this._image = value;
    }
}



