import {Pipe} from "@angular/core/src/metadata/directives";
import {PipeTransform} from "@angular/core";
import {User} from "../user/user.model";
import {APPConstants} from "../utility/constants";

@Pipe({name: 'Names'})
export class Names implements PipeTransform {
    appConstants = new APPConstants()
    transform(users: User[]): string {
        let admins = users.map(user => user.firstName + " " + user.lastName).join(', ')
        if (admins == "") {
            return this.appConstants.NOT_AVAILABLE_MESSAGE
        } else {
            return admins
        }
    }
}