"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var templemanagement_component_1 = require("./templemanagement.component");
var temples_component_1 = require("./temples/temples.component");
var templedetail_component_1 = require("./templedetail/templedetail.component");
var constants_1 = require("../utility/constants");
var templeuser_component_1 = require("./templeuser/templeuser.component");
var routes = [
    {
        path: '',
        component: templemanagement_component_1.TemplemanagementComponent,
        children: [
            {
                path: '',
                component: temples_component_1.TemplesComponent,
                data: { title: "Temples" }
            },
            {
                path: constants_1.RouteConstants.APP_TEMPLE_DETAIL,
                component: templedetail_component_1.TempledetailComponent,
                data: { title: "Temple Detail" }
            },
            {
                path: constants_1.RouteConstants.APP_TEMPLE_USER,
                component: templeuser_component_1.TempleuserComponent,
                data: { title: "Users" }
            },
        ]
    },
];
var TempleManagementRoutingModule = (function () {
    function TempleManagementRoutingModule() {
    }
    TempleManagementRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(routes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], TempleManagementRoutingModule);
    return TempleManagementRoutingModule;
}());
exports.TempleManagementRoutingModule = TempleManagementRoutingModule;
//# sourceMappingURL=templemanagement-routing.module.js.map