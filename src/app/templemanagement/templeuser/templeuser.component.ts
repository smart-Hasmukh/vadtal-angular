import { Component, OnInit } from '@angular/core';
import {TitleConstants, APPConstants, HttpStatusConstants} from "../../utility/constants";
import {Title} from "@angular/platform-browser";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Temple} from "../temple.model";
import {User} from "../../user/user.model";
import {TempleService} from "../temples/temple.service";
import {RequestOptions, URLSearchParams} from "@angular/http";
import {UserService} from "../../user/user.service";
import {Pager} from "../pager.model";

@Component({
  selector: 'app-temple-user',
  templateUrl: './templeuser.component.html',
  styleUrls: ['./templeuser.component.css']
})
export class TempleuserComponent implements OnInit {

  templeId : number;
  users : User[];
  canShowUserList = false
  pager: Pager;
  totalCount: number;
  appConstant = new APPConstants();
  titleConstants = new TitleConstants();


  constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute,
              private router: Router, private templeService: TempleService,private userService : UserService) {
      this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
    })
  }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.TEMPLE_USER);
    this.activatedRoute.queryParams.subscribe(data =>  {
      this.templeId = data['id']
      console.log("this.templeId======>",this.templeId)
      this.getUsers(1)
    });
  }

  getUsers(pageIndex: number) {
    let searchParams = `?pageNumber=${pageIndex}&templeId=${this.templeId}`
    /*this.userService.getUserList(searchParams).subscribe(response => {
      if (response.status == HttpStatusConstants.SUCCESS) {
        this.pager = response.pager;
        this.users = response.payload.data;
        this.totalCount = this.pager.totalRecords;
        console.log("Users====>",this.users)
        if (this.users.length > 0) {
          this.canShowUserList = true
        } else {
          this.canShowUserList = false
        }
        console.log("canShowUserList====>",this.canShowUserList)
      }
    })*/
  }
}

