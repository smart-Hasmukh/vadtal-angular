"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var TempleuserComponent = (function () {
    function TempleuserComponent(title, breadcrumbService, activatedRoute, router, templeService, userService) {
        var _this = this;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.templeService = templeService;
        this.userService = userService;
        this.canShowUserList = false;
        this.appConstant = new constants_1.APPConstants();
        this.titleConstants = new constants_1.TitleConstants();
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
        });
    }
    TempleuserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.title.setTitle(this.titleConstants.TEMPLE_USER);
        this.activatedRoute.queryParams.subscribe(function (data) {
            _this.templeId = data['id'];
            console.log("this.templeId======>", _this.templeId);
            _this.getUsers(1);
        });
    };
    TempleuserComponent.prototype.getUsers = function (pageIndex) {
        var searchParams = "?pageNumber=" + pageIndex + "&templeId=" + this.templeId;
        /*this.userService.getUserList(searchParams).subscribe(response => {
          if (response.status == HttpStatusConstants.SUCCESS) {
            this.pager = response.pager;
            this.users = response.payload.data;
            this.totalCount = this.pager.totalRecords;
            console.log("Users====>",this.users)
            if (this.users.length > 0) {
              this.canShowUserList = true
            } else {
              this.canShowUserList = false
            }
            console.log("canShowUserList====>",this.canShowUserList)
          }
        })*/
    };
    TempleuserComponent = __decorate([
        core_1.Component({
            selector: 'app-temple-user',
            templateUrl: './templeuser.component.html',
            styleUrls: ['./templeuser.component.css']
        })
    ], TempleuserComponent);
    return TempleuserComponent;
}());
exports.TempleuserComponent = TempleuserComponent;
//# sourceMappingURL=templeuser.component.js.map