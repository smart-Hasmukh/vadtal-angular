/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TempleuserComponent } from './templeuser.component';

describe('TempleuserComponent', () => {
  let component: TempleuserComponent;
  let fixture: ComponentFixture<TempleuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempleuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempleuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
