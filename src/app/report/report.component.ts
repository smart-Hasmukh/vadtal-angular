import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {TitleConstants} from "../utility/constants";

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  titleConstants = new TitleConstants();
  constructor(private title: Title) { }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.REPORT);
  }

}
