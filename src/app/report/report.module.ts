import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ReportComponent} from "./report.component";
import {ReportRoutingModule} from "./report-routing.module";
import {MaterialModule} from "@angular/material";

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        ReportRoutingModule
    ],
    declarations: [ReportComponent]
})

export class ReportModule {
}
