"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var constants_1 = require("../utility/constants");
var adminroles_service_1 = require("./adminroles.service");
var AdminrolesComponent = (function () {
    function AdminrolesComponent(title, formBuilder, rolesService, userService) {
        this.title = title;
        this.formBuilder = formBuilder;
        this.rolesService = rolesService;
        this.userService = userService;
        this.titleConstants = new constants_1.TitleConstants();
        this.showStyle = false;
        this.roles = [];
        this.users = [];
        this.appConstant = new constants_1.APPConstants();
        this.recordsPerPage = 0;
        this.pageNumber = 1;
    }
    AdminrolesComponent.prototype.ngOnInit = function () {
        this.recordsPerPage = this.appConstant.PAGINATION_ARRAY[0];
        this.title.setTitle(this.titleConstants.ADMIN_ROLES);
        this.initialize();
    };
    AdminrolesComponent.prototype.initialize = function () {
        this.createAdminRolesForm();
        this.getRoles();
    };
    AdminrolesComponent.prototype.getRemoveClass = function () {
        if (this.showStyle) {
            return "";
        }
        else {
            return "remove-close-icon";
        }
    };
    // create role model methods
    AdminrolesComponent.prototype.showCreateRoleDialouge = function (dialog) {
        dialog.show();
    };
    //suspend roles model methods
    AdminrolesComponent.prototype.showSuspendRole = function (dialog) {
        dialog.show();
    };
    //assign roles model methods
    AdminrolesComponent.prototype.showAssignRole = function (dialog) {
        dialog.show();
    };
    AdminrolesComponent.prototype.getRoles = function () {
        var _this = this;
        this.rolesService.getRoles().subscribe(function (response) {
            _this.roles = response.payload["data"];
            if (!_this.selectedRole && _this.roles.length > 0) {
                _this.selectedRole = _this.roles[0];
                _this.getUsers(_this.pageNumber, _this.selectedRole.id, _this.recordsPerPage);
            }
        });
    };
    AdminrolesComponent.prototype.addRoles = function (data, dialog) {
        this.rolesService.addRole(data).subscribe(function (response) {
            dialog.close();
        });
    };
    AdminrolesComponent.prototype.getUsersRoleWise = function (role) {
        this.selectedRole = role;
        this.getUsers(this.pageNumber, this.selectedRole.id, this.recordsPerPage);
    };
    AdminrolesComponent.prototype.getNewUsers = function (pageNumber) {
        this.pageNumber = pageNumber;
        this.getUsers(this.pageNumber, this.selectedRole.id, this.recordsPerPage);
    };
    AdminrolesComponent.prototype.changeNumberOfPerPageUsers = function (newValue) {
        this.recordsPerPage = parseInt(newValue);
        this.getUsers(this.pageNumber, this.selectedRole.id, this.recordsPerPage);
    };
    AdminrolesComponent.prototype.getUsers = function (pageNumber, roleId, recordsPerPage) {
        var _this = this;
        this.userService.getUserList(pageNumber, +recordsPerPage, { "roleId": roleId }).subscribe(function (response) {
            console.log("=====>", response);
            _this.pager = response.pager;
            _this.totalCount = _this.pager.totalRecords;
            _this.users = response.payload.data;
            _this.appConstant.PAGINATION_SIZE = _this.pager.recordsPerPage;
        });
    };
    AdminrolesComponent.prototype.assignRole = function (data, dialog) {
    };
    AdminrolesComponent.prototype.createAdminRolesForm = function () {
        this.rolesForm = this.formBuilder.group({
            name: new forms_1.FormControl('', []),
            code: new forms_1.FormControl('', []),
            description: new forms_1.FormControl('', [])
        });
    };
    AdminrolesComponent = __decorate([
        core_1.Component({
            selector: 'app-adminroles',
            templateUrl: './adminroles.component.html',
            providers: [adminroles_service_1.AdminrolesService],
            styleUrls: ['./adminroles.component.css']
        })
    ], AdminrolesComponent);
    return AdminrolesComponent;
}());
exports.AdminrolesComponent = AdminrolesComponent;
//# sourceMappingURL=adminroles.component.js.map