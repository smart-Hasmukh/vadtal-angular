export class Roles {
    private _id: number;
    private _name: string;
    private _code: string;
    private _level: number;
    private _description: string;
    private createdBy: number;
    private updatedBy: number;
    private _isEnable: boolean;
    private created_at: string;
    private updated_at: string;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get code(): string {
        return this._code;
    }

    set code(value: string) {
        this._code = value;
    }

    get level(): number {
        return this._level;
    }

    set level(value: number) {
        this._level = value;
    }

    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }

    get isEnable(): boolean {
        return this._isEnable;
    }

    set isEnable(value: boolean) {
        this._isEnable = value;
    }
}