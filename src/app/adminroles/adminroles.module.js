"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var adminroles_component_1 = require("./adminroles.component");
var adminroles_routing_module_1 = require("./adminroles-routing.module");
var md2_1 = require("md2");
var material_1 = require("@angular/material");
var forms_1 = require("@angular/forms");
var user_service_1 = require("../user/user.service");
var ng2_pagination_1 = require("ng2-pagination");
var AdminrolesModule = (function () {
    function AdminrolesModule() {
    }
    AdminrolesModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                adminroles_routing_module_1.AdminrolesRoutingModule,
                md2_1.Md2Module.forRoot(),
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ng2_pagination_1.Ng2PaginationModule
            ],
            providers: [user_service_1.UserService],
            declarations: [adminroles_component_1.AdminrolesComponent]
        })
    ], AdminrolesModule);
    return AdminrolesModule;
}());
exports.AdminrolesModule = AdminrolesModule;
//# sourceMappingURL=adminroles.module.js.map