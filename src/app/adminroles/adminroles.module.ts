import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AdminrolesComponent} from "./adminroles.component";
import {AdminrolesRoutingModule} from "./adminroles-routing.module";
import {Md2Module} from "md2";
import {MaterialModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserService} from "../user/user.service";
import {Ng2PaginationModule} from "ng2-pagination";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AdminrolesRoutingModule,
    Md2Module.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    Ng2PaginationModule
  ],
  providers: [UserService],
  declarations: [AdminrolesComponent]
})

export class AdminrolesModule {
}