/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AdminrolesService } from './adminroles.service';

describe('AdminrolesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminrolesService]
    });
  });

  it('should ...', inject([AdminrolesService], (service: AdminrolesService) => {
    expect(service).toBeTruthy();
  }));
});
