"use strict";
var Roles = (function () {
    function Roles() {
    }
    Object.defineProperty(Roles.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Roles.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Roles.prototype, "code", {
        get: function () {
            return this._code;
        },
        set: function (value) {
            this._code = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Roles.prototype, "level", {
        get: function () {
            return this._level;
        },
        set: function (value) {
            this._level = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Roles.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (value) {
            this._description = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Roles.prototype, "isEnable", {
        get: function () {
            return this._isEnable;
        },
        set: function (value) {
            this._isEnable = value;
        },
        enumerable: true,
        configurable: true
    });
    return Roles;
}());
exports.Roles = Roles;
//# sourceMappingURL=roles.model.js.map