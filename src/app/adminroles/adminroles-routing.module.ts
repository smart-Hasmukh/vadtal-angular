import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AdminrolesComponent} from "./adminroles.component";


export const routes: Routes = [
    {
        path: '',
        component: AdminrolesComponent,
        children : [

        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AdminrolesRoutingModule {
}

