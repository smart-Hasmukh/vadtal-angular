import {Injectable} from "@angular/core";
import {HttpService} from "../utility/http-service";
import {ToastsManager} from "ng2-toastr";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../utility/constants";
import {Response} from "@angular/http";

@Injectable()
export class AdminrolesService {
  constructor(private  httpService: HttpService, private toastManager: ToastsManager) {
  }

  getRoles(): Observable<any> {
    return this.httpService.get(ApiEventConstants.ROLES + "?records=all").map(res => this.extractData(res, false));
  }

  addRole(data: any): Observable<any> {
    return this.httpService.post(ApiEventConstants.ROLES, data).map(res => this.extractData(res, true));
  }


  private extractData(res: Response, showToast: boolean) {
    if (showToast) {
      this.toastManager.success(res.json().message)
    }
    let data = res.json();
    return data || {};
  }
}
