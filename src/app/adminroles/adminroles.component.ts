import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, FormControl} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import {TitleConstants, APPConstants} from "../utility/constants";
import {AdminrolesService} from "./adminroles.service";
import {UserService} from "../user/user.service";
import {Roles} from "./roles.model";
import {Pager} from "../templemanagement/pager.model";
import {User} from "../user/user.model";

@Component({
    selector: 'app-adminroles',
    templateUrl: './adminroles.component.html',
    providers: [AdminrolesService],
    styleUrls: ['./adminroles.component.css']
})

export class AdminrolesComponent implements OnInit {
    titleConstants = new TitleConstants();
    rolesForm: FormGroup;
    showStyle = false;
    selectedRole : Roles;
    pager: Pager;
    roles: Roles [] = [];
    users: User [] = [];
    totalCount: number;
    appConstant = new APPConstants();

    recordsPerPage : number = 0;
    pageNumber : number = 1;

    constructor(private title: Title, private formBuilder: FormBuilder, private rolesService: AdminrolesService, private  userService: UserService) {

    }

    ngOnInit() {
        this.recordsPerPage = this.appConstant.PAGINATION_ARRAY[0];
        this.title.setTitle(this.titleConstants.ADMIN_ROLES);
        this.initialize();
    }

    initialize() {
        this.createAdminRolesForm()
        this.getRoles()
    }

    getRemoveClass() {
        if (this.showStyle) {
            return "";
        } else {
            return "remove-close-icon";
        }
    }

    // create role model methods
    showCreateRoleDialouge(dialog: any) {
        dialog.show();
    }

    //suspend roles model methods
    showSuspendRole(dialog: any) {
        dialog.show();
    }

    //assign roles model methods
    showAssignRole(dialog: any) {
        dialog.show();
    }

    getRoles() {
        this.rolesService.getRoles().subscribe(response => {
            this.roles = response.payload["data"]
            if (!this.selectedRole && this.roles.length > 0){
                this.selectedRole = this.roles[0];
                this.getUsers(this.pageNumber,this.selectedRole.id,this.recordsPerPage)
            }
        });
    }

    addRoles(data: any, dialog: any) {
        this.rolesService.addRole(data).subscribe(response => {
            dialog.close();
        });
    }

    getUsersRoleWise(role : Roles){
        this.selectedRole = role;
        this.getUsers(this.pageNumber, this.selectedRole.id, this.recordsPerPage);
    }

    getNewUsers(pageNumber : number){
        this.pageNumber = pageNumber
        this.getUsers(this.pageNumber,this.selectedRole.id, this.recordsPerPage)
    }

    changeNumberOfPerPageUsers(newValue : string){
        this.recordsPerPage = parseInt(newValue);
        this.getUsers(this.pageNumber,this.selectedRole.id, this.recordsPerPage)
    }

    getUsers(pageNumber : number, roleId : number, recordsPerPage:number) {
        this.userService.getUserList(pageNumber, +recordsPerPage, {"roleId" : roleId}).subscribe(response=>{
            console.log("=====>",response)
            this.pager = response.pager;
            this.totalCount = this.pager.totalRecords;
            this.users = response.payload.data;
            this.appConstant.PAGINATION_SIZE = this.pager.recordsPerPage;
        })
    }

    assignRole(data: any, dialog: any) {

    }

    createAdminRolesForm() {
        this.rolesForm = this.formBuilder.group({
            name: new FormControl('', []),
            code: new FormControl('', []),
            description: new FormControl('', [])
        });
    }
}
