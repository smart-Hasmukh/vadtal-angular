export enum ValidationType{
    REQUIRED_FIELD,
    MIN_VALUE,
    MAX_VALUE,
}

export class ChangePasswordConstants {
    public CURRENT_PASS : string = "Current password is required."
    public NEW_PASS : string = "New password is required."
    public CONFIRM_PASS : string = "Confirm password is required."
    public MATCH_PASS : string = "Confirm password does not match."
}

export class ProfileMessageConstants {
    public PROFILE_PIC_EXCEPTION : string  = "Profile picture must be within 5 MB."
    public FIRST_NAME : string = "Firstname must be within 30 characters and non empty."
    public MIDDLE_NAME : string = "Middlename must be within 30 characters or empty."
    public LAST_NAME : string = "Lastname must be within 30 characters and non empty."
    public MNO : string = "Mobile No is required."
    public DOB : string = "Date Of Birth is required."
    public LOCATION : string  = "Select Location."
    public PROFESSION : string = "Profession must be within 30 characters or empty."
    public BLOOD_GROUP : string  = "Select Blood Group."
    public EMAIL : string  = "Email is required."
}