"use strict";
(function (ValidationType) {
    ValidationType[ValidationType["REQUIRED_FIELD"] = 0] = "REQUIRED_FIELD";
    ValidationType[ValidationType["MIN_VALUE"] = 1] = "MIN_VALUE";
    ValidationType[ValidationType["MAX_VALUE"] = 2] = "MAX_VALUE";
})(exports.ValidationType || (exports.ValidationType = {}));
var ValidationType = exports.ValidationType;
var ChangePasswordConstants = (function () {
    function ChangePasswordConstants() {
        this.CURRENT_PASS = "Current password is required.";
        this.NEW_PASS = "New password is required.";
        this.CONFIRM_PASS = "Confirm password is required.";
        this.MATCH_PASS = "Confirm password does not match.";
    }
    return ChangePasswordConstants;
}());
exports.ChangePasswordConstants = ChangePasswordConstants;
var ProfileMessageConstants = (function () {
    function ProfileMessageConstants() {
        this.PROFILE_PIC_EXCEPTION = "Profile picture must be within 5 MB.";
        this.FIRST_NAME = "Firstname must be within 30 characters and non empty.";
        this.MIDDLE_NAME = "Middlename must be within 30 characters or empty.";
        this.LAST_NAME = "Lastname must be within 30 characters and non empty.";
        this.MNO = "Mobile No is required.";
        this.DOB = "Date Of Birth is required.";
        this.LOCATION = "Select Location.";
        this.PROFESSION = "Profession must be within 30 characters or empty.";
        this.BLOOD_GROUP = "Select Blood Group.";
        this.EMAIL = "Email is required.";
    }
    return ProfileMessageConstants;
}());
exports.ProfileMessageConstants = ProfileMessageConstants;
//# sourceMappingURL=validationmessage-constants.js.map