export class RouteConstants {
    public static APP_LOGIN: string = "login"
    public static APP_HOME: string = "home"
    public static APP_ACCOMODATION: string = "accomodation"

    public static APP_USER: string = "user"
    public static APP_USER_LIST: string = "user-list"
    public static APP_USER_DETAIL: string = "user-detail"
    public static APP_USER_ADD: string = "user-add"
    public static APP_USER_PRIVILEGE: string = "user-priviliges"

    public static APP_TEMPLE_MANAGEMENT: string = "temple"
    public static APP_TEMPLE_LIST: string = "temple-list"
    public static APP_TEMPLE_DETAIL: string = "temple-detail"
    public static APP_TEMPLE_USER: string = "temple-user"
    public static APP_REPORT: string = "report"

    public static APP_DONATION: string = "donation"
    public static APP_DONATION_AREA_MANAGEMENT: string = "area-management"
    public static APP_DONATION_YEAR_MANAGEMENT: string = "year-management"
    public static APP_DONATION_BOOK_TYPE: string = "book-type"
    public static APP_DONATION_DONOR_PROFILE_UPDATE: string = "donor-profile-update"
    public static APP_DONATION_DELETE_RECEIPT: string = "delete-receipt"

    public static APP_ADMIN_ROLES: string = "admin-roles"
}

export class TitleConstants {
    public LOGIN: string = "Login"
    public HOME: string = "Home"
    public ACCOMMODATION: string = "Accommodation"
    public USER: string = "Users"
    public USER_LIST: string = "User List"
    public USER_DETAIL: string = "User Detail"
    public USER_ADD: string = "User Add"
    public USER_PRIVILEGE: string = "User Privilege"
    public TEMPLE_MANAGEMENT: string = "Temple"
    public TEMPLE_LIST: string = "Temple List"
    public TEMPLE_DETAIL: string = "Temple Detail"
    public TEMPLE_USER: string = "Temple User"
    public REPORT: string = "Report"
    public DONATION: string = "Donation"
    public DONATION_AREA_MANAGEMENT: string = "Area Management"
    public AREA_LIST: string = "Area List"
    public YEAR_LIST: string = "Year List"
    public DONATION_YEAR_MANAGEMENT: string = "Year Management"
    public DONATION_BOOK_TYPE: string = "Book Type"
    public DONATION_BOOK_TYPE_LIST: string = "Book Type List"
    public DONATION_DONOR_PROFILE_UPDATE: string = "Donor Profile Update"
    public DONATION_DELETE_RECEIPT: string = "Delete Receipt"
    public ADMIN_ROLES: string = "Admin Roles"
    public DRAWER: string = "Jay Swaminarayan"
    public FORGOT_PASSWORD: string = "Forgot Password"
    public SEND_OTP: string = "Send OTP"
    public SET_NEW_PASSWORD: string = "Set New Password"
    public RESET_PASSWORD: string = "Reset Password"

}

export class KeyConstants {
    public static JWT_RESPONSE_TOKEN: string = "at"
    public static CURRENT_USER: string = "cu"
    public static PRIVILEGES: string = "up"
    public static TIMESTAMP: string = "ts"
}

export class ApiEventConstants {
    public static TEMPLE = "temple";
    public static COUNTRY = "country";
    public static STATE = "state";
    public static CITY = "city";
    public static USER = "user";
    public static AREA = "area";
    public static YEAR = "year";
    public static DONATIONBOOKTYPE = "donationBookType";


    public static ROLES = "role";
}

export class HttpStatusConstants {
    public static SUCCESS: number = 200
    public static UNAUTHORIZED: number = 401
}

export class APPConstants {
    public static BASE_URL: string = "http://gifkar.cloudapp.net:8000/v1.0/"
    public BASE_TEMPLE_IMAGE_URL : string = "http://gifkar.cloudapp.net:8000/images/temple/";
    public BASE_USER_IMAGE_URL : string = "http://gifkar.cloudapp.net:8000/images/users/";
    public BASE_TEMPLE_THUMBNAIL_IMAGE_URL : string = "http://gifkar.cloudapp.net:8000/images/temple/thumbnails/";



    public NOT_AVAILABLE_MESSAGE : string = "Not provided";
    public static SESSION_TIMEOUT: number = 7200;
    public PAGINATION_SIZE = 10;
    public static IMAGE_FILE_SIZE_LIMIT = 5242880;
    public PAGINATION_ARRAY : number[] = [5,10,15];
    public RECORDS_PER_PAGE : number = 5;
}

export class DateFormatConstant {
    public static DATE_FORMAT_GET2 = 'DD MM, YYYY at HH:mm';
    public static DATE_FORMAT_GET = 'YYYY-MM-DD HH:mm';
    public static DATE_FORMAT_EVENT_VIEW = 'YYYY-MM-DD HH:mm';
    public static DATE_FORMAT_EVENT_RECEIVED = 'DD MMM, YYYY at HH:mm';
    public static DATE_FORMAT_PROJECT_RECEIVED = 'DD MMM, YYYY';
    public static DATE_FORMAT_PROJECT_VIEW = 'YYYY-MM-DD';
}


