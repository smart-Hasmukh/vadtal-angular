"use strict";
var base64_typescript_class_1 = require("./base64-typescript-class");
var GeneralFunction = (function () {
    function GeneralFunction() {
    }
    GeneralFunction.ENCRYPT_OBJECT = function (value) {
        var base64 = new base64_typescript_class_1.Base64();
        return base64.encode(value);
    };
    GeneralFunction.DECRYPT_OBJECT = function (value) {
        if (value && value != "null" && value != null && value != "undefined") {
            var base64 = new base64_typescript_class_1.Base64();
            return base64.decode(value);
        }
        return '';
    };
    GeneralFunction.ConvertIntToBoolean = function (value) {
        if (value == null)
            return false;
        if (value == 1) {
            return true;
        }
        else {
            return false;
        }
    };
    return GeneralFunction;
}());
exports.GeneralFunction = GeneralFunction;
//# sourceMappingURL=general-functions.js.map