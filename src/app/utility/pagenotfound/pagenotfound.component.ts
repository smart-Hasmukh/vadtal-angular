import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {RouteConstants} from "../constants";

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.css']
})

export class PagenotfoundComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit() {
  }

  returnHome(){
    this.router.navigate(['/' + RouteConstants.APP_LOGIN])
  }

}
