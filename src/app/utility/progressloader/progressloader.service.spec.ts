/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProgressloaderService } from './progressloader.service';

describe('ProgressloaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgressloaderService]
    });
  });

  it('should ...', inject([ProgressloaderService], (service: ProgressloaderService) => {
    expect(service).toBeTruthy();
  }));
});
