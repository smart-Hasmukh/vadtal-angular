"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var BehaviorSubject_1 = require('rxjs/BehaviorSubject');
var ProgressloaderService = (function () {
    function ProgressloaderService() {
        this.isLoading = new BehaviorSubject_1.BehaviorSubject(false);
    }
    ProgressloaderService.prototype.showLoader = function () {
        this.setIsLoading(true);
    };
    ProgressloaderService.prototype.hideLoader = function () {
        this.setIsLoading(false);
    };
    ProgressloaderService.prototype.getIsLoading = function () {
        return this.isLoading.asObservable();
    };
    ProgressloaderService.prototype.setIsLoading = function (val) {
        this.isLoading.next(val);
    };
    ProgressloaderService = __decorate([
        core_1.Injectable()
    ], ProgressloaderService);
    return ProgressloaderService;
}());
exports.ProgressloaderService = ProgressloaderService;
//# sourceMappingURL=progressloader.service.js.map