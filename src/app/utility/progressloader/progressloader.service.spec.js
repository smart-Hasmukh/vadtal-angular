/* tslint:disable:no-unused-variable */
"use strict";
var testing_1 = require('@angular/core/testing');
var progressloader_service_1 = require('./progressloader.service');
describe('ProgressloaderService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [progressloader_service_1.ProgressloaderService]
        });
    });
    it('should ...', testing_1.inject([progressloader_service_1.ProgressloaderService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=progressloader.service.spec.js.map