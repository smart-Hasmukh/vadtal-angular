import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProgressloaderService {
  private isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor() { }
  showLoader(){
    this.setIsLoading(true);
  }

  hideLoader(){
    this.setIsLoading(false);
  }

  getIsLoading(): Observable<boolean> {
    return this.isLoading.asObservable();
  }

  setIsLoading(val : boolean): void {
    this.isLoading.next(val);
  }
}
