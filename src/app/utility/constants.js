"use strict";
var RouteConstants = (function () {
    function RouteConstants() {
    }
    RouteConstants.APP_LOGIN = "login";
    RouteConstants.APP_HOME = "home";
    RouteConstants.APP_ACCOMODATION = "accomodation";
    RouteConstants.APP_USER = "user";
    RouteConstants.APP_USER_LIST = "user-list";
    RouteConstants.APP_USER_DETAIL = "user-detail";
    RouteConstants.APP_USER_ADD = "user-add";
    RouteConstants.APP_USER_PRIVILEGE = "user-priviliges";
    RouteConstants.APP_TEMPLE_MANAGEMENT = "temple";
    RouteConstants.APP_TEMPLE_LIST = "temple-list";
    RouteConstants.APP_TEMPLE_DETAIL = "temple-detail";
    RouteConstants.APP_TEMPLE_USER = "temple-user";
    RouteConstants.APP_REPORT = "report";
    RouteConstants.APP_DONATION = "donation";
    RouteConstants.APP_DONATION_AREA_MANAGEMENT = "area-management";
    RouteConstants.APP_DONATION_YEAR_MANAGEMENT = "year-management";
    RouteConstants.APP_DONATION_BOOK_TYPE = "book-type";
    RouteConstants.APP_DONATION_DONOR_PROFILE_UPDATE = "donor-profile-update";
    RouteConstants.APP_DONATION_DELETE_RECEIPT = "delete-receipt";
    RouteConstants.APP_ADMIN_ROLES = "admin-roles";
    return RouteConstants;
}());
exports.RouteConstants = RouteConstants;
var TitleConstants = (function () {
    function TitleConstants() {
        this.LOGIN = "Login";
        this.HOME = "Home";
        this.ACCOMMODATION = "Accommodation";
        this.USER = "Users";
        this.USER_LIST = "User List";
        this.USER_DETAIL = "User Detail";
        this.USER_ADD = "User Add";
        this.USER_PRIVILEGE = "User Privilege";
        this.TEMPLE_MANAGEMENT = "Temple";
        this.TEMPLE_LIST = "Temple List";
        this.TEMPLE_DETAIL = "Temple Detail";
        this.TEMPLE_USER = "Temple User";
        this.REPORT = "Report";
        this.DONATION = "Donation";
        this.DONATION_AREA_MANAGEMENT = "Area Management";
        this.AREA_LIST = "Area List";
        this.YEAR_LIST = "Year List";
        this.DONATION_YEAR_MANAGEMENT = "Year Management";
        this.DONATION_BOOK_TYPE = "Book Type";
        this.DONATION_BOOK_TYPE_LIST = "Book Type List";
        this.DONATION_DONOR_PROFILE_UPDATE = "Donor Profile Update";
        this.DONATION_DELETE_RECEIPT = "Delete Receipt";
        this.ADMIN_ROLES = "Admin Roles";
        this.DRAWER = "Jay Swaminarayan";
        this.FORGOT_PASSWORD = "Forgot Password";
        this.SEND_OTP = "Send OTP";
        this.SET_NEW_PASSWORD = "Set New Password";
        this.RESET_PASSWORD = "Reset Password";
    }
    return TitleConstants;
}());
exports.TitleConstants = TitleConstants;
var KeyConstants = (function () {
    function KeyConstants() {
    }
    KeyConstants.JWT_RESPONSE_TOKEN = "at";
    KeyConstants.CURRENT_USER = "cu";
    KeyConstants.PRIVILEGES = "up";
    KeyConstants.TIMESTAMP = "ts";
    return KeyConstants;
}());
exports.KeyConstants = KeyConstants;
var ApiEventConstants = (function () {
    function ApiEventConstants() {
    }
    ApiEventConstants.TEMPLE = "temple";
    ApiEventConstants.COUNTRY = "country";
    ApiEventConstants.STATE = "state";
    ApiEventConstants.CITY = "city";
    ApiEventConstants.USER = "user";
    ApiEventConstants.AREA = "area";
    ApiEventConstants.YEAR = "year";
    ApiEventConstants.DONATIONBOOKTYPE = "donationBookType";
    ApiEventConstants.ROLES = "role";
    return ApiEventConstants;
}());
exports.ApiEventConstants = ApiEventConstants;
var HttpStatusConstants = (function () {
    function HttpStatusConstants() {
    }
    HttpStatusConstants.SUCCESS = 200;
    HttpStatusConstants.UNAUTHORIZED = 401;
    return HttpStatusConstants;
}());
exports.HttpStatusConstants = HttpStatusConstants;
var APPConstants = (function () {
    function APPConstants() {
        this.BASE_TEMPLE_IMAGE_URL = "http://gifkar.cloudapp.net:8000/images/temple/";
        this.BASE_USER_IMAGE_URL = "http://gifkar.cloudapp.net:8000/images/users/";
        this.BASE_TEMPLE_THUMBNAIL_IMAGE_URL = "http://gifkar.cloudapp.net:8000/images/temple/thumbnails/";
        this.NOT_AVAILABLE_MESSAGE = "Not provided";
        this.PAGINATION_SIZE = 10;
        this.PAGINATION_ARRAY = [5, 10, 15];
        this.RECORDS_PER_PAGE = 5;
    }
    APPConstants.BASE_URL = "http://gifkar.cloudapp.net:8000/v1.0/";
    APPConstants.SESSION_TIMEOUT = 7200;
    APPConstants.IMAGE_FILE_SIZE_LIMIT = 5242880;
    return APPConstants;
}());
exports.APPConstants = APPConstants;
var DateFormatConstant = (function () {
    function DateFormatConstant() {
    }
    DateFormatConstant.DATE_FORMAT_GET2 = 'DD MM, YYYY at HH:mm';
    DateFormatConstant.DATE_FORMAT_GET = 'YYYY-MM-DD HH:mm';
    DateFormatConstant.DATE_FORMAT_EVENT_VIEW = 'YYYY-MM-DD HH:mm';
    DateFormatConstant.DATE_FORMAT_EVENT_RECEIVED = 'DD MMM, YYYY at HH:mm';
    DateFormatConstant.DATE_FORMAT_PROJECT_RECEIVED = 'DD MMM, YYYY';
    DateFormatConstant.DATE_FORMAT_PROJECT_VIEW = 'YYYY-MM-DD';
    return DateFormatConstant;
}());
exports.DateFormatConstant = DateFormatConstant;
//# sourceMappingURL=constants.js.map