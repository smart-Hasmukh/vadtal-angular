"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/Rx');
var constants_1 = require("./constants");
var HttpService = (function (_super) {
    __extends(HttpService, _super);
    function HttpService(backend, defaultOptions, loaderService, toastManager, userService, router) {
        _super.call(this, backend, defaultOptions);
        this.loaderService = loaderService;
        this.toastManager = toastManager;
        this.userService = userService;
        this.router = router;
    }
    /**
     * Performs any type of http request.
     * @param url
     * @param options
     * @returns {Observable<Response>}
     */
    HttpService.prototype.request = function (url, options) {
        return _super.prototype.request.call(this, url, options);
    };
    /**
     * Performs a request with `get` http method.
     * @param url
     * @param options
     * @returns {Observable<>}
     */
    HttpService.prototype.get = function (url, options) {
        var _this = this;
        this.requestInterceptor();
        console.log("===>URL", this.getFullUrl(url));
        return _super.prototype.get.call(this, this.getFullUrl(url), this.requestOptions())
            .catch(this.onCatch)
            .do(function (res) {
            console.log("===>Response", res);
            _this.onSubscribeSuccess(res);
        }, function (error) {
            console.log("===>Error", error);
            _this.onSubscribeError(error);
        })
            .finally(function () {
            _this.onFinally();
        });
    };
    HttpService.prototype.getLocal = function (url, options) {
        return _super.prototype.get.call(this, url, options);
    };
    /**
     * Performs a request with `post` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */
    HttpService.prototype.post = function (url, body, options) {
        var _this = this;
        this.requestInterceptor();
        console.log("URL============>", this.getFullUrl(url));
        console.log("Body============>", body);
        return _super.prototype.post.call(this, this.getFullUrl(url), body, this.requestOptions(options))
            .catch(this.onCatch)
            .do(function (res) {
            console.log("Response============>", res);
            _this.onSubscribeSuccess(res);
        }, function (error) {
            console.log("Error============>", error);
            _this.onSubscribeError(error);
        })
            .finally(function () {
            _this.onFinally();
        });
    };
    HttpService.prototype.postWithFile = function (url, postData, filesObj, options) {
        var _this = this;
        this.requestInterceptor();
        var headers = new http_1.Headers();
        var formData = new FormData();
        for (var _i = 0, filesObj_1 = filesObj; _i < filesObj_1.length; _i++) {
            var obj = filesObj_1[_i];
            var imgFilesObj = obj['files'];
            for (var i = 0; i < imgFilesObj.length; i++) {
                formData.append(obj["reqKey"], imgFilesObj[i], imgFilesObj[i].name);
            }
        }
        if (postData !== "" && postData !== undefined && postData !== null) {
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        console.log("Body============>", formData);
        console.log("URL============>", this.getFullUrl(url));
        return _super.prototype.post.call(this, this.getFullUrl(url), formData, this.requestOptions(options))
            .catch(this.onCatch)
            .do(function (res) {
            console.log("Response============>", res);
            _this.onSubscribeSuccess(res);
        }, function (error) {
            console.log("Error============>", error);
            _this.onSubscribeError(error);
        })
            .finally(function () {
            _this.onFinally();
        });
    };
    /**
     * Performs a request with `put` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */
    HttpService.prototype.put = function (url, body, options) {
        var _this = this;
        this.requestInterceptor();
        console.log("URL============>", this.getFullUrl(url));
        return _super.prototype.put.call(this, this.getFullUrl(url), body, this.requestOptions(options))
            .catch(this.onCatch)
            .do(function (res) {
            console.log("Response============>", res);
            _this.onSubscribeSuccess(res);
        }, function (error) {
            console.log("Error============>", error);
            _this.onSubscribeError(error);
        })
            .finally(function () {
            _this.onFinally();
        });
    };
    /**
     * Performs a request with `put` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */
    HttpService.prototype.putWithFile = function (url, postData, filesObj, options) {
        var _this = this;
        var headers = new http_1.Headers();
        var formData = new FormData();
        for (var _i = 0, filesObj_2 = filesObj; _i < filesObj_2.length; _i++) {
            var obj = filesObj_2[_i];
            var imgFilesObj = obj['files'];
            for (var i = 0; i < imgFilesObj.length; i++) {
                formData.append(obj["reqKey"], imgFilesObj[i], imgFilesObj[i].name);
            }
        }
        if (postData !== "" && postData !== undefined && postData !== null) {
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        console.log("Body============>", formData);
        console.log("URL============>", this.getFullUrl(url));
        return _super.prototype.put.call(this, this.getFullUrl(url), formData, this.requestOptions(options))
            .catch(this.onCatch)
            .do(function (res) {
            console.log("Response============>", res);
            _this.onSubscribeSuccess(res);
        }, function (error) {
            console.log("Error============>", error);
            _this.onSubscribeError(error);
        })
            .finally(function () {
            _this.onFinally();
        });
    };
    /**
     * Performs a request with `delete` http method.
     * @param url
     * @param options
     * @returns {Observable<>}
     */
    HttpService.prototype.delete = function (url, options) {
        var _this = this;
        this.requestInterceptor();
        console.log("URL============>", this.getFullUrl(url));
        return _super.prototype.delete.call(this, this.getFullUrl(url), this.requestOptions(options))
            .catch(this.onCatch)
            .do(function (res) {
            console.log("Response============>", res);
            _this.onSubscribeSuccess(res);
        }, function (error) {
            console.log("Error============>", error);
            _this.onSubscribeError(error);
        })
            .finally(function () {
            _this.onFinally();
        });
    };
    /**
     * Request options.
     * @param options
     * @returns {RequestOptionsArgs}
     */
    HttpService.prototype.requestOptions = function (options) {
        if (options == null) {
            options = new http_1.RequestOptions();
        }
        if (options.headers == null) {
            options.headers = this.createHeaders();
        }
        return options;
    };
    /**
     * Build API url.
     * @param url
     * @returns {string}
     */
    HttpService.prototype.getFullUrl = function (url) {
        // return full URL to API here
        return constants_1.APPConstants.BASE_URL + url;
    };
    /**
     * Request interceptor.
     */
    HttpService.prototype.requestInterceptor = function () {
        this.loaderService.showLoader();
    };
    /**
     * Response interceptor.
     */
    HttpService.prototype.responseInterceptor = function () {
        this.loaderService.hideLoader();
    };
    /**
     * Error handler.
     * @param error
     * @param caught
     * @returns {ErrorObservable}
     */
    HttpService.prototype.onCatch = function (error, caught) {
        return Observable_1.Observable.throw(error);
    };
    /**
     * onSubscribeSuccess
     * @param res
     */
    HttpService.prototype.onSubscribeSuccess = function (res) {
    };
    /**
     * onSubscribeError
     * @param error
     */
    HttpService.prototype.onSubscribeError = function (error) {
        console.log(error.json().payload.error);
        this.toastManager.error(error.json().payload.error);
        if (error.status == 401) {
            this.userService.setUser(null);
            this.userService.setToken("");
            this.userService.setPrivileges(null);
            localStorage.clear();
            this.router.navigate(['/' + constants_1.RouteConstants.APP_LOGIN]);
        }
    };
    /**
     * onFinally
     */
    HttpService.prototype.onFinally = function () {
        this.responseInterceptor();
    };
    HttpService.prototype.createHeaders = function () {
        var token = this.userService.getToken();
        var headers = new http_1.Headers(token ? { 'Authorization': "bearer " + token } : null);
        headers.append("Accept", 'application/json');
        return headers;
    };
    HttpService = __decorate([
        core_1.Injectable()
    ], HttpService);
    return HttpService;
}(http_1.Http));
exports.HttpService = HttpService;
//# sourceMappingURL=http-service.js.map