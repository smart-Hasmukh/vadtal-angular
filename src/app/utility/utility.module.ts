import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ToastModule, ToastsManager} from "ng2-toastr/ng2-toastr";
import {ConnectionBackend, RequestOptions, XHRBackend} from "@angular/http";
import {HttpService} from "./http-service";
import {ProgressloaderComponent} from "./progressloader/progressloader.component";
import {ProgressloaderService} from "./progressloader/progressloader.service";
import {PagenotfoundComponent} from "./pagenotfound/pagenotfound.component";
import {UserService} from "../sharedService/user.service";
import {Router} from "@angular/router";

@NgModule({
    imports: [
        CommonModule,
        ToastModule
    ],
    declarations: [ProgressloaderComponent, PagenotfoundComponent],
    providers: [
        {
            provide: HttpService,
            useFactory: (backend: ConnectionBackend, options: RequestOptions,
                         loaderService: ProgressloaderService, toastManager: ToastsManager, userService: UserService, router: Router) => {
                return new HttpService(backend, options, loaderService, toastManager, userService,router);
            },
            deps: [XHRBackend, RequestOptions, ProgressloaderService, ToastsManager, UserService,Router]
        },
        ProgressloaderService,
        ToastsManager
    ],
    exports: [
        ProgressloaderComponent,
        PagenotfoundComponent
    ]
})

export class UtilityModule {
}
