import {Base64} from "./base64-typescript-class";
export class GeneralFunction {
    public static ENCRYPT_OBJECT(value): string {
        let base64 = new Base64();
        return base64.encode(value);
    }

    public static DECRYPT_OBJECT(value): string {
        if (value && value != "null" && value != null && value != "undefined") {
            let base64 = new Base64();
            return base64.decode(value);
        }
        return '';
    }

    public static ConvertIntToBoolean(value): Boolean {
        if(value==null)
            return false;
        if (value == 1) {
            return true;
        } else {
            return false;
        }
    }


}