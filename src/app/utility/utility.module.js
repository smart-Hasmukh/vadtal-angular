"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ng2_toastr_1 = require("ng2-toastr/ng2-toastr");
var http_1 = require("@angular/http");
var http_service_1 = require("./http-service");
var progressloader_component_1 = require("./progressloader/progressloader.component");
var progressloader_service_1 = require("./progressloader/progressloader.service");
var pagenotfound_component_1 = require("./pagenotfound/pagenotfound.component");
var user_service_1 = require("../sharedService/user.service");
var router_1 = require("@angular/router");
var UtilityModule = (function () {
    function UtilityModule() {
    }
    UtilityModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                ng2_toastr_1.ToastModule
            ],
            declarations: [progressloader_component_1.ProgressloaderComponent, pagenotfound_component_1.PagenotfoundComponent],
            providers: [
                {
                    provide: http_service_1.HttpService,
                    useFactory: function (backend, options, loaderService, toastManager, userService, router) {
                        return new http_service_1.HttpService(backend, options, loaderService, toastManager, userService, router);
                    },
                    deps: [http_1.XHRBackend, http_1.RequestOptions, progressloader_service_1.ProgressloaderService, ng2_toastr_1.ToastsManager, user_service_1.UserService, router_1.Router]
                },
                progressloader_service_1.ProgressloaderService,
                ng2_toastr_1.ToastsManager
            ],
            exports: [
                progressloader_component_1.ProgressloaderComponent,
                pagenotfound_component_1.PagenotfoundComponent
            ]
        })
    ], UtilityModule);
    return UtilityModule;
}());
exports.UtilityModule = UtilityModule;
//# sourceMappingURL=utility.module.js.map