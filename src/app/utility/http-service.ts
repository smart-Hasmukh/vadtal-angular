import {Injectable} from '@angular/core';
import {
    Http,
    ConnectionBackend,
    RequestOptions,
    RequestOptionsArgs,
    Response,
    Headers, Request
} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {APPConstants, KeyConstants, RouteConstants} from "./constants";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import {ProgressloaderService} from "./progressloader/progressloader.service";
import {UserService} from "../sharedService/user.service";
import {Router} from "@angular/router";

@Injectable()
export class HttpService extends Http {

    constructor(backend: ConnectionBackend,
                defaultOptions: RequestOptions,
                private  loaderService : ProgressloaderService,
                private toastManager : ToastsManager,private userService : UserService,private router: Router) {
        super(backend, defaultOptions);

    }

    /**
     * Performs any type of http request.
     * @param url
     * @param options
     * @returns {Observable<Response>}
     */

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options);
    }

    /**
     * Performs a request with `get` http method.
     * @param url
     * @param options
     * @returns {Observable<>}
     */

    get(url: string, options?: RequestOptionsArgs): Observable<any> {
        this.requestInterceptor();
        console.log("===>URL",this.getFullUrl(url));
        return super.get(this.getFullUrl(url), this.requestOptions())
            .catch(this.onCatch)
            .do((res: Response) => {
                console.log("===>Response",res);
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                console.log("===>Error",error);
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    getLocal(url: string, options?: RequestOptionsArgs): Observable<any> {

        return super.get(url, options);
    }

    /**
     * Performs a request with `post` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */

    post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
        this.requestInterceptor();
        console.log("URL============>",this.getFullUrl(url));
        console.log("Body============>" ,body);
        return super.post(this.getFullUrl(url), body, this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                console.log("Response============>",res);
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                console.log("Error============>",error);
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    postWithFile (url: string, postData: any, filesObj: any, options?: RequestOptionsArgs): Observable<any>  {
        this.requestInterceptor();
        let headers = new Headers();
        let formData:FormData = new FormData();
        for(var obj of filesObj){
            let imgFilesObj : File[] = obj['files'];
            for (let i = 0; i < imgFilesObj.length; i++) {
                formData.append(obj["reqKey"], imgFilesObj[i], imgFilesObj[i].name);
            }
        }
        if(postData !=="" && postData !== undefined && postData !==null){
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        console.log("Body============>" ,formData);
        console.log("URL============>",this.getFullUrl(url));
        return super.post(this.getFullUrl(url), formData, this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                console.log("Response============>",res);
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                console.log("Error============>",error);
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    /**
     * Performs a request with `put` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
        this.requestInterceptor();
        console.log("URL============>",this.getFullUrl(url));
        return super.put(this.getFullUrl(url), body, this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                console.log("Response============>",res);
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                console.log("Error============>",error);
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    /**
     * Performs a request with `put` http method.
     * @param url
     * @param body
     * @param options
     * @returns {Observable<>}
     */

    putWithFile (url: string, postData: any, filesObj: any, options?: RequestOptionsArgs): Observable<any>  {
        let headers = new Headers();
        let formData:FormData = new FormData();
        for(var obj of filesObj){
            let imgFilesObj : File[] = obj['files'];
            for (let i = 0; i < imgFilesObj.length; i++) {
                formData.append(obj["reqKey"], imgFilesObj[i], imgFilesObj[i].name);
            }
        }
        if(postData !=="" && postData !== undefined && postData !==null){
            for (var property in postData) {
                if (postData.hasOwnProperty(property)) {
                    formData.append(property, postData[property]);
                }
            }
        }
        console.log("Body============>" ,formData);
        console.log("URL============>",this.getFullUrl(url));
        return super.put(this.getFullUrl(url), formData, this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                console.log("Response============>",res);
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                console.log("Error============>",error);
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    /**
     * Performs a request with `delete` http method.
     * @param url
     * @param options
     * @returns {Observable<>}
     */

    delete(url: string, options?: RequestOptionsArgs): Observable<any> {
        this.requestInterceptor();
        console.log("URL============>",this.getFullUrl(url));
        return super.delete(this.getFullUrl(url), this.requestOptions(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                console.log("Response============>",res);
                this.onSubscribeSuccess(res);
            }, (error: any) => {
                console.log("Error============>",error);
                this.onSubscribeError(error);
            })
            .finally(() => {
                this.onFinally();
            });
    }

    /**
     * Request options.
     * @param options
     * @returns {RequestOptionsArgs}
     */

    private requestOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = this.createHeaders();
        }
        return options;
    }

    /**
     * Build API url.
     * @param url
     * @returns {string}
     */
    private getFullUrl(url: string): string {
        // return full URL to API here
        return APPConstants.BASE_URL + url;
    }

    /**
     * Request interceptor.
     */
    private requestInterceptor(): void {
        this.loaderService.showLoader()
    }

    /**
     * Response interceptor.
     */

    private responseInterceptor(): void {
        this.loaderService.hideLoader()
    }

    /**
     * Error handler.
     * @param error
     * @param caught
     * @returns {ErrorObservable}
     */

    private onCatch(error: any, caught: Observable<any>): Observable<any> {
        return Observable.throw(error);
    }

    /**
     * onSubscribeSuccess
     * @param res
     */

    private onSubscribeSuccess(res: Response): void {

    }

    /**
     * onSubscribeError
     * @param error
     */

    private onSubscribeError(error: any): void {
        console.log(error.json().payload.error)
        this.toastManager.error(error.json().payload.error);
        if (error.status == 401) {
            this.userService.setUser(null);
            this.userService.setToken("");
            this.userService.setPrivileges(null);
            localStorage.clear();
            this.router.navigate(['/' + RouteConstants.APP_LOGIN]);
        }
    }

    /**
     * onFinally
     */

    private onFinally(): void {
        this.responseInterceptor();

    }

    createHeaders() {
        const token = this.userService.getToken();
        let headers = new Headers(token ? {'Authorization': `bearer ${token}`} : null);
        headers.append("Accept",'application/json')
        return headers
    }
}

