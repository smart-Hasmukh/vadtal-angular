import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "./app-routing/app-routing.module";
import {AppComponent} from "./app.component";
import {UtilityModule} from "./utility/utility.module";
import {AuthGaurd} from "./_gaurds/auth.gaurds";
import {SigninForgotpasswordService} from "./signin-forgotpassword/signin-forgotpassword.service";
import {SigninForgotpasswordModule} from "./signin-forgotpassword/signin-forgotpassword.module";
import {Md2Module} from "md2";
import {MaterialModule} from "@angular/material";
import {UserService} from "./sharedService/user.service";
import {HttpModule} from "@angular/http";
import {Ng2PaginationModule} from "ng2-pagination";
import {MomentModule} from "angular2-moment";
import {CountryService} from "./country/country.service";
import {CityService} from "./city/city.service";
import {StateService} from "./state/state.service";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        AppRoutingModule,
        MaterialModule.forRoot(),
        Md2Module.forRoot(),
        UtilityModule,
        MomentModule,
        SigninForgotpasswordModule,
        Ng2PaginationModule
    ],
    providers: [
        AuthGaurd,
        SigninForgotpasswordService,
        UserService,
        CountryService,
        StateService,
        CityService,
    ],
    bootstrap: [AppComponent]
})

export class AppModule {

}
