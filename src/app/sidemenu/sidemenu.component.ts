import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {TitleConstants, RouteConstants} from "../utility/constants";

@Component({
    selector: 'app-sidemenu',
    templateUrl: 'sidemenu.component.html',
    styleUrls: ['sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {

    @Input()
    slideRef: any;

    titleConstants = new TitleConstants();

    constructor() {
    }

    ngOnInit() {
    }

    get routeHome() {
        return RouteConstants.APP_HOME;
    }

    get routeUser() {
        return RouteConstants.APP_USER;
    }

    get routeDonation() {
        return RouteConstants.APP_DONATION;
    }

    get routeAccommodation(){
        return RouteConstants.APP_ACCOMODATION;
    }

    get routeTemple(){
        return RouteConstants.APP_TEMPLE_MANAGEMENT;
    }

    get routeReport(){
        return RouteConstants.APP_REPORT;
    }
}
