import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenuComponent } from './sidemenu.component';
import { MaterialModule } from '@angular/material';
import {HomeRoutingModule} from "../home/homerouting.module";

@NgModule({
  imports: [
      CommonModule,
      HomeRoutingModule,
      MaterialModule,
  ],
  declarations: [
      SidemenuComponent
  ],
  exports : [
      SidemenuComponent
  ]
})
export class SidemenuModule { }
