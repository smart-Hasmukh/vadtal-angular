"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../utility/constants");
var SidemenuComponent = (function () {
    function SidemenuComponent() {
        this.titleConstants = new constants_1.TitleConstants();
    }
    SidemenuComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(SidemenuComponent.prototype, "routeHome", {
        get: function () {
            return constants_1.RouteConstants.APP_HOME;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidemenuComponent.prototype, "routeUser", {
        get: function () {
            return constants_1.RouteConstants.APP_USER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidemenuComponent.prototype, "routeDonation", {
        get: function () {
            return constants_1.RouteConstants.APP_DONATION;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidemenuComponent.prototype, "routeAccommodation", {
        get: function () {
            return constants_1.RouteConstants.APP_ACCOMODATION;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidemenuComponent.prototype, "routeTemple", {
        get: function () {
            return constants_1.RouteConstants.APP_TEMPLE_MANAGEMENT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SidemenuComponent.prototype, "routeReport", {
        get: function () {
            return constants_1.RouteConstants.APP_REPORT;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input()
    ], SidemenuComponent.prototype, "slideRef");
    SidemenuComponent = __decorate([
        core_1.Component({
            selector: 'app-sidemenu',
            templateUrl: 'sidemenu.component.html',
            styleUrls: ['sidemenu.component.css']
        })
    ], SidemenuComponent);
    return SidemenuComponent;
}());
exports.SidemenuComponent = SidemenuComponent;
//# sourceMappingURL=sidemenu.component.js.map