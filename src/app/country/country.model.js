"use strict";
var Country = (function () {
    function Country() {
    }
    Object.defineProperty(Country.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Country.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Country.prototype, "phoneCode", {
        get: function () {
            return this._phoneCode;
        },
        set: function (value) {
            this._phoneCode = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Country.prototype, "createdBy", {
        get: function () {
            return this._createdBy;
        },
        set: function (value) {
            this._createdBy = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Country.prototype, "updatedBy", {
        get: function () {
            return this._updatedBy;
        },
        set: function (value) {
            this._updatedBy = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Country.prototype, "isEnable", {
        get: function () {
            return this._isEnable;
        },
        set: function (value) {
            this._isEnable = value;
        },
        enumerable: true,
        configurable: true
    });
    return Country;
}());
exports.Country = Country;
//# sourceMappingURL=country.model.js.map