export class Country {

    private _id: number;
    private _name: string;
    private _phoneCode: string;
    private _createdBy: number;
    private _updatedBy: number;
    private _isEnable: number;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get phoneCode(): string {
        return this._phoneCode;
    }

    set phoneCode(value: string) {
        this._phoneCode = value;
    }

    get createdBy(): number {
        return this._createdBy;
    }

    set createdBy(value: number) {
        this._createdBy = value;
    }

    get updatedBy(): number {
        return this._updatedBy;
    }

    set updatedBy(value: number) {
        this._updatedBy = value;
    }

    get isEnable(): number {
        return this._isEnable;
    }

    set isEnable(value: number) {
        this._isEnable = value;
    }
}