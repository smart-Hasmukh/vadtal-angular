"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var constants_1 = require("../utility/constants");
var home_component_1 = require("./home.component");
var appboard_component_1 = require("./appboard/appboard.component");
var routes = [
    {
        path: '',
        component: home_component_1.HomeComponent,
        children: [
            {
                path: '',
                component: appboard_component_1.AppboardComponent,
                data: { title: "Home" }
            },
            {
                path: constants_1.RouteConstants.APP_USER,
                loadChildren: 'app/user/user.module#UserModule',
                data: { title: "Users" }
            },
            {
                path: constants_1.RouteConstants.APP_DONATION,
                loadChildren: 'app/donation/donation.module#DonationModule',
                data: { title: "Donation-Dashboard" }
            },
            {
                path: constants_1.RouteConstants.APP_ACCOMODATION,
                loadChildren: 'app/accomodation/accomodation.module#AccomodationModule',
                data: { title: "Accomodation" }
            },
            {
                path: constants_1.RouteConstants.APP_TEMPLE_MANAGEMENT,
                loadChildren: 'app/templemanagement/templemanagement.module#TemplemanagementModule',
                data: { title: "Temples" }
            },
            {
                path: constants_1.RouteConstants.APP_REPORT,
                loadChildren: 'app/report/report.module#ReportModule',
                data: { title: "Reports" }
            },
            {
                path: constants_1.RouteConstants.APP_ADMIN_ROLES,
                loadChildren: 'app/adminroles/adminroles.module#AdminrolesModule',
                data: { title: "Admin Roles" }
            }
        ]
    }
];
var HomeRoutingModule = (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(routes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());
exports.HomeRoutingModule = HomeRoutingModule;
//# sourceMappingURL=homerouting.module.js.map