import { Component, style, state, animate, transition, trigger } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {ActivatedRoute, Router} from "@angular/router";
import {TitleConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";

@Component({
  selector: 'app-appboard',
  templateUrl: './appboard.component.html',
  styleUrls: ['./appboard.component.css'],
  animations: [
    trigger('testingBottom', [
      state('active', style({transform: 'scale(1)'})),
      transition('void => *', [
        style({transform: 'scaleY(0)'}),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({transform: 'scaleY(0)'}))
      ])
    ])
  ]
})
export class AppboardComponent {
  public moreControlFlag = false;
  titleConstants = new TitleConstants();

  constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.data.subscribe(data => {
      this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
    })
  }
  ngOnInit(): void {
    this.title.setTitle(this.titleConstants.HOME);
  }

  loading_overlay_status = false;
}
