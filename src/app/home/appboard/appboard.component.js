"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var AppboardComponent = (function () {
    function AppboardComponent(title, breadcrumbService, activatedRoute, router) {
        var _this = this;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.moreControlFlag = false;
        this.titleConstants = new constants_1.TitleConstants();
        this.loading_overlay_status = false;
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
        });
    }
    AppboardComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.HOME);
    };
    AppboardComponent = __decorate([
        core_1.Component({
            selector: 'app-appboard',
            templateUrl: './appboard.component.html',
            styleUrls: ['./appboard.component.css'],
            animations: [
                core_1.trigger('testingBottom', [
                    core_1.state('active', core_1.style({ transform: 'scale(1)' })),
                    core_1.transition('void => *', [
                        core_1.style({ transform: 'scaleY(0)' }),
                        core_1.animate(100)
                    ]),
                    core_1.transition('* => void', [
                        core_1.animate(100, core_1.style({ transform: 'scaleY(0)' }))
                    ])
                ])
            ]
        })
    ], AppboardComponent);
    return AppboardComponent;
}());
exports.AppboardComponent = AppboardComponent;
//# sourceMappingURL=appboard.component.js.map