import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RouteConstants} from "../utility/constants";
import {HomeComponent} from "./home.component";
import {AppboardComponent} from "./appboard/appboard.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: AppboardComponent,
        data: {title: "Home"}
      },
      {
        path: RouteConstants.APP_USER,
        loadChildren: 'app/user/user.module#UserModule',
        data: {title: "Users"}
      },
      {
        path: RouteConstants.APP_DONATION,
        loadChildren: 'app/donation/donation.module#DonationModule',
        data: {title: "Donation-Dashboard"},
      },
      {
        path: RouteConstants.APP_ACCOMODATION,
        loadChildren: 'app/accomodation/accomodation.module#AccomodationModule',
        data: {title: "Accomodation"},
      },
      {
        path: RouteConstants.APP_TEMPLE_MANAGEMENT,
        loadChildren: 'app/templemanagement/templemanagement.module#TemplemanagementModule',
        data: {title: "Temples"},
      },
      {
        path: RouteConstants.APP_REPORT,
        loadChildren: 'app/report/report.module#ReportModule',
        data: {title: "Reports"},
      },
      {
        path: RouteConstants.APP_ADMIN_ROLES,
        loadChildren: 'app/adminroles/adminroles.module#AdminrolesModule',
        data: {title: "Admin Roles"},
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class HomeRoutingModule {
}


