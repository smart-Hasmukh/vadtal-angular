"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var material_1 = require("@angular/material");
var header_module_1 = require("../header/header.module");
var homerouting_module_1 = require("./homerouting.module");
var home_component_1 = require("./home.component");
var appboard_component_1 = require("./appboard/appboard.component");
var sidemenu_module_1 = require("../sidemenu/sidemenu.module");
var header_breadcrumb_service_1 = require("../header/header-breadcrumb.service");
var HomeModule = (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                homerouting_module_1.HomeRoutingModule,
                header_module_1.HeaderModule,
                sidemenu_module_1.SidemenuModule,
            ],
            providers: [header_breadcrumb_service_1.HeaderBreadcrumbService],
            declarations: [home_component_1.HomeComponent, appboard_component_1.AppboardComponent]
        })
    ], HomeModule);
    return HomeModule;
}());
exports.HomeModule = HomeModule;
//# sourceMappingURL=home.module.js.map