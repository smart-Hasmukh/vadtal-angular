import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "@angular/material";
import {HeaderModule} from "../header/header.module";
import {HomeRoutingModule} from "./homerouting.module";
import {HomeComponent} from "./home.component";
import {AppboardComponent} from "./appboard/appboard.component";
import {SidemenuModule} from "../sidemenu/sidemenu.module";
import {HeaderBreadcrumbService} from "../header/header-breadcrumb.service";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HomeRoutingModule,
    HeaderModule,
    SidemenuModule,
  ],
  providers : [HeaderBreadcrumbService],
  declarations: [HomeComponent, AppboardComponent],
})

export class HomeModule {

}
