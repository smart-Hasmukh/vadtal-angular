import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {UserComponent} from "./user.component";
import {UserlistComponent} from "./userlist/userlist.component";
import {AdduserComponent} from "./adduser/adduser.component";
import {UserdetailComponent} from "./userdetail/userdetail.component";
import {RouteConstants} from "../utility/constants";
import {UserprivilegeComponent} from "./userprivilege/userprivilege.component";

const routes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [
            {
                path: '',
                component: UserlistComponent,
                data: {title: "Users"}
            },
            {
                path: RouteConstants.APP_USER_ADD,
                component: AdduserComponent,
                data: {title: "Add User"}
            },
            {
                path: RouteConstants.APP_USER_DETAIL,
                component: UserdetailComponent,
                data: {title: "User Detail"}
            },
            {
                path: RouteConstants.APP_USER_PRIVILEGE,
                component: UserprivilegeComponent,
                data: {title: "User Privileges"}
            },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class UserRoutingModule {
}


