import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {TitleConstants, HttpStatusConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";
import {User} from "../user.model";
import {URLSearchParams, RequestOptions} from "@angular/http";

@Component({
    selector: 'app-userdetail',
    templateUrl: './userdetail.component.html',
    styleUrls: ['./userdetail.component.css'],
    providers: [UserService]
})
export class UserdetailComponent implements OnInit {
    titleConstants = new TitleConstants();

    id: number;
    user: User;

    constructor(private userService: UserService, private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {

        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
            this.id = data['id'];
        })

        activatedRoute.queryParams.subscribe(
            data => this.id = data['id']);
        console.log(this.id);
        this.getUsersDetail(this.id);

    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.USER_DETAIL);
    }

    /*isCollapsedImage: boolean = false;*/
    activeBtnValue = "Active";
    suspendBtnValue = "Suspend";

    activeUser() {
        if (this.user.isEnable == 0) {
            let options = new RequestOptions({
                search: new URLSearchParams('status=1')
            });
            this.userService.activateOrSupendUser(this.user.id, options).subscribe(response => {
                console.log(response)
                if (response.status == HttpStatusConstants.SUCCESS) {
                    this.user.isEnable = 1
                    this.activeBtnValue = "Active";
                    this.suspendBtnValue = "Suspend";
                }
            })
        }
    }

    suspendUser() {
        if (this.user.isEnable == 1) {
            let options = new RequestOptions({
                search: new URLSearchParams('status=0')
            });
            this.userService.activateOrSupendUser(this.user.id, options).subscribe(response => {
                if (response.status == HttpStatusConstants.SUCCESS) {
                    this.user.isEnable = 0
                    this.activeBtnValue = "Activate";
                    this.suspendBtnValue = "Suspended";

                }
            })
        }
    }

    getUsersDetail(id: number) {
        this.userService.getUserDetail(id).subscribe(response => {
            if (response.status == HttpStatusConstants.SUCCESS) {
                this.user = response.payload.data;
                console.log(this.user)
            }
        })
    }

    // Add class hoverEffect on userDetils
    isCollapsed;

    hoverEffectClass() {
        if (!this.isCollapsed) {
            return "contactHover";
        } else {
            return "";
        }
    }

    // edit user details model methods
    showEditUserDetails(dialog: any) {
        dialog.show();
    }

    closeEditUserDetails(dialog: any) {
        dialog.close();
    }

    convertIntToBoolean(value): String {
        if (value == null)
            return "false";
        if (value == 1) {
            return "true";
        } else {
            return "false";
        }
    }

    checkValueNull(value): String {
        if (value == "" || value == null) {
            return "-";
        } else {
            return value;
        }
    }


}
