"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var user_service_1 = require("../user.service");
var http_1 = require("@angular/http");
var UserdetailComponent = (function () {
    function UserdetailComponent(userService, title, breadcrumbService, activatedRoute, router) {
        var _this = this;
        this.userService = userService;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.titleConstants = new constants_1.TitleConstants();
        /*isCollapsedImage: boolean = false;*/
        this.activeBtnValue = "Active";
        this.suspendBtnValue = "Suspend";
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
            _this.id = data['id'];
        });
        activatedRoute.queryParams.subscribe(function (data) { return _this.id = data['id']; });
        console.log(this.id);
        this.getUsersDetail(this.id);
    }
    UserdetailComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.USER_DETAIL);
    };
    UserdetailComponent.prototype.activeUser = function () {
        var _this = this;
        if (this.user.isEnable == 0) {
            var options = new http_1.RequestOptions({
                search: new http_1.URLSearchParams('status=1')
            });
            this.userService.activateOrSupendUser(this.user.id, options).subscribe(function (response) {
                console.log(response);
                if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                    _this.user.isEnable = 1;
                    _this.activeBtnValue = "Active";
                    _this.suspendBtnValue = "Suspend";
                }
            });
        }
    };
    UserdetailComponent.prototype.suspendUser = function () {
        var _this = this;
        if (this.user.isEnable == 1) {
            var options = new http_1.RequestOptions({
                search: new http_1.URLSearchParams('status=0')
            });
            this.userService.activateOrSupendUser(this.user.id, options).subscribe(function (response) {
                if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                    _this.user.isEnable = 0;
                    _this.activeBtnValue = "Activate";
                    _this.suspendBtnValue = "Suspended";
                }
            });
        }
    };
    UserdetailComponent.prototype.getUsersDetail = function (id) {
        var _this = this;
        this.userService.getUserDetail(id).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                _this.user = response.payload.data;
                console.log(_this.user);
            }
        });
    };
    UserdetailComponent.prototype.hoverEffectClass = function () {
        if (!this.isCollapsed) {
            return "contactHover";
        }
        else {
            return "";
        }
    };
    // edit user details model methods
    UserdetailComponent.prototype.showEditUserDetails = function (dialog) {
        dialog.show();
    };
    UserdetailComponent.prototype.closeEditUserDetails = function (dialog) {
        dialog.close();
    };
    UserdetailComponent.prototype.convertIntToBoolean = function (value) {
        if (value == null)
            return "false";
        if (value == 1) {
            return "true";
        }
        else {
            return "false";
        }
    };
    UserdetailComponent.prototype.checkValueNull = function (value) {
        if (value == "" || value == null) {
            return "-";
        }
        else {
            return value;
        }
    };
    UserdetailComponent = __decorate([
        core_1.Component({
            selector: 'app-userdetail',
            templateUrl: './userdetail.component.html',
            styleUrls: ['./userdetail.component.css'],
            providers: [user_service_1.UserService]
        })
    ], UserdetailComponent);
    return UserdetailComponent;
}());
exports.UserdetailComponent = UserdetailComponent;
//# sourceMappingURL=userdetail.component.js.map