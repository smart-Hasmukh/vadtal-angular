import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UserComponent} from "./user.component";
import {UserRoutingModule} from "./user-routing.module";
import {MaterialModule} from "@angular/material";
import {UserlistComponent} from './userlist/userlist.component';
import {AdduserComponent} from './adduser/adduser.component';
import {UserdetailComponent} from './userdetail/userdetail.component';
import {UserprivilegeComponent} from './userprivilege/userprivilege.component';
import {Md2Module} from "md2";
import {UserService} from "./user.service";
import {Ng2PaginationModule} from "ng2-pagination";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {SelectModule} from 'angular2-select';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        Ng2PaginationModule,
        UserRoutingModule,
        Md2Module,
        SelectModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [UserService],
    declarations: [UserComponent, UserlistComponent, AdduserComponent, UserdetailComponent, UserprivilegeComponent]
})

export class UserModule {
}
