import {Pages} from "./pages.model";

export class Privileges {
    private _id: number;
    private _name: string;
    private _icon: string;
    private _isEnable: number;
    private _created_at: string;
    private _updated_at: string;
    private _pages: Pages[];


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get icon(): string {
        return this._icon;
    }

    set icon(value: string) {
        this._icon = value;
    }

    get isEnable(): number {
        return this._isEnable;
    }

    set isEnable(value: number) {
        this._isEnable = value;
    }

    get created_at(): string {
        return this._created_at;
    }

    set created_at(value: string) {
        this._created_at = value;
    }

    get updated_at(): string {
        return this._updated_at;
    }

    set updated_at(value: string) {
        this._updated_at = value;
    }

    get pages(): Pages[] {
        return this._pages;
    }

    set pages(value: Pages[]) {
        this._pages = value;
    }
}
