"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../../utility/constants");
var directives_1 = require("@angular/core/src/metadata/directives");
var UserprivilegeComponent = (function () {
    function UserprivilegeComponent(title, breadcrumbService, activatedRoute, router) {
        var _this = this;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.isAllUserPrivileges = false;
        this.isFullAccess = false;
        this.isCreate = false;
        this.isRead = false;
        this.isEdit = false;
        this.isSuspend = false;
        this.isResetPassword = false;
        this.titleConstants = new constants_1.TitleConstants();
        this.onAllUserPrivilgesChange = function () {
            if (_this.isFullAccess && _this.isCreate && _this.isRead && _this.isEdit && _this.isSuspend && _this.isResetPassword) {
                _this.isFullAccess = false;
                _this.isCreate = false;
                _this.isRead = false;
                _this.isEdit = false;
                _this.isSuspend = false;
                _this.isResetPassword = false;
            }
            else {
                _this.isFullAccess = true;
                _this.isCreate = true;
                _this.isRead = true;
                _this.isEdit = true;
                _this.isSuspend = true;
                _this.isResetPassword = true;
            }
        };
        this.onAllAccessChange = function (isChecked) {
            _this.isFullAccess = !_this.isFullAccess;
        };
        this.onCreateChange = function (isChecked) {
            _this.isCreate = !_this.isCreate;
        };
        this.onReadChange = function (isChecked) {
            _this.isRead = !_this.isRead;
        };
        this.onEditChange = function (isChecked) {
            _this.isEdit = !_this.isEdit;
        };
        this.onSuspendChange = function (isChecked) {
            _this.isSuspend = !_this.isSuspend;
        };
        this.onPasswordChange = function (isChecked) {
            _this.isResetPassword = !_this.isResetPassword;
        };
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
        });
    }
    UserprivilegeComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.USER_PRIVILEGE);
    };
    __decorate([
        directives_1.Input()
    ], UserprivilegeComponent.prototype, "privileges");
    UserprivilegeComponent = __decorate([
        core_1.Component({
            selector: 'app-userprivilege',
            templateUrl: './userprivilege.component.html',
            styleUrls: ['./userprivilege.component.css']
        })
    ], UserprivilegeComponent);
    return UserprivilegeComponent;
}());
exports.UserprivilegeComponent = UserprivilegeComponent;
//# sourceMappingURL=userprivilege.component.js.map