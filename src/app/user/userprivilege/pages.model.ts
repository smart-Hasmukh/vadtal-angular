export class Pages {
    private _id: number;
    private _moduleId: number;
    private _name: string;
    private _icon: string;
    private _isReport: string;
    private _created_at: string;
    private _updated_at: string;
    private _canAdd: number;
    private _canEdit: number;
    private _canView: number;
    private _canExport: number;
    private _canCancel: number;
    private _canPrint: number;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get moduleId(): number {
        return this._moduleId;
    }

    set moduleId(value: number) {
        this._moduleId = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get icon(): string {
        return this._icon;
    }

    set icon(value: string) {
        this._icon = value;
    }

    get isReport(): string {
        return this._isReport;
    }

    set isReport(value: string) {
        this._isReport = value;
    }

    get created_at(): string {
        return this._created_at;
    }

    set created_at(value: string) {
        this._created_at = value;
    }

    get updated_at(): string {
        return this._updated_at;
    }

    set updated_at(value: string) {
        this._updated_at = value;
    }

    get canAdd(): number {
        return this._canAdd;
    }

    set canAdd(value: number) {
        this._canAdd = value;
    }

    get canEdit(): number {
        return this._canEdit;
    }

    set canEdit(value: number) {
        this._canEdit = value;
    }

    get canView(): number {
        return this._canView;
    }

    set canView(value: number) {
        this._canView = value;
    }

    get canExport(): number {
        return this._canExport;
    }

    set canExport(value: number) {
        this._canExport = value;
    }

    get canCancel(): number {
        return this._canCancel;
    }

    set canCancel(value: number) {
        this._canCancel = value;
    }

    get canPrint(): number {
        return this._canPrint;
    }

    set canPrint(value: number) {
        this._canPrint = value;
    }
}