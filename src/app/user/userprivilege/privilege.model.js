"use strict";
var Privileges = (function () {
    function Privileges() {
    }
    Object.defineProperty(Privileges.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Privileges.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Privileges.prototype, "icon", {
        get: function () {
            return this._icon;
        },
        set: function (value) {
            this._icon = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Privileges.prototype, "isEnable", {
        get: function () {
            return this._isEnable;
        },
        set: function (value) {
            this._isEnable = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Privileges.prototype, "created_at", {
        get: function () {
            return this._created_at;
        },
        set: function (value) {
            this._created_at = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Privileges.prototype, "updated_at", {
        get: function () {
            return this._updated_at;
        },
        set: function (value) {
            this._updated_at = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Privileges.prototype, "pages", {
        get: function () {
            return this._pages;
        },
        set: function (value) {
            this._pages = value;
        },
        enumerable: true,
        configurable: true
    });
    return Privileges;
}());
exports.Privileges = Privileges;
//# sourceMappingURL=privilege.model.js.map