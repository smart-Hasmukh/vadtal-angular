import {Component, OnInit} from '@angular/core';
import {TitleConstants} from "../../utility/constants";
import {Title} from "@angular/platform-browser";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";
import {isBoolean} from "util";
import {User} from "../user.model";
import {Input} from "@angular/core/src/metadata/directives";
import {Pages} from "./pages.model";

@Component({
    selector: 'app-userprivilege',
    templateUrl: './userprivilege.component.html',
    styleUrls: ['./userprivilege.component.css']
})
export class UserprivilegeComponent implements OnInit {
    @Input()
    privileges: Pages;
    isAllUserPrivileges = false;
    isFullAccess = false;
    isCreate = false;
    isRead = false;
    isEdit = false;
    isSuspend = false;
    isResetPassword = false;

    titleConstants = new TitleConstants();

    constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })

    }

    onAllUserPrivilgesChange = () => {
        if (this.isFullAccess && this.isCreate && this.isRead && this.isEdit && this.isSuspend && this.isResetPassword) {
            this.isFullAccess = false;
            this.isCreate = false;
            this.isRead = false;
            this.isEdit = false;
            this.isSuspend = false;
            this.isResetPassword = false;
        } else {
            this.isFullAccess = true;
            this.isCreate = true;
            this.isRead = true;
            this.isEdit = true;
            this.isSuspend = true;
            this.isResetPassword = true;
        }
    }

    onAllAccessChange = (isChecked: boolean) => {
        this.isFullAccess = !this.isFullAccess;
    };

    onCreateChange = (isChecked: boolean) => {
        this.isCreate = !this.isCreate;
    };

    onReadChange = (isChecked: boolean) => {
        this.isRead = !this.isRead;
    };

    onEditChange = (isChecked: boolean) => {
        this.isEdit = !this.isEdit;
    };

    onSuspendChange = (isChecked: boolean) => {
        this.isSuspend = !this.isSuspend;
    };

    onPasswordChange = (isChecked: boolean) => {
        this.isResetPassword = !this.isResetPassword;
    };

    ngOnInit() {
        this.title.setTitle(this.titleConstants.USER_PRIVILEGE);
    }

}
