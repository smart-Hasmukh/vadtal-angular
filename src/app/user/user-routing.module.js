"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var user_component_1 = require("./user.component");
var userlist_component_1 = require("./userlist/userlist.component");
var adduser_component_1 = require("./adduser/adduser.component");
var userdetail_component_1 = require("./userdetail/userdetail.component");
var constants_1 = require("../utility/constants");
var userprivilege_component_1 = require("./userprivilege/userprivilege.component");
var routes = [
    {
        path: '',
        component: user_component_1.UserComponent,
        children: [
            {
                path: '',
                component: userlist_component_1.UserlistComponent,
                data: { title: "Users" }
            },
            {
                path: constants_1.RouteConstants.APP_USER_ADD,
                component: adduser_component_1.AdduserComponent,
                data: { title: "Add User" }
            },
            {
                path: constants_1.RouteConstants.APP_USER_DETAIL,
                component: userdetail_component_1.UserdetailComponent,
                data: { title: "User Detail" }
            },
            {
                path: constants_1.RouteConstants.APP_USER_PRIVILEGE,
                component: userprivilege_component_1.UserprivilegeComponent,
                data: { title: "User Privileges" }
            },
        ]
    },
];
var UserRoutingModule = (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(routes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());
exports.UserRoutingModule = UserRoutingModule;
//# sourceMappingURL=user-routing.module.js.map