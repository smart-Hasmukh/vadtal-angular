import {Component, OnInit} from '@angular/core';
import {TitleConstants} from "../utility/constants";
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    titleConstants = new TitleConstants();

    constructor(private title: Title) {
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.USER);
    }

}
