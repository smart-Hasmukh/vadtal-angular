"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../../utility/constants");
var user_service_1 = require("../user.service");
var UserlistComponent = (function () {
    function UserlistComponent(userService, title, breadcrumbService, activatedRoute, router) {
        var _this = this;
        this.userService = userService;
        this.title = title;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.basicRowHeight = 50;
        this.fixedRowHeight = 100;
        this.ratioGutter = 1;
        this.fitListHeight = '300px';
        this.ratio = '4:1';
        this.titleConstants = new constants_1.TitleConstants();
        this.appConstant = new constants_1.APPConstants();
        this.activatedRoute.data.subscribe(function (data) {
            _this.breadcrumbService.addFriendlyNameForRoute(_this.router.url, data["title"]);
        });
    }
    UserlistComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.USER_LIST);
        this.getUsers(1);
    };
    UserlistComponent.prototype.getUsers = function (pageIndex) {
        var _this = this;
        var searchParams = "?pageNumber=" + pageIndex;
        this.userService.getUserList(pageIndex).subscribe(function (response) {
            if (response.status == constants_1.HttpStatusConstants.SUCCESS) {
                // this.eventCount = response['count'];
                _this.pager = response.pager;
                _this.users = response.payload.data;
                _this.totalCount = _this.pager.totalRecords;
            }
        });
    };
    UserlistComponent = __decorate([
        core_1.Component({
            selector: 'app-user-list',
            templateUrl: './userlist.component.html',
            styleUrls: ['./userlist.component.css'],
            providers: [user_service_1.UserService]
        })
    ], UserlistComponent);
    return UserlistComponent;
}());
exports.UserlistComponent = UserlistComponent;
//# sourceMappingURL=userlist.component.js.map