import {Component, OnInit} from "@angular/core";
import {Title} from "@angular/platform-browser";
import {TitleConstants, HttpStatusConstants, APPConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";
import {User} from "../user.model";
import {Pager} from "../../templemanagement/pager.model";
import {URLSearchParams, RequestOptions} from "@angular/http";

@Component({
    selector: 'app-user-list',
    templateUrl: './userlist.component.html',
    styleUrls: ['./userlist.component.css'],
    providers: [UserService]
})
export class UserlistComponent implements OnInit {

    basicRowHeight = 50;
    fixedRowHeight = 100;
    ratioGutter = 1;
    fitListHeight = '300px';
    ratio = '4:1';
    titleConstants = new TitleConstants();
    appConstant = new APPConstants();
    users: User[];
    pager: Pager;
    totalCount: number;

    constructor(private userService: UserService, private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    ngOnInit() {
        this.title.setTitle(this.titleConstants.USER_LIST);
        this.getUsers(1);
    }

    getUsers(pageIndex: number) {
        let searchParams = `?pageNumber=${pageIndex}`
        this.userService.getUserList(pageIndex).subscribe(response => {
            if (response.status == HttpStatusConstants.SUCCESS) {
                // this.eventCount = response['count'];
                this.pager = response.pager;
                this.users = response.payload.data;
                this.totalCount = this.pager.totalRecords;
                // this.appConstant.PAGINATION_SIZE = this.pager.recordsPerPage;
            }
        })
    }



}
