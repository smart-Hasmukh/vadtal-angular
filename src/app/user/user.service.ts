import { Injectable } from '@angular/core';
import {HttpService} from "../utility/http-service";
import {ToastsManager} from "ng2-toastr";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../utility/constants";
import {Response} from "@angular/http";

@Injectable()
export class UserService {

  constructor(private  httpService: HttpService,private toastManager : ToastsManager) { }

  getUserList(pageIndex : number, recordsPerPage? : number, search? : any): Observable<any> {
    let queryString : string = "?recordsPerPage="+ recordsPerPage + "&pageNumber=" + pageIndex;
    if(search){
      queryString = queryString + "&search=" + JSON.stringify(search);
    }
    console.log("------------>", queryString);
    return this.httpService.get(ApiEventConstants.USER + queryString).map(res => this.extractData(res, false));
  }

  getUserDetail(id : number): Observable<any> {
    return this.httpService.get(ApiEventConstants.USER+"/"+id).map(res => this.extractData(res, false));
  }

  activateOrSupendUser(id,options): Observable<any> {
    return this.httpService
        .delete(ApiEventConstants.USER+"/"+id+"/",options)
        .map(res => this.extractData(res, true));
  }

  private extractData(res: Response, showToast: boolean) {
    if (showToast) {
      this.toastManager.success(res.json().message)
    }
    let data = res.json();
    return data || {};
  }

}
