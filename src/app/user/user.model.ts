import {City} from "../city/city.model";
import {State} from "../state/state.model";
import {Country} from "../country/country.model";
import {Temple} from "../templemanagement/temple.model";
import {Pages} from "./userprivilege/pages.model";
import {Roles} from "../adminroles/roles.model";

export class User {
    private _id: number;
    private _username: string;
    private _firstName: string;
    private _lastName: string;
    private email: string;
    private landlineNo: string;
    private mobileNo: string;
    private localAddress: string;
    private permenantAddress: string;
    private pincode: string;
    private electionCard: string;
    private adharCard: string;
    private panCardNo: string;
    private licenceNo: string;
    private image: string;
    private roleId: number;
    private templeId: number;
    private otp: string;
    private otpCreatedAt: string;
    private updatedBy: number;
    private updated_at: string;
    private _isEnable: number;
    private role: Roles;
    private city: City;
    private state: State;
    private country: Country;
    private temple: Temple;
    private last_login: LastLogin[];
    private privileges: Pages[];

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get isEnable(): number {
        return this._isEnable;
    }

    set isEnable(value: number) {
        this._isEnable = value;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }
}

export class LastLogin {
    private id: number;
    private userId: number;
    private ipAddress: string;
    private domain: string;
    private loginTime: string;
}