"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var user_component_1 = require("./user.component");
var user_routing_module_1 = require("./user-routing.module");
var material_1 = require("@angular/material");
var userlist_component_1 = require('./userlist/userlist.component');
var adduser_component_1 = require('./adduser/adduser.component');
var userdetail_component_1 = require('./userdetail/userdetail.component');
var userprivilege_component_1 = require('./userprivilege/userprivilege.component');
var md2_1 = require("md2");
var user_service_1 = require("./user.service");
var ng2_pagination_1 = require("ng2-pagination");
var forms_1 = require('@angular/forms');
var angular2_select_1 = require('angular2-select');
var UserModule = (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                ng2_pagination_1.Ng2PaginationModule,
                user_routing_module_1.UserRoutingModule,
                md2_1.Md2Module,
                angular2_select_1.SelectModule,
                forms_1.ReactiveFormsModule,
                forms_1.FormsModule
            ],
            providers: [user_service_1.UserService],
            declarations: [user_component_1.UserComponent, userlist_component_1.UserlistComponent, adduser_component_1.AdduserComponent, userdetail_component_1.UserdetailComponent, userprivilege_component_1.UserprivilegeComponent]
        })
    ], UserModule);
    return UserModule;
}());
exports.UserModule = UserModule;
//# sourceMappingURL=user.module.js.map