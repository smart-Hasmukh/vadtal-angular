import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {TitleConstants} from "../../utility/constants";
import {HeaderBreadcrumbService} from "../../header/header-breadcrumb.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-add-user',
    templateUrl: './adduser.component.html',
    styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
    uploadImageStatus = false;
    changeImageStatus = true;
    titleConstants = new TitleConstants();
    constructor(private title: Title, private breadcrumbService: HeaderBreadcrumbService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.activatedRoute.data.subscribe(data => {
            this.breadcrumbService.addFriendlyNameForRoute(this.router.url, data["title"]);
        })
    }

    myOptions: Array<any>;
    mySelectValue: Array<string>; // Array of strings for multi select, string for single select.

    ngOnInit() {
        this.title.setTitle(this.titleConstants.USER_ADD);
        this.myOptions = [
            {value: 'a', label: 'Alpha'},
            {value: 'b', label: 'Beta'},
            {value: 'c', label: 'Gamma'},
        ];
        this.mySelectValue = ['b', 'c'];
    }


}
