"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../utility/constants");
var UserService = (function () {
    function UserService(httpService, toastManager) {
        this.httpService = httpService;
        this.toastManager = toastManager;
    }
    UserService.prototype.getUserList = function (pageIndex, recordsPerPage, search) {
        var _this = this;
        var queryString = "?recordsPerPage=" + recordsPerPage + "&pageNumber=" + pageIndex;
        if (search) {
            queryString = queryString + "&search=" + JSON.stringify(search);
        }
        console.log("------------>", queryString);
        return this.httpService.get(constants_1.ApiEventConstants.USER + queryString).map(function (res) { return _this.extractData(res, false); });
    };
    UserService.prototype.getUserDetail = function (id) {
        var _this = this;
        return this.httpService.get(constants_1.ApiEventConstants.USER + "/" + id).map(function (res) { return _this.extractData(res, false); });
    };
    UserService.prototype.activateOrSupendUser = function (id, options) {
        var _this = this;
        return this.httpService
            .delete(constants_1.ApiEventConstants.USER + "/" + id + "/", options)
            .map(function (res) { return _this.extractData(res, true); });
    };
    UserService.prototype.extractData = function (res, showToast) {
        if (showToast) {
            this.toastManager.success(res.json().message);
        }
        var data = res.json();
        return data || {};
    };
    UserService = __decorate([
        core_1.Injectable()
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map