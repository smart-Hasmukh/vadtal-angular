"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var directives_1 = require("@angular/core/src/metadata/directives");
var AppComponent = (function () {
    function AppComponent(toastr, vRef, userService) {
        this.toastr = toastr;
        this.userService = userService;
        this.toastr.setRootViewContainerRef(vRef);
    }
    AppComponent.prototype.doSomething = function ($event) {
        // localStorage.clear();
        this.userService.isSessionTimeout();
    };
    __decorate([
        directives_1.HostListener('window:beforeunload', ['$event'])
    ], AppComponent.prototype, "doSomething");
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map