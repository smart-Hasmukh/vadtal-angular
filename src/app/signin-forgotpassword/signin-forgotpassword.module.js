"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var signin_forgotpassword_component_1 = require("./signin-forgotpassword.component");
var material_1 = require("@angular/material");
var signin_forgotpassword_routing_1 = require("./signin-forgotpassword.routing");
var md2_1 = require("md2");
var SigninForgotpasswordModule = (function () {
    function SigninForgotpasswordModule() {
    }
    SigninForgotpasswordModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                signin_forgotpassword_routing_1.SigninForgotpasswordRoutingModule,
                md2_1.Md2Module
            ],
            declarations: [signin_forgotpassword_component_1.SigninForgotpasswordComponent],
            exports: [signin_forgotpassword_component_1.SigninForgotpasswordComponent]
        })
    ], SigninForgotpasswordModule);
    return SigninForgotpasswordModule;
}());
exports.SigninForgotpasswordModule = SigninForgotpasswordModule;
//# sourceMappingURL=signin-forgotpassword.module.js.map