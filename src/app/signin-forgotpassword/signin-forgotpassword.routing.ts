import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RouteConstants} from "../utility/constants";
import {SigninForgotpasswordComponent} from "./signin-forgotpassword.component";
import {AuthGaurd} from "../_gaurds/auth.gaurds";

const routes: Routes = [
    {
        path: RouteConstants.APP_LOGIN,
        component: SigninForgotpasswordComponent,
        canActivate: [AuthGaurd]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ]
})

export class SigninForgotpasswordRoutingModule {
}
