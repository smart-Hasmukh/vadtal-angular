/* tslint:disable:no-unused-variable */
"use strict";
var testing_1 = require('@angular/core/testing');
var signin_forgotpassword_service_1 = require('./signin-forgotpassword.service');
describe('SigninForgotpasswordService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [signin_forgotpassword_service_1.SigninForgotpasswordService]
        });
    });
    it('should ...', testing_1.inject([signin_forgotpassword_service_1.SigninForgotpasswordService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=signin-forgotpassword.service.spec.js.map