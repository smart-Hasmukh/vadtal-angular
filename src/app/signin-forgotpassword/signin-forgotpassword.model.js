"use strict";
var Login = (function () {
    function Login() {
    }
    Login.prototype.getToken = function () {
        return this._token;
    };
    Login.prototype.setToken = function (value) {
        this._token = value;
    };
    Login.prototype.getUser = function () {
        return this._user;
    };
    Login.prototype.setUser = function (value) {
        this._user = value;
    };
    Login.prototype.getPrivileges = function () {
        return this._privileges;
    };
    Login.prototype.setPrivileges = function (value) {
        this._privileges = value;
    };
    return Login;
}());
exports.Login = Login;
//# sourceMappingURL=signin-forgotpassword.model.js.map