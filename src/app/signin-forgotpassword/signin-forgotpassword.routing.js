"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var constants_1 = require("../utility/constants");
var signin_forgotpassword_component_1 = require("./signin-forgotpassword.component");
var auth_gaurds_1 = require("../_gaurds/auth.gaurds");
var routes = [
    {
        path: constants_1.RouteConstants.APP_LOGIN,
        component: signin_forgotpassword_component_1.SigninForgotpasswordComponent,
        canActivate: [auth_gaurds_1.AuthGaurd]
    }
];
var SigninForgotpasswordRoutingModule = (function () {
    function SigninForgotpasswordRoutingModule() {
    }
    SigninForgotpasswordRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(routes)
            ]
        })
    ], SigninForgotpasswordRoutingModule);
    return SigninForgotpasswordRoutingModule;
}());
exports.SigninForgotpasswordRoutingModule = SigninForgotpasswordRoutingModule;
//# sourceMappingURL=signin-forgotpassword.routing.js.map