"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../utility/constants");
var SigninForgotpasswordService = (function () {
    function SigninForgotpasswordService(router, httpService, toastManager, userService) {
        this.router = router;
        this.httpService = httpService;
        this.toastManager = toastManager;
        this.userService = userService;
    }
    SigninForgotpasswordService.prototype.login = function (body) {
        var _this = this;
        return this.httpService.post('login/', body)
            .map(function (response) {
            var status = response.json() && response.json().status;
            if (status == constants_1.HttpStatusConstants.SUCCESS) {
                _this.userService.setPrivileges(response.json().payload["privileges"]);
                _this.userService.setToken(response.json().payload.token);
                _this.userService.setUser(response.json().payload.user);
                _this.userService.setTimestamp((new Date()).getTime());
                return true;
            }
            else {
                return false;
            }
        });
    };
    SigninForgotpasswordService.prototype.logout = function () {
        this.userService.setUser(null);
        this.userService.setToken("");
        this.userService.setPrivileges(null);
        localStorage.clear();
        this.router.navigate(['/' + constants_1.RouteConstants.APP_LOGIN]);
    };
    SigninForgotpasswordService.prototype.changePassword = function (values) {
        var _this = this;
        return this.httpService.put("ApiEventConstants.CHANGE_PASS", values).map(function (res) {
            return _this.extractData(res, true);
        });
    };
    SigninForgotpasswordService.prototype.extractData = function (res, showToast) {
        if (showToast) {
            this.toastManager.success(res.json().message);
        }
        var data = res.json();
        return data || {};
    };
    SigninForgotpasswordService = __decorate([
        core_1.Injectable()
    ], SigninForgotpasswordService);
    return SigninForgotpasswordService;
}());
exports.SigninForgotpasswordService = SigninForgotpasswordService;
//# sourceMappingURL=signin-forgotpassword.service.js.map