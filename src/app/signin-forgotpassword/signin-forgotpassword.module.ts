import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SigninForgotpasswordComponent} from "./signin-forgotpassword.component";
import {MaterialModule} from "@angular/material";
import {SigninForgotpasswordRoutingModule} from "./signin-forgotpassword.routing";
import {Md2Module} from "md2";

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        SigninForgotpasswordRoutingModule,
        Md2Module
    ],
    declarations: [SigninForgotpasswordComponent],
    exports: [SigninForgotpasswordComponent]
})

export class SigninForgotpasswordModule {
}
