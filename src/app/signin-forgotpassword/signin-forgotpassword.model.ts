import {Privileges} from "../user/userprivilege/privilege.model";
import {User} from "../sharedService/user.model";

export class Login {
    private _token: string;
    private _user: User;
    private _privileges: Privileges[];

    constructor() {

    }

    getToken(): string {
        return this._token;
    }

    setToken(value: string) {
        this._token = value;
    }

    getUser(): User {
        return this._user;
    }

    setUser(value: User) {
        this._user = value;
    }

    getPrivileges(): Privileges[] {
        return this._privileges;
    }

    setPrivileges(value: Privileges[]) {
        this._privileges = value;
    }
}