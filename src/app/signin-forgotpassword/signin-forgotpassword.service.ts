import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Response} from "@angular/http";
import {ToastsManager} from "ng2-toastr";
import {ApiEventConstants, HttpStatusConstants, RouteConstants} from "../utility/constants";
import {HttpService} from "../utility/http-service";
import {UserService} from "../sharedService/user.service";

@Injectable()
export class SigninForgotpasswordService {

  constructor(private router: Router,
              private httpService: HttpService,
              private toastManager: ToastsManager,
              private userService: UserService) {

  }

  login(body): Observable<any> {
    return this.httpService.post('login/', body)
        .map((response: any) => {
          let status = response.json() && response.json().status;
          if (status == HttpStatusConstants.SUCCESS) {
            this.userService.setPrivileges(response.json().payload["privileges"]);
            this.userService.setToken(response.json().payload.token);
            this.userService.setUser(response.json().payload.user);
            this.userService.setTimestamp((new Date()).getTime());
            return true;
          } else {
            return false;
          }
        });
  }

  logout() {
    this.userService.setUser(null);
    this.userService.setToken("");
    this.userService.setPrivileges(null);
    localStorage.clear();
    this.router.navigate(['/' + RouteConstants.APP_LOGIN]);
  }

  changePassword(values: any): Observable<any> {
    return this.httpService.put("ApiEventConstants.CHANGE_PASS", values).map(res =>
        this.extractData(res, true)
    );
  }

  private extractData(res: Response, showToast: boolean) {
    if (showToast) {
      this.toastManager.success(res.json().message)
    }
    let data = res.json();
    return data || {};
  }
}
