"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var constants_1 = require("../utility/constants");
var SigninForgotpasswordComponent = (function () {
    function SigninForgotpasswordComponent(title, loginService, router) {
        this.title = title;
        this.loginService = loginService;
        this.router = router;
        this.position = 'above';
        this.delay = 0;
        this.loginFormStatus = true;
        this.sendOTPForm = false;
        this.getOTPForm = false;
        this.setNewPasswordForm = false;
        this.forgotPwdStatus = false;
        this.titleConstants = new constants_1.TitleConstants();
        this.responseArray = [];
    }
    SigninForgotpasswordComponent.prototype.ngOnInit = function () {
        this.title.setTitle(this.titleConstants.LOGIN);
        /*for(let i=0;i<10000;i++){
            this.responseArray.push(
                {"id": 1, "name": "Shaw", "country": "Tanzania", "dob" : "20-08-1993","temple" : "Vadtal"},
            )
        }*/
    };
    SigninForgotpasswordComponent.prototype.forgotPassword = function () {
        this.title.setTitle(this.titleConstants.FORGOT_PASSWORD);
        this.loginFormStatus = false;
        this.forgotPwdStatus = true;
        this.sendOTPForm = true;
    };
    SigninForgotpasswordComponent.prototype.sendOTP = function () {
        this.title.setTitle(this.titleConstants.SEND_OTP);
        this.sendOTPForm = false;
        this.loginFormStatus = false;
        this.forgotPwdStatus = true;
        this.getOTPForm = true;
    };
    SigninForgotpasswordComponent.prototype.setNewPassword = function () {
        this.title.setTitle(this.titleConstants.RESET_PASSWORD);
        this.sendOTPForm = false;
        this.getOTPForm = false;
        this.forgotPwdStatus = false;
        this.loginFormStatus = false;
        this.setNewPasswordForm = true;
    };
    SigninForgotpasswordComponent.prototype.resetPassword = function () {
        this.title.setTitle(this.titleConstants.LOGIN);
        this.setNewPasswordForm = false;
        this.sendOTPForm = false;
        this.getOTPForm = false;
        this.forgotPwdStatus = false;
        this.loginFormStatus = true;
    };
    SigninForgotpasswordComponent.prototype.login = function (username, password) {
        var _this = this;
        var body = {
            "username": username,
            "password": password
        };
        // this.generatePDF();
        this.loginService.login(body).subscribe(function (success) {
            if (success) {
                console.log("<------------------------Loggedin Successfully-------------------------->");
                _this.router.navigate(["/" + constants_1.RouteConstants.APP_HOME]);
            }
        });
    };
    SigninForgotpasswordComponent.prototype.generatePDF = function () {
        console.log("----->", (new Date()).getTime());
        var columns = [
            { title: "ID", dataKey: "id" },
            { title: "Name", dataKey: "name" },
            { title: "Country", dataKey: "country" },
            { title: "DOB", dataKey: "dob" },
            { title: "Temple", dataKey: "templename" },
        ];
        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, this.responseArray, {
            styles: { fillColor: [140, 150, 159] },
            columnStyles: {
                id: { fillColor: 255 }
            },
            margin: { top: 60 },
            addPageContent: function (data) {
                doc.text("Header", 40, 30);
            }
        });
        doc.save('table.pdf');
        console.log("----->", (new Date()).getTime());
    };
    SigninForgotpasswordComponent.prototype.backToLoginForm = function () {
        this.loginFormStatus = true;
        this.sendOTPForm = false;
        this.forgotPwdStatus = false;
    };
    SigninForgotpasswordComponent.prototype.backToForgotUsernameForm = function () {
        this.loginFormStatus = false;
        this.forgotPwdStatus = true;
        this.sendOTPForm = true;
        this.getOTPForm = false;
    };
    SigninForgotpasswordComponent.prototype.backTogetOTPForm = function () {
        this.setNewPasswordForm = false;
        this.loginFormStatus = false;
        this.forgotPwdStatus = false;
        this.getOTPForm = true;
        this.forgotPwdStatus = true;
    };
    SigninForgotpasswordComponent = __decorate([
        core_1.Component({
            selector: 'app-signin-forgotpassword',
            templateUrl: './signin-forgotpassword.component.html',
            styleUrls: ['./signin-forgotpassword.component.css'],
            animations: [
                core_1.trigger('flyInOut', [
                    core_1.state('in', core_1.style({ transform: 'translateX(0)' })),
                    core_1.transition('void => *', [
                        core_1.style({ transform: 'translateX(200%)' }),
                        core_1.animate(500)
                    ]),
                    core_1.transition('* => void', [
                        core_1.animate(100, core_1.style({ transform: 'translateX(-100%)' }))
                    ])
                ])
            ]
        })
    ], SigninForgotpasswordComponent);
    return SigninForgotpasswordComponent;
}());
exports.SigninForgotpasswordComponent = SigninForgotpasswordComponent;
//# sourceMappingURL=signin-forgotpassword.component.js.map