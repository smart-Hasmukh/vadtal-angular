/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SigninForgotpasswordService } from './signin-forgotpassword.service';

describe('SigninForgotpasswordService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SigninForgotpasswordService]
    });
  });

  it('should ...', inject([SigninForgotpasswordService], (service: SigninForgotpasswordService) => {
    expect(service).toBeTruthy();
  }));
});
