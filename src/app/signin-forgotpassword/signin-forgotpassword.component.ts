import {Component, style, state, animate, transition, trigger, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {SigninForgotpasswordService} from "./signin-forgotpassword.service";
import {RouteConstants, TitleConstants} from "../utility/constants";
import {Title} from "@angular/platform-browser";
declare let jsPDF;

@Component({
    selector: 'app-signin-forgotpassword',
    templateUrl: './signin-forgotpassword.component.html',
    styleUrls: ['./signin-forgotpassword.component.css'],
    animations: [
        trigger('flyInOut', [
            state('in', style({transform: 'translateX(0)'})),
            transition('void => *', [
                style({transform: 'translateX(200%)'}),
                animate(500)
            ]),
            transition('* => void', [
                animate(100, style({transform: 'translateX(-100%)'}))
            ])
        ])
    ]
})
export class SigninForgotpasswordComponent implements OnInit {

    position: string = 'above';
    delay: number = 0;

    loginFormStatus = true;
    sendOTPForm = false;
    getOTPForm = false;
    setNewPasswordForm = false;
    forgotPwdStatus = false;

    titleConstants = new TitleConstants();
    responseArray : any [] = [];
    ngOnInit(): void {
        this.title.setTitle(this.titleConstants.LOGIN);
        /*for(let i=0;i<10000;i++){
            this.responseArray.push(
                {"id": 1, "name": "Shaw", "country": "Tanzania", "dob" : "20-08-1993","temple" : "Vadtal"},
            )
        }*/
    }

    constructor(private title: Title, private loginService: SigninForgotpasswordService, private router: Router) {

    }

    forgotPassword(): void {
        this.title.setTitle(this.titleConstants.FORGOT_PASSWORD);
        this.loginFormStatus = false;
        this.forgotPwdStatus = true;
        this.sendOTPForm = true;
    }

    sendOTP(): void {
        this.title.setTitle(this.titleConstants.SEND_OTP);
        this.sendOTPForm = false;
        this.loginFormStatus = false;
        this.forgotPwdStatus = true;
        this.getOTPForm = true;
    }

    setNewPassword(): void {
        this.title.setTitle(this.titleConstants.RESET_PASSWORD);
        this.sendOTPForm = false;
        this.getOTPForm = false;
        this.forgotPwdStatus = false;
        this.loginFormStatus = false;
        this.setNewPasswordForm = true;
    }

    resetPassword(): void {
        this.title.setTitle(this.titleConstants.LOGIN);
        this.setNewPasswordForm = false;
        this.sendOTPForm = false;
        this.getOTPForm = false;
        this.forgotPwdStatus = false;
        this.loginFormStatus = true;
    }

    login(username:string,password:string):void {
        let body = {
            "username": username,
            "password": password
        }
        // this.generatePDF();
        this.loginService.login(body).subscribe(success => {
            if (success) {
                console.log("<------------------------Loggedin Successfully-------------------------->");
                this.router.navigate(["/" + RouteConstants.APP_HOME]);
            }
        })

    }

    generatePDF():void {
        console.log("----->", (new Date()).getTime())
        var columns = [
            {title: "ID", dataKey: "id"},
            {title: "Name", dataKey: "name"},
            {title: "Country", dataKey: "country"},
            {title: "DOB", dataKey: "dob"},
            {title: "Temple", dataKey: "templename"},
        ];
        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, this.responseArray, {
            styles: {fillColor: [140, 150, 159]},
            columnStyles: {
                id: {fillColor: 255}
            },
            margin: {top: 60},
            addPageContent: function(data) {
                doc.text("Header", 40, 30);
            }
        });
        doc.save('table.pdf');
        console.log("----->", (new Date()).getTime())
    }

    backToLoginForm(): void {
        this.loginFormStatus = true;
        this.sendOTPForm = false;
        this.forgotPwdStatus = false;
    }

    backToForgotUsernameForm(): void {
        this.loginFormStatus = false;
        this.forgotPwdStatus = true;
        this.sendOTPForm = true;
        this.getOTPForm = false;

    }

    backTogetOTPForm(): void {
        this.setNewPasswordForm = false;
        this.loginFormStatus = false;
        this.forgotPwdStatus = false;
        this.getOTPForm = true;
        this.forgotPwdStatus = true;
    }
}
