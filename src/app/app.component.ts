import {Component, ViewContainerRef} from "@angular/core";
import {ToastsManager} from "ng2-toastr/ng2-toastr";
import {HostListener} from "@angular/core/src/metadata/directives";
import {UserService} from "./sharedService/user.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    constructor(public toastr: ToastsManager, vRef: ViewContainerRef, private userService: UserService) {
        this.toastr.setRootViewContainerRef(vRef);
    }

    @HostListener('window:beforeunload', ['$event'])
    doSomething($event) {
        // localStorage.clear();
        this.userService.isSessionTimeout();
    }
}
