/* tslint:disable:no-unused-variable */
"use strict";
var testing_1 = require('@angular/core/testing');
var header_breadcrumb_service_1 = require('./header-breadcrumb.service');
describe('HeaderBreadcrumbService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [header_breadcrumb_service_1.HeaderBreadcrumbService]
        });
    });
    it('should ...', testing_1.inject([header_breadcrumb_service_1.HeaderBreadcrumbService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=header-breadcrumb.service.spec.js.map