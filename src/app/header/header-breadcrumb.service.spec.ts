/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HeaderBreadcrumbService } from './header-breadcrumb.service';

describe('HeaderBreadcrumbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeaderBreadcrumbService]
    });
  });

  it('should ...', inject([HeaderBreadcrumbService], (service: HeaderBreadcrumbService) => {
    expect(service).toBeTruthy();
  }));
});
