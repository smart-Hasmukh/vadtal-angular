import {Component, EventEmitter} from "@angular/core";
import {Output} from "@angular/core/src/metadata/directives";
import {SigninForgotpasswordService} from "../signin-forgotpassword/signin-forgotpassword.service";
import {Router, NavigationEnd, ActivatedRoute} from "@angular/router";
import {HeaderBreadcrumbService} from "./header-breadcrumb.service";
import {RouteConstants} from "../utility/constants";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  position: string = 'before';
  delay: number = 0;

  @Output() menuClick = new EventEmitter();
  private _urls: string[];
  private _routerSubrciption: any;

  constructor(private loginService: SigninForgotpasswordService, private router: Router,
              private breadcrumbService: HeaderBreadcrumbService,private activatedRoute: ActivatedRoute,) {
    this._urls = new Array();
    this._routerSubrciption = this.router.events.subscribe((navigationEnd: NavigationEnd) => {
      this._urls.length = 0; //Fastest way to clear out array
      if (this.router.url != ("/" + RouteConstants.APP_HOME)) {
        this.breadcrumbService.hideRoute(("/" + RouteConstants.APP_HOME))
      }else {
        this.breadcrumbService.showRoute(("/" + RouteConstants.APP_HOME))
      }
      this.generateBreadcrumbTrail(navigationEnd.urlAfterRedirects ? navigationEnd.urlAfterRedirects : navigationEnd.url);
    });
  }

  onMenuClick() {
    this.menuClick.emit()
  }

  logout() {
    this.loginService.logout()
  }


  redirectHomePage(){
    this.router.navigate(["/" + RouteConstants.APP_HOME]);
  }

  generateBreadcrumbTrail(url: string): void {
    if (!this.breadcrumbService.isRouteHidden(url)) {
      //Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
      this._urls.unshift(url);
    }

    if (url.lastIndexOf('/') > 0) {
      this.generateBreadcrumbTrail(url.substr(0, url.lastIndexOf('/'))); //Find last '/' and add everything before it as a parent route
    }
  }

  navigateTo(url: string): void {
    this.router.navigateByUrl(url);
  }

  friendlyName(url: string): string {
    return !url ? '' : this.breadcrumbService.getFriendlyNameForRoute(url);
  }

  ngOnDestroy(): void {
    this._routerSubrciption.unsubscribe();
  }
}
