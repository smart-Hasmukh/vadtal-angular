import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HeaderComponent} from "./header.component";
import {MaterialModule} from "@angular/material";
import {HeaderBreadcrumbService} from "./header-breadcrumb.service";
import {SigninForgotpasswordService} from "../signin-forgotpassword/signin-forgotpassword.service";
import {Md2Module} from "md2";

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    Md2Module
  ],
  declarations: [HeaderComponent],
  providers: [SigninForgotpasswordService, HeaderBreadcrumbService],
  exports: [HeaderComponent]
})
export class HeaderModule {
}
