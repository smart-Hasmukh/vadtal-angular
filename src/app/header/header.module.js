"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var header_component_1 = require("./header.component");
var material_1 = require("@angular/material");
var header_breadcrumb_service_1 = require("./header-breadcrumb.service");
var signin_forgotpassword_service_1 = require("../signin-forgotpassword/signin-forgotpassword.service");
var md2_1 = require("md2");
var HeaderModule = (function () {
    function HeaderModule() {
    }
    HeaderModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                md2_1.Md2Module
            ],
            declarations: [header_component_1.HeaderComponent],
            providers: [signin_forgotpassword_service_1.SigninForgotpasswordService, header_breadcrumb_service_1.HeaderBreadcrumbService],
            exports: [header_component_1.HeaderComponent]
        })
    ], HeaderModule);
    return HeaderModule;
}());
exports.HeaderModule = HeaderModule;
//# sourceMappingURL=header.module.js.map