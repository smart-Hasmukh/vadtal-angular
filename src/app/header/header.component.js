"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var directives_1 = require("@angular/core/src/metadata/directives");
var constants_1 = require("../utility/constants");
var HeaderComponent = (function () {
    function HeaderComponent(loginService, router, breadcrumbService, activatedRoute) {
        var _this = this;
        this.loginService = loginService;
        this.router = router;
        this.breadcrumbService = breadcrumbService;
        this.activatedRoute = activatedRoute;
        this.position = 'before';
        this.delay = 0;
        this.menuClick = new core_1.EventEmitter();
        this._urls = new Array();
        this._routerSubrciption = this.router.events.subscribe(function (navigationEnd) {
            _this._urls.length = 0; //Fastest way to clear out array
            if (_this.router.url != ("/" + constants_1.RouteConstants.APP_HOME)) {
                _this.breadcrumbService.hideRoute(("/" + constants_1.RouteConstants.APP_HOME));
            }
            else {
                _this.breadcrumbService.showRoute(("/" + constants_1.RouteConstants.APP_HOME));
            }
            _this.generateBreadcrumbTrail(navigationEnd.urlAfterRedirects ? navigationEnd.urlAfterRedirects : navigationEnd.url);
        });
    }
    HeaderComponent.prototype.onMenuClick = function () {
        this.menuClick.emit();
    };
    HeaderComponent.prototype.logout = function () {
        this.loginService.logout();
    };
    HeaderComponent.prototype.redirectHomePage = function () {
        this.router.navigate(["/" + constants_1.RouteConstants.APP_HOME]);
    };
    HeaderComponent.prototype.generateBreadcrumbTrail = function (url) {
        if (!this.breadcrumbService.isRouteHidden(url)) {
            //Add url to beginning of array (since the url is being recursively broken down from full url to its parent)
            this._urls.unshift(url);
        }
        if (url.lastIndexOf('/') > 0) {
            this.generateBreadcrumbTrail(url.substr(0, url.lastIndexOf('/'))); //Find last '/' and add everything before it as a parent route
        }
    };
    HeaderComponent.prototype.navigateTo = function (url) {
        this.router.navigateByUrl(url);
    };
    HeaderComponent.prototype.friendlyName = function (url) {
        return !url ? '' : this.breadcrumbService.getFriendlyNameForRoute(url);
    };
    HeaderComponent.prototype.ngOnDestroy = function () {
        this._routerSubrciption.unsubscribe();
    };
    __decorate([
        directives_1.Output()
    ], HeaderComponent.prototype, "menuClick");
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.css']
        })
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map