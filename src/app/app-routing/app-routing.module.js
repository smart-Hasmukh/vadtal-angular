"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var constants_1 = require("../utility/constants");
var auth_gaurds_1 = require("../_gaurds/auth.gaurds");
var utility_module_1 = require("../utility/utility.module");
exports.routes = [
    {
        path: '',
        redirectTo: constants_1.RouteConstants.APP_LOGIN,
        pathMatch: 'full'
    },
    {
        path: constants_1.RouteConstants.APP_HOME,
        loadChildren: 'app/home/home.module#HomeModule',
        canActivate: [auth_gaurds_1.AuthGaurd]
    },
    {
        path: '**',
        redirectTo: constants_1.RouteConstants.APP_LOGIN,
        pathMatch: 'full'
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                utility_module_1.UtilityModule,
                router_1.RouterModule.forRoot(exports.routes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map