import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RouteConstants} from "../utility/constants";
import {AuthGaurd} from "../_gaurds/auth.gaurds";
import {UtilityModule} from "../utility/utility.module";

export const routes: Routes = [
    {
        path: '',
        redirectTo: RouteConstants.APP_LOGIN,
        pathMatch: 'full'
    },
    {
        path: RouteConstants.APP_HOME,
        loadChildren: 'app/home/home.module#HomeModule',
        canActivate: [AuthGaurd]
    },
    {
        path: '**',
        redirectTo: RouteConstants.APP_LOGIN,
        pathMatch: 'full',
    }
];

@NgModule({
    imports: [
        UtilityModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {

}
