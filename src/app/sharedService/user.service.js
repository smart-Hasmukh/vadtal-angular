"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../utility/constants");
var general_functions_1 = require("../utility/general-functions");
var UserService = (function () {
    function UserService() {
        this._isSessionActive = false;
        this.setValues();
    }
    UserService.prototype.setValues = function () {
        this.getToken();
        this.getPrivileges();
        this.getUser();
    };
    UserService.prototype.setPrivileges = function (value) {
        this._privileges = value;
    };
    UserService.prototype.getPrivileges = function () {
        if (this._privileges && this._privileges != null) {
            return this._privileges;
        }
        else {
            return this.getPrivilegesFromLocalStorage();
        }
    };
    UserService.prototype.setUser = function (value) {
        this._user = value;
    };
    UserService.prototype.getToken = function () {
        this.setTimestamp((new Date()).getTime());
        if (this._token && this._token != "null" && this._token != null && this._token != "undefined") {
            return this._token;
        }
        else {
            return this.getTokenFromLocalStorage();
        }
    };
    UserService.prototype.setToken = function (value) {
        this._token = value;
    };
    UserService.prototype.getUser = function () {
        if (this._user && this._user != null) {
            return this._user;
        }
        else {
            return this.getUserFromLocalStorage();
        }
    };
    UserService.prototype.setPrivilegesInLocalStorage = function (value) {
        localStorage.setItem(constants_1.KeyConstants.PRIVILEGES, JSON.stringify(value));
    };
    UserService.prototype.getPrivilegesFromLocalStorage = function () {
        var privilege = JSON.parse(localStorage.getItem(constants_1.KeyConstants.PRIVILEGES));
        this.setPrivileges(privilege);
        localStorage.removeItem(constants_1.KeyConstants.PRIVILEGES);
        return privilege;
    };
    UserService.prototype.setUserInLocalStorage = function (value) {
        localStorage.setItem(constants_1.KeyConstants.CURRENT_USER, JSON.stringify(value));
    };
    UserService.prototype.getUserFromLocalStorage = function () {
        var user = JSON.parse(localStorage.getItem(constants_1.KeyConstants.CURRENT_USER));
        this.setUser(user);
        localStorage.removeItem(constants_1.KeyConstants.CURRENT_USER);
        return user;
    };
    UserService.prototype.setTokenInLocalStorage = function (value) {
        localStorage.setItem(constants_1.KeyConstants.JWT_RESPONSE_TOKEN, general_functions_1.GeneralFunction.ENCRYPT_OBJECT(value));
    };
    UserService.prototype.getTokenFromLocalStorage = function () {
        var token = general_functions_1.GeneralFunction.DECRYPT_OBJECT(localStorage.getItem(constants_1.KeyConstants.JWT_RESPONSE_TOKEN));
        localStorage.removeItem(constants_1.KeyConstants.JWT_RESPONSE_TOKEN);
        this.setToken(token);
        return token;
    };
    UserService.prototype.setTimestamp = function (value) {
        localStorage.setItem(constants_1.KeyConstants.TIMESTAMP, general_functions_1.GeneralFunction.ENCRYPT_OBJECT(value.toString()));
    };
    UserService.prototype.getTimestamp = function () {
        var timestamp = parseInt(general_functions_1.GeneralFunction.DECRYPT_OBJECT(localStorage.getItem(constants_1.KeyConstants.TIMESTAMP)));
        return timestamp;
    };
    UserService.prototype.isSessionTimeout = function () {
        var savedTimeMillies = this.getTimestamp();
        var currentTimeMillies = (new Date()).getTime();
        var diff = (currentTimeMillies - savedTimeMillies) / 1000;
        if (diff < constants_1.APPConstants.SESSION_TIMEOUT) {
            this._isSessionActive = true;
            this.setTokenInLocalStorage(this.getToken());
            this.setUserInLocalStorage(this.getUser());
            this.setPrivilegesInLocalStorage(this.getPrivileges());
        }
        else {
            localStorage.clear();
            this._isSessionActive = false;
        }
        return this._isSessionActive;
    };
    UserService = __decorate([
        core_1.Injectable()
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map