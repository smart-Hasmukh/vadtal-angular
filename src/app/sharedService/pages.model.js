"use strict";
var Pages = (function () {
    function Pages() {
    }
    Object.defineProperty(Pages.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "moduleId", {
        get: function () {
            return this._moduleId;
        },
        set: function (value) {
            this._moduleId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "icon", {
        get: function () {
            return this._icon;
        },
        set: function (value) {
            this._icon = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "isReport", {
        get: function () {
            return this._isReport;
        },
        set: function (value) {
            this._isReport = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "created_at", {
        get: function () {
            return this._created_at;
        },
        set: function (value) {
            this._created_at = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "updated_at", {
        get: function () {
            return this._updated_at;
        },
        set: function (value) {
            this._updated_at = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "canAdd", {
        get: function () {
            return this._canAdd;
        },
        set: function (value) {
            this._canAdd = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "canEdit", {
        get: function () {
            return this._canEdit;
        },
        set: function (value) {
            this._canEdit = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "canView", {
        get: function () {
            return this._canView;
        },
        set: function (value) {
            this._canView = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "canExport", {
        get: function () {
            return this._canExport;
        },
        set: function (value) {
            this._canExport = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "canCancel", {
        get: function () {
            return this._canCancel;
        },
        set: function (value) {
            this._canCancel = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pages.prototype, "canPrint", {
        get: function () {
            return this._canPrint;
        },
        set: function (value) {
            this._canPrint = value;
        },
        enumerable: true,
        configurable: true
    });
    return Pages;
}());
exports.Pages = Pages;
//# sourceMappingURL=pages.model.js.map