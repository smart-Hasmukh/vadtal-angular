"use strict";
var User = (function () {
    function User() {
    }
    return User;
}());
exports.User = User;
var UserDetail = (function () {
    function UserDetail() {
    }
    Object.defineProperty(UserDetail.prototype, "firstName", {
        get: function () {
            return this._firstName;
        },
        set: function (value) {
            this._firstName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserDetail.prototype, "lastName", {
        get: function () {
            return this._lastName;
        },
        set: function (value) {
            this._lastName = value;
        },
        enumerable: true,
        configurable: true
    });
    return UserDetail;
}());
exports.UserDetail = UserDetail;
//# sourceMappingURL=user.model.js.map