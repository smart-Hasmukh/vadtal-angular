import {Temple} from "../templemanagement/temple.model";

export class User {
    private _token: string;
    private _user: UserDetail;
}

export class UserDetail {
    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }
    private _id: number;
    private _username: string;
    private _firstName: string;
    private _lastName: string;
    private _email: string;
    private _landlineNo: string;
    private _mobileNo: string;
    private _localAddress: string;
    private _permenantAddress: string;
    private _countryId: number;
    private _stateId: number;
    private _cityId: number;
    private _pincode: number;
    private _electionCard: string;
    private _adharCard: string;
    private _panCardNo: string;
    private _licenceNo: string;
    private _image: string;
    private _roleId: number;
    private _templeId: number;
    private _otp: number;
    private _otpCreatedAt: string;
    private _updatedBy: string;
    private _isEnable: number;
    private _updated_at: string;
    private _temple: Temple;
}

