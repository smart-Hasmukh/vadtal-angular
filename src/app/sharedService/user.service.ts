import {Injectable} from "@angular/core";
import {User} from "./user.model";
import {KeyConstants, APPConstants} from "../utility/constants";
import {Privileges} from "../user/userprivilege/privilege.model";
import {GeneralFunction} from "../utility/general-functions";

@Injectable()

export class UserService {
  private _privileges: Privileges[];
  private _user: User;
  private _token: string
  private _isSessionActive: boolean = false;

  constructor() {
   this.setValues();
  }

  setValues(){
    this.getToken();
    this.getPrivileges();
    this.getUser();
  }

  setPrivileges(value: Privileges[]) {
    this._privileges = value;
  }

  getPrivileges(): Privileges[] {
    if (this._privileges && this._privileges != null) {
      return this._privileges;
    } else {
      return this.getPrivilegesFromLocalStorage();
    }
  }

  setUser(value: User) {
    this._user = value;
  }

  getToken(): string {
    this.setTimestamp((new Date()).getTime());
    if (this._token && this._token != "null" && this._token != null && this._token != "undefined") {
      return this._token;
    } else {
      return this.getTokenFromLocalStorage();
    }
  }

  setToken(value: string) {
    this._token = value;
  }

  getUser(): User {
    if (this._user && this._user != null) {
      return this._user;
    } else {
      return this.getUserFromLocalStorage();
    }
  }

  setPrivilegesInLocalStorage(value) {
    localStorage.setItem(KeyConstants.PRIVILEGES, JSON.stringify(value));
  }

  getPrivilegesFromLocalStorage(): any {
    let privilege: Privileges[] = JSON.parse(localStorage.getItem(KeyConstants.PRIVILEGES));
    this.setPrivileges(privilege);
    localStorage.removeItem(KeyConstants.PRIVILEGES);
    return privilege;
  }


  setUserInLocalStorage(value) {
    localStorage.setItem(KeyConstants.CURRENT_USER, JSON.stringify(value));
  }

  getUserFromLocalStorage(): any {
    let user: User = JSON.parse(localStorage.getItem(KeyConstants.CURRENT_USER));
    this.setUser(user);
    localStorage.removeItem(KeyConstants.CURRENT_USER);
    return user;
  }

  setTokenInLocalStorage(value) {
    localStorage.setItem(KeyConstants.JWT_RESPONSE_TOKEN, GeneralFunction.ENCRYPT_OBJECT(value));
  }

  getTokenFromLocalStorage(): any {
    let token: string = GeneralFunction.DECRYPT_OBJECT(localStorage.getItem(KeyConstants.JWT_RESPONSE_TOKEN));
    localStorage.removeItem(KeyConstants.JWT_RESPONSE_TOKEN);
    this.setToken(token);
    return token;
  }

  setTimestamp(value) {
    localStorage.setItem(KeyConstants.TIMESTAMP, GeneralFunction.ENCRYPT_OBJECT(value.toString()));
  }

  getTimestamp(): number {
    let timestamp: number = parseInt(GeneralFunction.DECRYPT_OBJECT(localStorage.getItem(KeyConstants.TIMESTAMP)));
    return timestamp;
  }

  isSessionTimeout(): boolean {
    let savedTimeMillies: number = this.getTimestamp();
    let currentTimeMillies: number = (new Date()).getTime();
    let diff: number = (currentTimeMillies - savedTimeMillies) / 1000;
    if (diff < APPConstants.SESSION_TIMEOUT) {
      this._isSessionActive = true;
      this.setTokenInLocalStorage(this.getToken());
      this.setUserInLocalStorage(this.getUser());
      this.setPrivilegesInLocalStorage(this.getPrivileges());
    } else {
      localStorage.clear();
      this._isSessionActive = false;
    }
    return this._isSessionActive;


  }
}
