export class State {

    private _id: number;
    private _name: string;
    private _countryId: number;
    private _isEnable: number;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get countryId(): number {
        return this._countryId;
    }

    set countryId(value: number) {
        this._countryId = value;
    }

    get isEnable(): number {
        return this._isEnable;
    }

    set isEnable(value: number) {
        this._isEnable = value;
    }
}
