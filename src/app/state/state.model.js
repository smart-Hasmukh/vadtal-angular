"use strict";
var State = (function () {
    function State() {
    }
    Object.defineProperty(State.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(State.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(State.prototype, "countryId", {
        get: function () {
            return this._countryId;
        },
        set: function (value) {
            this._countryId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(State.prototype, "isEnable", {
        get: function () {
            return this._isEnable;
        },
        set: function (value) {
            this._isEnable = value;
        },
        enumerable: true,
        configurable: true
    });
    return State;
}());
exports.State = State;
//# sourceMappingURL=state.model.js.map