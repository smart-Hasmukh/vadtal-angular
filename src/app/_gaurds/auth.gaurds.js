"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var constants_1 = require("../utility/constants");
var AuthGaurd = (function () {
    function AuthGaurd(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    AuthGaurd.prototype.canActivate = function (route, state) {
        var accessToken = this.userService.getToken();
        if (state.url !== '/' + constants_1.RouteConstants.APP_LOGIN && accessToken && accessToken != "null" && accessToken != null && accessToken != "undefined") {
            return true;
        }
        else if (state.url == '/' + constants_1.RouteConstants.APP_LOGIN && accessToken && accessToken != "null" && accessToken != null && accessToken != "undefined") {
            this.router.navigate(['/' + constants_1.RouteConstants.APP_HOME]);
            return false;
        }
        else if (state.url !== '/' + constants_1.RouteConstants.APP_LOGIN && (accessToken == "" || accessToken == "null" || accessToken == null || accessToken == "undefined")) {
            this.router.navigate(['/' + constants_1.RouteConstants.APP_LOGIN]);
            return false;
        }
        return true;
    };
    AuthGaurd = __decorate([
        core_1.Injectable()
    ], AuthGaurd);
    return AuthGaurd;
}());
exports.AuthGaurd = AuthGaurd;
//# sourceMappingURL=auth.gaurds.js.map