import {Injectable} from "@angular/core";
import {CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from "@angular/router";
import {RouteConstants} from "../utility/constants";
import {Observable} from "rxjs";
import {UserService} from "../sharedService/user.service";

@Injectable()

export class AuthGaurd implements CanActivate {
  constructor(private router: Router, private userService: UserService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let accessToken: string = this.userService.getToken();
    if (state.url !== '/' + RouteConstants.APP_LOGIN && accessToken && accessToken != "null" && accessToken != null && accessToken != "undefined") {
      return true;
    } else if (state.url == '/' + RouteConstants.APP_LOGIN && accessToken && accessToken != "null" && accessToken != null && accessToken != "undefined") {
      this.router.navigate(['/' + RouteConstants.APP_HOME])
      return false;
    } else if (state.url !== '/' + RouteConstants.APP_LOGIN && (accessToken == "" || accessToken == "null" || accessToken == null || accessToken == "undefined")) {
      this.router.navigate(['/' + RouteConstants.APP_LOGIN])
      return false;
    }
    return true
  }
}
