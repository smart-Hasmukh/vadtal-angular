"use strict";
var City = (function () {
    function City() {
    }
    Object.defineProperty(City.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(City.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(City.prototype, "stateId", {
        get: function () {
            return this._stateId;
        },
        set: function (value) {
            this._stateId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(City.prototype, "isEnable", {
        get: function () {
            return this._isEnable;
        },
        set: function (value) {
            this._isEnable = value;
        },
        enumerable: true,
        configurable: true
    });
    return City;
}());
exports.City = City;
//# sourceMappingURL=city.model.js.map