export class City {

    private _id: number;
    private _name: string;
    private _stateId: number;
    private _isEnable: number;

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get stateId(): number {
        return this._stateId;
    }

    set stateId(value: number) {
        this._stateId = value;
    }

    get isEnable(): number {
        return this._isEnable;
    }

    set isEnable(value: number) {
        this._isEnable = value;
    }


}
