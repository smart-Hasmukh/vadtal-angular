/* tslint:disable:no-unused-variable */
"use strict";
var testing_1 = require('@angular/core/testing');
var city_service_1 = require('./city.service');
describe('CityService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [city_service_1.CityService]
        });
    });
    it('should ...', testing_1.inject([city_service_1.CityService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=city.service.spec.js.map