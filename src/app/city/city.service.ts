import { Injectable } from '@angular/core';
import {HttpService} from "../utility/http-service";
import {Observable} from "rxjs";
import {ApiEventConstants} from "../utility/constants";
import {ToastsManager} from "ng2-toastr";
import {Response} from "@angular/http";

@Injectable()
export class CityService {

  constructor(private  httpService: HttpService,private toastManager : ToastsManager) { }

  getCityList(searchParams?): Observable<any> {
      if(!searchParams){
          return this.httpService.get(ApiEventConstants.CITY).map(res => this.extractData(res, false));
      } else {
          return this.httpService.get(ApiEventConstants.CITY + searchParams).map(res => this.extractData(res, false));
      }
  }

    private extractData(res: Response, showToast: boolean) {
        if (showToast) {
            this.toastManager.success(res.json().message)
        }
        let data = res.json();
        return data || {};
    }
}
