import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AccomodationComponent} from "./accomodation.component";
import {AccomodationRoutingModule} from "./accomodation-routing.module";
import {MaterialModule} from "@angular/material";

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        AccomodationRoutingModule,
    ],
    declarations: [AccomodationComponent]
})

export class AccomodationModule {
}
