import { Component, OnInit } from '@angular/core';
import {TitleConstants} from "../utility/constants";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-accomodation',
  templateUrl: './accomodation.component.html',
  styleUrls: ['./accomodation.component.css']
})
export class AccomodationComponent implements OnInit {

  titleConstants = new TitleConstants();

  constructor(private title: Title) { }

  ngOnInit() {
    this.title.setTitle(this.titleConstants.ACCOMMODATION);
  }

}
