"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var accomodation_component_1 = require("./accomodation.component");
var accomodation_routing_module_1 = require("./accomodation-routing.module");
var material_1 = require("@angular/material");
var AccomodationModule = (function () {
    function AccomodationModule() {
    }
    AccomodationModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                material_1.MaterialModule,
                accomodation_routing_module_1.AccomodationRoutingModule,
            ],
            declarations: [accomodation_component_1.AccomodationComponent]
        })
    ], AccomodationModule);
    return AccomodationModule;
}());
exports.AccomodationModule = AccomodationModule;
//# sourceMappingURL=accomodation.module.js.map