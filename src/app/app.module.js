"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing/app-routing.module");
var app_component_1 = require("./app.component");
var utility_module_1 = require("./utility/utility.module");
var auth_gaurds_1 = require("./_gaurds/auth.gaurds");
var signin_forgotpassword_service_1 = require("./signin-forgotpassword/signin-forgotpassword.service");
var signin_forgotpassword_module_1 = require("./signin-forgotpassword/signin-forgotpassword.module");
var md2_1 = require("md2");
var material_1 = require("@angular/material");
var user_service_1 = require("./sharedService/user.service");
var http_1 = require("@angular/http");
var ng2_pagination_1 = require("ng2-pagination");
var angular2_moment_1 = require("angular2-moment");
var country_service_1 = require("./country/country.service");
var city_service_1 = require("./city/city.service");
var state_service_1 = require("./state/state.service");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                app_routing_module_1.AppRoutingModule,
                material_1.MaterialModule.forRoot(),
                md2_1.Md2Module.forRoot(),
                utility_module_1.UtilityModule,
                angular2_moment_1.MomentModule,
                signin_forgotpassword_module_1.SigninForgotpasswordModule,
                ng2_pagination_1.Ng2PaginationModule
            ],
            providers: [
                auth_gaurds_1.AuthGaurd,
                signin_forgotpassword_service_1.SigninForgotpasswordService,
                user_service_1.UserService,
                country_service_1.CountryService,
                state_service_1.StateService,
                city_service_1.CityService,
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map