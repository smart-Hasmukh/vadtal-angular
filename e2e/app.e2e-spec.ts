import { VadtalPage } from './app.po';

describe('vadtal App', function() {
  let page: VadtalPage;

  beforeEach(() => {
    page = new VadtalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
